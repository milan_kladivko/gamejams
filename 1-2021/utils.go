package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"log"
	"time"
)

//// game timer stuff

func dt() SecsDT {
	return G.Time.DT
}

func (t *GameTime) update() {

	now := time.Now()
	dt := now.Sub(t.FrameStart)
	t.FrameStart = now

	// Clamp to some small-ish value when severely lagging.
	dt_max := time.Millisecond * 200
	if dt > dt_max {
		dt = dt_max
	}

	if false {
		log.Printf("dt = %.2fms", dt.Milliseconds())
	}

	t.DT = dt.Seconds() // Save into the global for use everywhere, don't pass it
	t.ElapsedTime += t.DT
	t.ElapsedFrames += 1 // Assuming one update per frame here
}

// Clamp returns x clamped to the interval [min, max].
//
// If x is less than min, min is returned. If x is more than max, max is returned. Otherwise, x is
// returned.
func Clamp(x, min, max float64) float64 {
	if x < min {
		return min
	}
	if x > max {
		return max
	}
	return x
}

func geom_xy(x, y float64) (g ebi.GeoM) {
	g.Translate(x, y)
	return g
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(win_w, win_h int) (render_w, render_h int) {
	return int(RENDER_W), int(RENDER_H)
}

func player() *Player { return &G.State.Player } // shortcut
