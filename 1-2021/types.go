package main

import (
	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"

	"time"
)

// Here should be included any types, interfaces and methods
// to fulfill those interfaces (the last part might not be a good idea).

type Game struct {
	State   GameState
	Time    GameTime
	Sound   GameSounds
	Console GameConsole
}

//

type GameTime struct {
	FrameStart    time.Time // Do we really need such a large timestamp?
	DT            SecsDT
	ElapsedTime   SecsDT
	ElapsedFrames int64
}

// In the entire project, we'll be using the same DT unit.
// Working with math is weird enough, no point in throwing in nanos
// that `time.Duration` uses. Not good for games.
// This is syntax for being able to still use `float64` for times
// interchangably, but mentioning `SecsDT` where appropriate is preferrable.
type SecsDT = float64

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function.
func DurationToDt(d time.Duration) SecsDT {
	// Note: Yes, this doesn't truncate the seconds. It returns the fractional
	// conversion of nanoseconds to seconds.
	return d.Seconds()
}

//

type GameSounds struct {
	Ctx  *audio.Context
	Wavs map[string]SpammableSound
}
type SpammableSound struct {
	players []*audio.Player
}

//

type GameState struct {
	// World camera
	CameraX, CameraY float64

	// Entities, I guess
	Player           Player
	Stones           []Stone
	BackgroundImages []BackgroundImage
	Punies           []Puny

	// UI
	UIBoxes    []UIBox
	FleshBoxes []FleshBox
}

type Player struct {
	X, Y, Dx, Dy    float64 // DiifX, DiffY
	Rot             float64
	CollisionRadius float64

	Anim AnimationPlayer
}

type Stone struct {
	X, Y, Dx, Dy    float64
	Rot, Drot       float64
	CollisionRadius float64

	img *ebi.Image
}

type Puny struct {
	X, Y, Dx, Dy    float64
	Rot, Drot       float64
	CollisionRadius float64

	img *ebi.Image
}

type BackgroundImage struct {
	// To multiply the camera offsetting to appear as if the images were
	// deeper / higher than we are.
	DepthFactor float64
	X, Y        float64

	img *ebi.Image
}

// Some DOD stuff (entities)

type thing_id = uint32
type thing_with_id struct{ EntityID thing_id }

var TakenIDs map[thing_id]struct{}

func NewEntityID() thing_id {
	for {
		id := rand.Uint32()
		if _, taken := TakenIDs[id]; taken {
			continue
		}
		return id
	}
	return 0
}

type Particle struct {
	thing_with_id
	img                         *ebi.Image
	TimeToLive, TimeToLiveStart SecsDT
}
type Movable struct {
	thing_with_id
	X, Y, Rot    float64
	Dx, Dy, Drot float64
}

// UI
type UIBox struct {
	X, Y float64
	img  *ebi.Image
}
type FleshBox struct {
	X, Y float64
	Anim AnimationPlayer
}
