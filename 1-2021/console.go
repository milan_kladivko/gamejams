package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"log"
	"strings"
)

// Console to type arbitrary commands into

type GameConsole struct {
	IsOpen      bool
	CommandText string
	ErrorText   string
}

// To not overwrite the console commands that we might want to do
// inline or with `init()` functions, we'll just have that outside
// of the game object. No reason not to.
// Ideally, the console wouldn't need an init func at all.
var consoleCommands = make(map[string]func())

func AddConsoleCommand(name string, f func()) {

	log.Printf(" +cmd :: `%s`", name)
	if _, alreadyExists := consoleCommands[name]; alreadyExists {
		panic("Command `" + name + "` already exists")
	}

	consoleCommands[name] = f
}

func ConsoleUpdate() (input_was_handled_by_console bool) {
	if true {
		return false
	}

	if inpututil.IsKeyJustPressed(ebi.KeyComma) { // Toggle console
		G.Console.IsOpen = !G.Console.IsOpen
		G.Console.ErrorText = "" // Clear error

	} else if G.Console.IsOpen == true {

		if inpututil.IsKeyJustPressed(ebi.KeyEnter) { // Do the command

			cmd, ok := consoleCommands[G.Console.CommandText]
			if !ok {
				G.Console.ErrorText = "" +
					"Unknown command: `" + G.Console.CommandText + "`"
			} else {
				cmd()                    // Exec command function
				G.Console.ErrorText = "" // Clear error
			}

		} else if inpututil.IsKeyJustPressed(ebi.KeyDelete) { // Clear text
			G.Console.CommandText = ""

		} else { // Append any text to the input characters
			writtenInput := ebi.InputChars()
			before := G.Console.CommandText

			var b strings.Builder
			b.Grow(len(before) + len(writtenInput))

			b.WriteString(before)
			for _, r := range writtenInput {
				b.WriteRune(r)
			}

			G.Console.CommandText = b.String()
		}
		return true
	}
	return false
}

func might_draw_console(screen *ebi.Image) {
	if G.Console.IsOpen {
		ebitenutil.DebugPrintAt(screen, "> "+G.Console.CommandText, 10, 20)
	}
}
