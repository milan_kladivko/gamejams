package main

// Note: For now, we'll use circles for the most part
// where every sprite will have its size in pixels
// hardcoded and we'll use circle-circle collision
// whenever we can.

// Note: Choose sizes a little smaller than the sprite itself.

// Sources and reading:
// https://web.eecs.umich.edu/~sugih/courses/eecs494/fall06/lectures/lecture5-physics.pdf

func AreCirclesColliding(xa, ya, ra, xb, yb, rb float64) bool {
	dx := xa - xb
	dy := ya - yb
	d_2 := dx*dx + dy*dy

	rsum := ra + rb

	return d_2 < (rsum * rsum)
}

func IsPlayerColliding(xa, ya, ra float64) bool {
	pl := player()
	xb, yb, rb := pl.X, pl.Y, pl.CollisionRadius

	return AreCirclesColliding(xa, ya, ra, xb, yb, rb)
}
