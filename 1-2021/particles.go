package main

import (
	"log"
	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

// DOD-style maps for holding data that isn't really together.
// The idea is to handle the general and shared parts as shared parts
// and avoid layering inheritance or interfaces.
// We'll see if that's a good idea.

// @todo; Move to game state
var all_movingObjects = make(map[thing_id]Movable)
var all_particles = make(map[thing_id]Particle)

// @todo; @test; Move to the proper place
var gx_particle *ebi.Image

func init() {
	img, err := LoadImage("./graphix/sungi3.png")
	canPanic(err)
	gx_particle = img
}

func update_all_movingObjects(dt_secs float64) {
	dict := all_movingObjects
	for id := range dict {
		o := dict[id] // Can't change the fields directly in a map
		{
			o.X += o.Dx     //* dt_secs
			o.Y += o.Dy     //* dt_secs
			o.Rot += o.Drot //* dt_secs
		}
		dict[id] = o
	}
}
func update_all_particles(dt_secs float64) {
	dict := all_particles
	for id := range dict {
		o := dict[id]
		{
			o.TimeToLive -= dt_secs
			if o.TimeToLive < 0 {
				delete_particle(id)
				continue // don't re-add it by mistake!!
			}
		}
		dict[id] = o
	}
}
func delete_particle(id thing_id) {
	delete(all_particles, id)
	delete(all_movingObjects, id)
}

// Yes, that's a type only for defining what particles we want
type particle_randparams struct {
	// @todo; There should be a way to define...
	//  - alternative sprites
	//  - animations for all particles
	//  - alternative animations for each particle to pick from
	//  For now though, just one image for each batch is enough,
	//  as we can still transform however we want to.
	Image *ebi.Image

	StartX, StartY, StartRand  float64
	Dx, Dy, DRand              float64
	Rot, RandRot               float64
	TimeToLive, TimeToLiveRand SecsDT
}

func create_particle_batch(n int, pars particle_randparams) {

	if pars.TimeToLive == 0 && pars.TimeToLiveRand == 0 {
		log.Printf("Probably want them to live, don't you?")
		return
	}

	rander := func(n, defy float64) float64 {
		r := rand.Float64()
		return n + (r - (r / 2))
	}
	for i := 0; i < n; i++ {
		phys := Movable{
			X:   rander(pars.StartX, pars.StartRand),
			Y:   rander(pars.StartY, pars.StartRand),
			Dx:  rander(pars.Dx, pars.DRand),
			Dy:  rander(pars.Dy, pars.DRand),
			Rot: rander(pars.Rot, pars.RandRot),
		}
		ttl := rander(pars.TimeToLive, pars.TimeToLiveRand)
		particle := Particle{
			TimeToLive: ttl, TimeToLiveStart: ttl,
			img: pars.Image,
		}

		{ // Add the item
			id := NewEntityID()
			phys.EntityID = id
			particle.EntityID = id

			all_particles[id] = particle
			all_movingObjects[id] = phys
		}

	}
}

func draw_particles(screen *ebi.Image, cam ebi.GeoM) {

	// We can probably reuse some of the things
	op := &ebi.DrawImageOptions{}

	// @todo; We should kind of batch things by the images
	//   that we want to draw. Ebiten doesn't require it, (and there's some
	//   magic involved by default) but we really should sort the particles
	//   into buckets by the original image if we decide to make stupid
	//   amounts of particles.

	for id := range all_particles {
		var (
			particle = all_particles[id]
			pos      = all_movingObjects[id]
		)

		scale := particle.TimeToLive / particle.TimeToLiveStart
		scale *= 0.5

		var g = &op.GeoM
		g.Reset()
		g.Scale(scale, scale) // @test
		{

			g.Translate(pos.X, pos.Y)
			// g.Rotate(pos.Rot)
			g.Concat(cam)
		}

		DrawCentered(screen, particle.img, op)
	}
}
