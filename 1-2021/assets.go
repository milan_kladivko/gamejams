package main

import (
	"image"
	_ "image/png"

	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"

	"encoding/json"
	"log"
	"strings"
	"time"

	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
)

var ( // `var` because what if we need to somehow detect at init time

	// Folder containing sound files
	FOLDER_SOUNDS = "./soundz"

	// List of the sound files, so we don't typo some strings
	SND_JAZZY = "jazzy.wav"
	SND_BYOM  = "byoom.wav"

	// Assign number of channels for sound effects to let multiple
	// effects play at once.
	// Also reused for validating presence of files at init.
	SND__CHANNELS = map[string]int{
		SND_JAZZY: 5,
		SND_BYOM:  1,
	}
	SND__CHANNELS_DEFAULT = 3
)

// Get a sound player to play back
func FreePlayer(soundname string) *audio.Player {
	spams, ok := G.Sound.Wavs[soundname]
	if !ok {
		panic(simpleError("unknown soundname `" + soundname + "`"))
	}

	for _, p := range spams.players {
		if p.IsPlaying() == false {
			return p
		}
	}
	return nil
}
func MustPlay(soundname string) {
	plr := FreePlayer(soundname)
	if plr == nil {
		// There's no free player, gotta grab one that's not done yet
		plr = G.Sound.Wavs[soundname].players[0]
	}
	plr.Rewind()
	plr.Play()
}

// Prepare sound for playback
func InitSound() {
	if LOG_SOUND_INIT {

		log.Print("Importing .wav files ...")

		tstart := time.Now()
		defer func() {
			tdiff := time.Now().Sub(tstart)

			log.Printf("Done.  (loaded in %.6fs)", DurationToDt(tdiff))
		}()
	}

	// Initializing the audio context with a sample rate that we'll
	// be using for all our sounds. The sample rate can't/won't change.
	G.Sound.Ctx = audio.NewContext(sampleRate)

	// Loading all sounds in the asset folder and initializing
	// their players into a map.
	G.Sound.Wavs = make(map[string]SpammableSound)

	// Importing all .wav files in the sounds folder
	type wavload struct {
		LoadableFilepath string
		ExecName         string
	}
	var wavFilepaths []wavload
	{
		dir := FOLDER_SOUNDS

		f, err := os.Open(dir)
		canPanic(err)

		stats, err := f.Stat()
		canPanic(err)
		if stats.IsDir() == false {
			panic("" +
				"the sounds folder isn't a directory (" + dir + ")")
		}

		fnames, err := f.Readdirnames(-1) // Just the filenames in the dir
		canPanic(err)

		ext := ".wav"
		for _, fname := range fnames {
			if strings.HasSuffix(fname, ext) {

				// @todo; Filepaths should always include the path
				// relative to the sounds folder -- if we ever add scans
				// for subfolders.

				// Note: The folder should include the extension, just in case.

				names := wavload{
					LoadableFilepath: filepath.Join(dir, fname),
					ExecName:         fname,
				}
				wavFilepaths = append(wavFilepaths, names)

				if LOG_SOUND_INIT {
					log.Printf("  `%s`  from  `%s`", names.ExecName, names.LoadableFilepath)
				}
			}
		}

		// @todo; Watch the files for saves, reload the files safely for
		// quick testing.
	}
	for _, names := range wavFilepaths {

		// Note: Player source data can't be shared between players,
		//  we have to copy the data for every player that wants the data.

		var nChannels, hasChannelsSet = SND__CHANNELS[names.ExecName]
		if !hasChannelsSet {
			nChannels = SND__CHANNELS_DEFAULT
		}

		spams := SpammableSound{
			players: make([]*audio.Player, 0, nChannels),
		}

		// Because the audio lib's API can't reuse the same data
		// and keep it in memory, we'll have to load the sound files
		// multiple times to get multiple views into the file.
		// I'm not ever sure how much data we have in memory,
		// or if it's read when we call play... who knows...
		// Nothing else to do but keep the bytes in memory
		// and decode multiple times I guess...

		wavdata, err := ioutil.ReadFile(names.LoadableFilepath)
		canPanic(err)

		for i := 0; i < nChannels; i++ {
			// New reader for every channel of the same sound.
			reader := bytes.NewReader(wavdata)

			decoded, err := wav.Decode(G.Sound.Ctx, reader)
			canPanic(err)
			plr, err := audio.NewPlayer(G.Sound.Ctx, decoded)
			canPanic(err)

			spams.players = append(spams.players, plr)
		}

		G.Sound.Wavs[names.ExecName] = spams
	}

	// Validate the required files, use the channels map for mentions
	for reqExecName := range SND__CHANNELS {
		if _, ok := G.Sound.Wavs[reqExecName]; !ok {
			panic(simpleError("Missing required sound file: `" + reqExecName + "`"))
		}
	}
}
func loadSound(path string) (*wav.Stream, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	decoded, err := wav.Decode(G.Sound.Ctx, bytes.NewReader(file))
	if err != nil {
		return nil, err
	}
	return decoded, nil
}

//
// Graphics
//

func DrawCentered(onto *ebi.Image, what *ebi.Image, op *ebi.DrawImageOptions) {
	bounds := what.Bounds()
	x, y := float64(-bounds.Dx()/2), float64(-bounds.Dy()/2)
	var geom ebi.GeoM
	geom.Translate(x, y)

	// Apply this translation at the beginning
	geom.Concat(op.GeoM)
	op.GeoM = geom

	onto.DrawImage(what, op)
}

var gx_stoneAlts []*ebi.Image

func prettyJson(thing interface{}) string { // @move!!!!!!!
	bytes, err := json.MarshalIndent(thing, "", "  ")
	canPanic(err)
	return string(bytes)
}

func LoadImageAlternatives(glob string) ([]*ebi.Image, []string) {
	altsPaths, err := filepath.Glob(glob)
	canPanic(err)

	alts := make([]*ebi.Image, len(altsPaths))

	for i, filepath := range altsPaths {
		s, err := LoadImage(filepath)
		canPanic(err)
		alts[i] = s
	}
	return alts, altsPaths
}

func pickRandomImageFrom(alts []*ebi.Image) *ebi.Image {
	return alts[rand.Intn(len(alts))]
}

//
//  ==== live reloads ====
//

type ReloadableAsset interface {
	RecordLoaded()
	HasNewerVersion() bool

	Reload() // File reload function needs to be filled by subtypes
}

var watchedAssets []ReloadableAsset

func WatchAssetChanges(a ReloadableAsset) {
	watchedAssets = append(watchedAssets, a)
}
func MaybeReloadWatchedAssets() {
	// Check and optionally reload assets at runtime
	for _, wa := range watchedAssets {
		if wa.HasNewerVersion() {
			wa.Reload()
		}
	}
}

type Asset struct {
	JsonFilepath string
	lastLoaded   time.Time
}

func (a *Asset) WatchPath(abspath string) {
	a.JsonFilepath = abspath
}
func (a *Asset) RecordLoaded() {
	a.lastLoaded = time.Now()
}
func (a Asset) HasNewerVersion() bool {
	s, err := os.Stat(a.JsonFilepath)
	if err != nil {
		log.Printf("Warn: Couldn't read file's modtime: `%s`\n%s\n", a.JsonFilepath, err)
		return false // Assume not changed on os error -- can't read it anyway
	}
	return s.ModTime().After(a.lastLoaded)
}

//
//  ==== simple sprites ====
//

type Sprite struct {
	Asset
	*ebi.Image
}

func LoadImage(path string) (*ebi.Image, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return ebi.NewImageFromImage(img), nil
}

//
//  ==== animation player ====
//

// A wrapper around the animation to allow playback.
type AnimationPlayer struct {
	Animation       *Animation // Controlled animation struct
	FrameIndex      int        // Index of currently pointed-at frame
	DurationCounter SecsDT     // Time counter for duration playback
	PlayedTag       *Tag       // Currently played tag of the animation
	sprite          *ebi.Image // Cached sprite
}

// Get the current sprite, as defined by the PlayedTag and the tag's FrameIndex.
func (pl AnimationPlayer) Sprite() *ebi.Image {
	if pl.sprite == nil {
		pl.sprite = ebi.NewImage(0, 0) // Create an empty sprite
	}
	bounds := pl.PlayedTag.Frames[pl.FrameIndex].Bounds
	pl.sprite = pl.Animation.Sheet.
		SubImage(image.Rect(
			int(bounds.Min.X), int(bounds.Min.Y),
			int(bounds.Max.X), int(bounds.Max.Y),
		)).(*ebi.Image)
	return pl.sprite
}

// Find a tag by name and switch to it. Nothing done to the FrameIndex.
func (pl *AnimationPlayer) SwitchToTag(tagName string) *AnimationPlayer {
	findTag := func(tagName string, tags []Tag) *Tag {
		for _, tag := range tags {
			//log.Print(tag.Name)
			if tag.Name == tagName {
				return &tag
			}
		}
		return nil
	}
	foundTag := findTag(tagName, pl.Animation.Tags)
	if foundTag == nil {
		log.Printf("Error switching to tag %s::%s; ignoring switch...\n",
			pl.Animation.ImageFilepath, tagName)
		return pl
	}
	pl.PlayedTag = foundTag
	return pl
}

// Switch to the default aseprite tag, playing the full animation.
func (pl *AnimationPlayer) SwitchToDefaultTag() *AnimationPlayer {
	pl.PlayedTag = &(pl.Animation.Tags[0])
	return pl
}

// Start the animation; reset the frame index.
func (pl *AnimationPlayer) Start() *AnimationPlayer {
	pl.FrameIndex = 0
	return pl
}

// Advance the animation frame by one unconditionally.
func (pl *AnimationPlayer) NextFrame() *AnimationPlayer {
	pl.FrameIndex++
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations.
func (pl *AnimationPlayer) Next(dt SecsDT) *AnimationPlayer {

	pl.DurationCounter += dt

	currentFrame := pl.PlayedTag.Frames[pl.FrameIndex]
	nextSwitch := SecsDT(float64(currentFrame.DurationMs) / 1000.0)

	if pl.DurationCounter >= nextSwitch {
		// Remember the overflow
		pl.DurationCounter -= nextSwitch
		// Next frame please
		pl.FrameIndex++
	}

	// Note: Using `Next()` directly can overflow
	return pl
}

// If past the end, loop back to the start.
func (pl *AnimationPlayer) LoopAtEnd() *AnimationPlayer {
	max := len(pl.PlayedTag.Frames) - 1
	if pl.FrameIndex > max {
		pl.FrameIndex = 0
	}
	return pl
}

// If past the end, stop at the end.
func (pl *AnimationPlayer) StopAtEnd() *AnimationPlayer {
	max := len(pl.PlayedTag.Frames) - 1
	if pl.FrameIndex > max {
		pl.FrameIndex = max
	}
	return pl
}

//
//  ==== animation data ====
//

// @todo; Add a way of adding sound-trigger data into the animations

type (

	// Note: Animation assets have two key files, the json and the png.
	// The json includes all data about frame regions and tags (what regions
	// belong to which animation).
	// The json also has the filepath of the png, relative to the json file.

	Animation struct {
		Asset         // The animation is watchable and reloadable at runtime
		ImageFilepath string
		Tags          []Tag      // Sub-animations included in the asset
		Sheet         *ebi.Image `json:"-"` // Image data
	}
	Tag struct {
		Name   string
		Frames []Frame
	}
	Frame struct {
		DurationMs int
		Bounds     pxlRect
	}
	pxlRect struct{ Min, Max pxlVec }
	pxlVec  struct{ X, Y float64 }
)

func LoadAnimation(abspath string) (*Animation, error) {
	var a Animation
	a.Asset.JsonFilepath = abspath
	a.Reload()
	return &a, nil
}

func (a *Animation) Reload() {
	defer a.RecordLoaded()

	bytes, err := ioutil.ReadFile(a.Asset.JsonFilepath)
	canPanic(err)
	var aseJson AseJSON
	aseJson.Filepath = a.Asset.JsonFilepath // @todo; Factor that out
	err = json.Unmarshal(bytes, &aseJson)
	canPanic(err)
	animation, err := aseJson.toAnimation()
	canPanic(err)

	// Change the content of the animation, always use the same
	// variable when live-reloading.
	a.Sheet = animation.Sheet
	a.Tags = animation.Tags
}

// The Aseprite editor's metadata format that is saved alongside
// an exported sprite sheet. It has all the information for putting
// all of the information back together as an animation or atlas.
//
// Deciding what goes in which field is done with default values,
// we don't specify the `json:"field_name"` tags for the fields
// and it works fine.
//
// @todo; Read triggers from the animation (sounds, events...)
//
// @todo; Read slices for atlas usage
type (
	AseJSON struct {
		Meta     aseMeta    // File properties, sizes and individual tags
		Frames   []aseFrame // Frame bounds and durations
		Filepath string     // Path of the loaded .json file
	}
	aseMeta struct {
		Image     string        // Filename of the spritesheet image
		Size      aseSize       // Size of the spritesheet
		FrameTags []aseFrameTag // Individual animations
	}
	aseFrameTag struct {
		Name string
		From int
		To   int
	}
	aseFrame struct {
		Duration int     // Duration in ms
		Frame    aseRect // Spritesheet's cutout region
	}
	aseRect struct {
		X, Y, W, H float64
	}
	aseSize struct {
		W, H float64
	}
)

// Convert aseprite export metadata into a usable animation struct
func (ase AseJSON) toAnimation() (*Animation, error) {

	aseJsonDir := filepath.Dir(ase.Filepath)
	loadPath := filepath.Join(aseJsonDir, ase.Meta.Image)

	picture, err := LoadImage(loadPath)
	if err != nil {
		return nil, err
	}

	animation := Animation{
		ImageFilepath: ase.Meta.Image,
		Sheet:         picture,
		Tags:          []Tag{},
	}

	// Extract a tag out of a picture and aseFrame data
	tagFromRange := func(
		name string, pic *ebi.Image, aseFrames []aseFrame,
	) Tag {
		var frames = make([]Frame, len(aseFrames))
		for index, it := range aseFrames {
			// Note: Y is down in aseprite but not in Pixel.
			// The Y has to be flipped to get the correct bounds of
			// the frames.

			var r aseRect = it.Frame

			frames[index] = Frame{
				Bounds: pxlRect{
					Min: pxlVec{
						X: r.X,
						Y: r.Y,
					},
					Max: pxlVec{
						X: r.X + r.W,
						Y: r.Y + r.H,
					},
				},
				DurationMs: it.Duration,
			}
		}
		return Tag{Name: name, Frames: frames}
	}

	// Always include the full animation as a default tag
	animation.Tags = append(animation.Tags,
		tagFromRange("full", picture, ase.Frames[:]))

	// Add the individual frame tags, if present
	for _, t := range ase.Meta.FrameTags {
		hmm := 1 // @test
		animation.Tags = append(animation.Tags,
			tagFromRange(t.Name, picture, ase.Frames[t.From:t.To+hmm]))
	}
	return &animation, nil
}
