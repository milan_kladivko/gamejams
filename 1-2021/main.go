package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"image/color"

	"fmt"
	"log"
	"math/rand"
)

const ( // Printing and logging constants
	PRETTY_PRINT_PANICS = true
	LOG_SOUND_INIT      = true
)

const (
	RENDER_W   float64 = 480
	RENDER_H   float64 = 320
	WIN_INIT_W         = int(RENDER_W * 2)
	WIN_INIT_H         = int(RENDER_H * 2)

	sampleRate = 44100
)

var G Game // Global game object

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
// If an error is passed, it will get returned by `ebi.RunGame(g)`.
func (g *Game) Update() error {
	defer GrabAndReformatPanics()

	G.Time.update()

	if inpututil.IsKeyJustPressed(ebi.KeyX) {
		MustPlay(SND_JAZZY)
	}
	if ebi.IsKeyPressed(ebi.KeyQ) {
		return quitkey
	}

	anim := &player().Anim
	anim.Next(dt())
	anim.LoopAtEnd()

	G.State.CameraY += 0.5 // @test; Move forward a bit

	{ // Player controls
		var p *Player = player()

		{ // movement
			const acc = 0.05
			if inpututil.IsKeyJustPressed(ebi.KeyW) {
				anim.SwitchToTag("propulsion")
				anim.FrameIndex = 0
			}
			if ebi.IsKeyPressed(ebi.KeyW) {
				// play propulsion
				p.Dy -= acc
			}
			if ebi.IsKeyPressed(ebi.KeyS) {
				p.Dy += acc
				anim.SwitchToTag("idle")
				anim.FrameIndex = 0
			}
			if ebi.IsKeyPressed(ebi.KeyD) {
				p.Dx += acc
			}
			if ebi.IsKeyPressed(ebi.KeyA) {
				p.Dx -= acc
			}

			{ // apply, limit etc
				const max = 1
				p.Dx = Clamp(p.Dx, -max, max)
				p.Dy = Clamp(p.Dy, -max, max)
				p.X += p.Dx
				p.Y += p.Dy
			}
		}

		if inpututil.IsKeyJustPressed(ebi.KeySpace) { // Shooting stones

			// Stone projectile itself
			s := Stone{
				X:    p.X,
				Y:    p.Y,
				Dx:   (p.Dx * 0.8),
				Dy:   (p.Dy * 0.8) - 3, // up
				Drot: (p.Dx * 0.1),

				CollisionRadius: 5,
				img:             pickRandomImageFrom(gx_stoneAlts),
			}
			G.State.Stones = append(G.State.Stones, s)

			// Particles for effect
			create_particle_batch(10, particle_randparams{
				StartX: s.X, StartY: s.Y, StartRand: 5,
				// @todo; rotation?
				Dx:         s.Dx * 0.5,
				Dy:         s.Dy * 0.2,
				DRand:      30,
				Image:      gx_particle,
				TimeToLive: 1, TimeToLiveRand: 3,
			})
		}
	}

	update_all_movingObjects(dt())
	update_all_particles(dt())

	{ // Collision w punies
		s := &G.State
		newPuns := make([]Puny, 0)
		for _, pn := range s.Punies {
			isCol := IsPlayerColliding(pn.X, pn.Y, pn.CollisionRadius)
			fbEmptyLevel := s.FleshBoxes[0].Anim.FrameIndex
			if isCol && fbEmptyLevel > 0 {
				// CollectPuny(pn)
				s.FleshBoxes[0].Anim.FrameIndex--
			} else {
				newPuns = append(newPuns, pn)
			}
		}
		s.Punies = newPuns
	}

	for i, s := range G.State.Stones {
		G.State.Stones[i].X += s.Dx
		G.State.Stones[i].Y += s.Dy
		G.State.Stones[i].Rot += s.Drot
	}
	for i, s := range G.State.Punies {
		G.State.Punies[i].X += s.Dx
		G.State.Punies[i].Y += s.Dy
		G.State.Punies[i].Rot += s.Drot
	}

	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebi.Image) {
	// Draws are run outside the main goroutine, gotta defer for this one.
	defer GrabAndReformatPanics()

	// Wipe the screen at the start
	screen.Fill(color.RGBA{0x12, 0x17, 0x3d, 0xfe})

	Cam := geom_xy(G.State.CameraX, G.State.CameraY)
	if false {
		log.Print(Cam)
	}

	// Start with the background, boi
	for _, bi := range G.State.BackgroundImages {
		op := &ebi.DrawImageOptions{}
		{
			op.GeoM.Concat(Cam)
			op.GeoM.Scale(bi.DepthFactor, bi.DepthFactor)
			op.GeoM.Translate(bi.X, bi.Y)
		}
		DrawCentered(screen, bi.img, op)
	}

	{ // Draw the player image
		p := player()
		img := p.Anim.Sprite()
		op := &ebi.DrawImageOptions{}
		{
			// Apply rotation
			op.GeoM.Rotate(p.Rot)
			// Move the rotated image
			op.GeoM.Translate(p.X, p.Y)
			// Apply the camera's offset
			op.GeoM.Concat(Cam)
		}
		DrawCentered(screen, img, op)

		if true {
			ebitenutil.DebugPrint(screen, fmt.Sprintf(""+
				"dx:%+3.1f\ndy:%+3.1f", p.Dx, p.Dy))
		}
	}

	st := G.State

	// Drawing stones
	for _, s := range st.Stones {
		op := &ebi.DrawImageOptions{}
		{
			op.GeoM.Rotate(s.Rot)
			op.GeoM.Translate(s.X, s.Y)
			op.GeoM.Concat(Cam)
		}
		DrawCentered(screen, s.img, op)
	}
	// Drawing punies
	for _, s := range st.Punies {
		op := &ebi.DrawImageOptions{}
		{
			op.GeoM.Rotate(s.Rot)
			op.GeoM.Translate(s.X, s.Y)
			op.GeoM.Concat(Cam)
		}
		DrawCentered(screen, s.img, op)
	}
	{ // Draw the UI
		{ // Generic UI boxes (usually containing smth)
			uibs := st.UIBoxes
			for _, box := range uibs {
				op := &ebi.DrawImageOptions{}
				{
					op.GeoM.Translate(box.X, box.Y)
				}
				screen.DrawImage(box.img, op)
			}
		}

		{ // Fleshboxes
			fbs := st.FleshBoxes
			for _, fb := range fbs {
				img := fb.Anim.Sprite()
				op := &ebi.DrawImageOptions{}
				{
					// Move the rotated image
					op.GeoM.Translate(fb.X, fb.Y)
				}
				screen.DrawImage(img, op)
			}
		}

	}

	ebitenutil.DebugPrintAt(screen, "Press X to play the wav", 0, int(RENDER_H/2))

	// All the particles at the end (probably want to see them above everything)
	draw_particles(screen, Cam)

	ebitenutil.DebugPrintAt(screen, "Press X to play the wav", 0, RENDER_H/2)

	// might_draw_console(screen)
	{
		fps := fmt.Sprintf("FPS: %.2f", ebi.CurrentFPS())
		ebitenutil.DebugPrintAt(screen, fps, 0, int(RENDER_H-20))
	}
}

func InitGraphics() {
	{ // Player animation data
		anim := &player().Anim

		dudeAnimations, err := LoadAnimation("./graphix/mimiship1.json")
		canPanic(err)

		anim.Animation = dudeAnimations
		anim.SwitchToDefaultTag()
	}

	{ // Alternative stone images
		alts, _ := LoadImageAlternatives("./graphix/alternable/rock*.png")
		gx_stoneAlts = alts
		// log.Printf("Loaded %d rocks: %v", len(alts), altsPaths)
	}

	{ // UI
		{ // Generic UI boxes
		}
		{ // Flesh boxes
			boxImg, err := LoadImage("./graphix/ui/box_flesh-holder.png")
			canPanic(err)
			box := UIBox{ // (almost)same as fleshbox
				X: 10,
				Y: RENDER_H / 1.5, // magic number
			}
			box.img = boxImg
			G.State.UIBoxes = append(G.State.UIBoxes, box)

			// the flesh itself
			fbAnim, err := LoadAnimation("./graphix/flesh_bar.json") // TODO rename files also
			canPanic(err)

			fb := FleshBox{ // offset by holder's border
				X: box.X + 6,
				Y: box.Y + 6,
			}
			fb.Anim.Animation = fbAnim
			fb.Anim.SwitchToDefaultTag()
			fb.Anim.FrameIndex = len(fb.Anim.PlayedTag.Frames) - 1

			G.State.FleshBoxes = append(G.State.FleshBoxes, fb)
		}
	}
}

func main() {
	defer GrabAndReformatPanics()

	{ // Window settings
		ebi.SetWindowSize(WIN_INIT_W, WIN_INIT_H)
		ebi.SetWindowTitle("Weekly Game Jam 183")
		ebi.SetWindowResizable(true)
	}

	// Prepare sound context, load sound files into memory.
	InitSound()
	// Load the assets into memory and load the animation information.
	InitGraphics()
	// Initialize the game state
	InitGameState := func() { // @todo; Move
		{ // Give the player some initial state
			p := player()
			p.X = RENDER_W / 2
			p.Y = RENDER_H / 2
			p.CollisionRadius = 20
		}

		s := &G.State
		{ // Throw some debris around
			alts, _ := LoadImageAlternatives("./graphix/alternable/debris*.png")
			const n = 200
			s.BackgroundImages = make([]BackgroundImage, n)
			for i := range s.BackgroundImages {
				bi := &s.BackgroundImages[i]

				bi.img = pickRandomImageFrom(alts)

				df := 1 - (0.2 * float64(rand.Intn(3)))
				//log.Printf("Depth: %.2f", df)
				bi.DepthFactor = df

				bi.X = (rand.Float64() * RENDER_W)
				bi.Y = (rand.Float64() * RENDER_H) * -10 // bruh
			}
		}
		{ // Throw some puny humans around
			alts, _ := LoadImageAlternatives("./graphix/alternable/puny*.png")
			const n = 200
			s.Punies = make([]Puny, n)
			for i := range s.Punies {
				bi := &s.Punies[i]

				bi.img = pickRandomImageFrom(alts)
				bi.CollisionRadius = 10
				//df := 1 - (0.2 * float64(rand.Intn(3)))
				//log.Printf("Depth: %.2f", df)
				//bi.DepthFactor = 1

				bi.X = (rand.Float64() * RENDER_W)
				bi.Y = (rand.Float64() * RENDER_H) * -10 // bruh
			}
		}
	}
	InitGameState()

	// Starts the game loop
	if err := ebi.RunGame(&G); err != nil {

		// RunGame returns error when
		// 1) OpenGL error happens,
		// 2) audio error happens or
		// 3) our (Game).Update() returns an error.

		if err == quitkey {
			log.Printf("Quit the game with a Q key")
			return
		}
		panic(err)
	}

}
