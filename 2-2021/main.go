/*
  The jam's page: https://itch.io/jam/nokiajam3
*/
package main

import (
	"image/color"
	"log"
	"math"
	"math/rand"
	"runtime"
	"strings"
	"time"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

// =======================================================================================
//   Constants and flags
// =======================================================================================

const ( // Printing and logging constants
	PRETTY_PRINT_PANICS = true
	LOG_SOUND_INIT      = true
)

var (
	RENDER_W   = float64(84) // Float is better for calculations
	RENDER_H   = float64(48)
	INIT_SCALE = 10.0 // Scaling the whole window up a bit

	THE_KEYS = []ebi.Key{ebi.KeySpace}
)

var (
	COLOR_WHITE = color.RGBA{0xc7, 0xf0, 0xd8, 0xff} // #c7f0d8
	COLOR_BLACK = color.RGBA{0x43, 0x52, 0x3d, 0xff} // #43523d
)

// =======================================================================================
//   Important globals
// =======================================================================================

var (
	G     Game      // Stuff that we could just copypaste from a savefile, potentially
	Sound GameSound // That isn't really game-state, is it
)

// Layout takes the outside size (e.g., the window size) and returns
// the (logical) screen size.
// If you don't have to adjust the screen size with the outside size,
// just return a fixed size.
func (g *Game) Layout(win_w, win_h int) (render_w, render_h int) {
	return int(RENDER_W), int(RENDER_H)
}

func (g *Game) InitializeWordbatch(earEntranceY float64) {

	var ( // @todo; Move out and set once in game

		set_abc       = LoadAnimation("grefix/abcs.json")
		set_abc_order = "abcdefghijklmnopqrstuvwxyz"
		set_num       = LoadAnimation("grefix/nums.json")
		set_num_order = "0123456789"
		set_sym       = LoadAnimation("grefix/symbols.json")
		// ...symbols are randomized, they don't need order

		wordpool []string = func(sep, str string) []string {
			str = strings.ToLower(str)
			split := strings.Split(str, sep)
			// Shuffle the split words
			rand.Shuffle(len(split), func(i, j int) {
				split[i], split[j] = split[j], split[i]
			})
			return split
		}(" . ", ""+
			"pythagoras . mitochondria . hypotenuse . paleolithic . renaissance . "+
			"concave . shakespeare . discriminant . integral . convex . "+
			"force . velocity . interpolation . refraction . abrasion . frequency . "+
			"superposition . transform . radiation . ellipse . monotheistic . "+
			"homogeneous . heterogeneous . sublimation . fusion . fission . topography"+
			" . wavelength . acceleration . latitude . longitude . hemisphere . "+
			"parallel . Doppler . ")
	)

	genWord := func(s string, importance_01 float64) (
		word *Word, letters []Letter, positions []Phys, width float64,
	) {
		s = strings.ToLower(s)

		word = &Word{
			Importance_01:     importance_01,
			ProcessedCounters: [3]int{0, 0, 0},
			DeletionTimer:     3000,
		}
		letters = make([]Letter, 0, len(s)) // Estimated the number of letters here
		positions = make([]Phys, 0, len(s))

		var offsetx float64 = 0 // Increased by letter width and spacing
		for _, char := range []byte(s) {
			ap := &AnimationPlayer{}
			if char == ' ' {
				offsetx += 3 // Just the offset to create a space, no letter created
				continue
			}
			c := string(char)

			if ix := strings.Index(set_abc_order, c); ix != -1 {
				offsetx += 4
				ap.Animation = set_abc
				ap.FrameIndex = ix
			} else if ix := strings.Index(set_num_order, c); ix != -1 {
				offsetx += 4
				ap.Animation = set_num
				ap.FrameIndex = ix
			} else {
				offsetx += 6
				ap.Animation = set_sym
				ap.FrameIndex = rand.Intn(len(set_sym.Tags[AnimationDefaultTag].Frames))
			}

			ap.SwitchToDefaultTag()
			positions = append(positions, Phys{X: offsetx})
			letters = append(letters, Letter{Word: word, Anim: ap, Char: c})
		}
		return word, letters, positions, offsetx
	}

	posx := 100.0   // Starting position for words
	padding := 20.0 // Padding between words
	for _, w := range wordpool {

		word, letters, phys, width := genWord(w, 1)

		AssertEq(len(letters), len(phys))

		for i := range letters {
			ID := NewEntityID()
			{
				letters[i].ID = ID
				g.Letters[ID] = letters[i]
			}
			{
				p := phys[i]
				p.ID = ID
				p.X += posx
				p.Y = earEntranceY
				g.Phys[ID] = p
			}
		}
		g.IncomingWords[word] = struct{}{}

		posx += width + padding // TODO: Might randomize
	}

}

func (g *Game) Initialize() {

	// Make sure rand generates different numbers every time
	rand.Seed(time.Now().UnixNano())

	// Zero the time
	g.Time = GameTime{}

	g.Cutscene = Cutscene{}

	{ // DOD maps initalizing
		g.IncomingWords = make(map[*Word]struct{})
		g.Phys = make(map[thing_id]Phys)
		g.Letters = make(map[thing_id]Letter)
	}

	// Game state
	g.CurrentScene = SCENE_Start
	g.RealtimeSpeedup = 100     // Bump up to make classes shorter
	g.FunTimeFactor = 1.0       // TODO:  Is unused, modify based on remaining seconds
	g.RemainingSecs = (45 * 60) // A full hour of school
	{
		g.Sanity.Teacher_01 = 0.5 // Start with medium value
		g.Sanity.Student_01 = 0.5 // Start with medium value

		g.Sanity.FactorAll = 1.0 // Might get modified elsewhere
		g.Sanity.FactorStudentLetters = .04
		g.Sanity.FactorTeacherTime = .2 //@test .1

	}
	{
		// Start with the headphones out of the head
		g.Headphones.CommandOutOfHead = false
		g.Headphones.IsInHead = false
		g.Headphones.Grabbie = LoadAnimationAndPlayer(FILE_grefix__cable_grab_json)
		g.Headphones.Grabbie.FrameIndex = len(g.Headphones.Grabbie.PlayedTag.Frames) - 1
		g.Headphones.Grabbie.DurationCounter = g.Headphones.Grabbie.CurrentFrame().Duration
	}

	// Hard-coded position of the ear -> determines where text lines
	// are gonna come from and where they'll stop.
	g.EarEntranceX = RENDER_W * 0.24 //0.16
	g.EarEntranceY = RENDER_H * 0.25

	// Words
	g.InitializeWordbatch(g.EarEntranceY)

	// log.Printf("Initialized Game State: --------\n%s---------\n", AsJSON(&G))
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
// If an error is passed, it will get returned by `ebi.RunGame(g)`.
func (g *Game) Update() error {
	defer GrabAndReformatPanics()

	// TODO:  Ideally we'd be locking the used maps separately
	//   and not the entire game state.
	g.Lock()
	defer g.Unlock()

	// Input
	var TheKeyDown = false
	var TheKeyDown_now = false
	{
		if ebi.IsKeyPressed(ebi.KeyQ) && runtime.GOOS != "js" {
			return quitkey
		}
		for _, k := range THE_KEYS {
			if ebi.IsKeyPressed(k) {
				TheKeyDown = true
			}
			if inpututil.IsKeyJustPressed(k) {
				TheKeyDown_now = true
			}
		}
	}

	if g.CurrentScene != SCENE_Start { // TODO:  wtf ew
		// The starting screen is the gameplay screen frozen in time
		g.Time.update()
	}
	DT := g.Time.DT

	//

	var sceneJustSwitched = g.LastFrameScene != g.CurrentScene
	g.LastFrameScene = g.CurrentScene

	switch g.CurrentScene {
	case SCENE_Playing:
		g.Cutscene.Anim = nil // Just in case

		break // That's the rest of the update, the most complicated

	case SCENE_Start:
		if TheKeyDown_now {
			g.CurrentScene = SCENE_Playing
			{
				// Try to play a sound when the player starts to kick up the web sounds,
				// autoplay might be an issue.
				// Might not actually get played but that's fine.
				MustPlay(FILE_soundz__deflect_wav)
			}
		}
		return nil

	case SCENE_Gameover_TeacherPissed:

		g.Headphones.Player.Pause()
		g.Headphones.Player.Rewind()

		// Play the cutscene
		if g.Cutscene.Anim == nil { // Load it at first
			g.Cutscene.Anim = LoadAnimationAndPlayer(FILE_grefix__gameover_json)
			g.Cutscene.GameOver = LoadAnimationAndPlayer(FILE_grefix__gameover_bar_json)
			g.Cutscene.GameOver.PlayedTag.Frames[0].Duration *= 2
			g.Cutscene.Anim.StopAtEnd()
			g.Cutscene.GameOver.StopAtEnd()
			{ // Sound effect
				soundDelay_channel := time.After(time.Millisecond * 500)
				go func() {
					<-soundDelay_channel
					MustPlay(FILE_soundz__booksmack_wav)
				}()
			}
		}

		g.Cutscene.Anim.Next(DT)
		g.Cutscene.GameOver.Next(DT)
		g.Cutscene.TotalTimeElapsed += DT

		if g.Cutscene.TotalTimeElapsed > 2 && TheKeyDown_now {
			g.Initialize() // Restart the game
		}
		return nil

	case SCENE_Gameover_StudentBoredToDeath:

		g.Headphones.Player.Pause()
		g.Headphones.Player.Rewind()

		// Play the cutscene
		if g.Cutscene.Anim == nil { // Load it at first
			g.Cutscene.Anim = LoadAnimationAndPlayer(FILE_grefix__gameover_sanity_json)
			g.Cutscene.GameOver = LoadAnimationAndPlayer(FILE_grefix__gameover_bar_json)
			g.Cutscene.GameOver.PlayedTag.Frames[0].Duration *= 2
			g.Cutscene.Anim.LoopAtEnd()
			g.Cutscene.GameOver.StopAtEnd()
			MustPlay(FILE_soundz__runaway_wav)
		}

		g.Cutscene.Anim.Next(DT)
		g.Cutscene.GameOver.Next(DT)
		g.Cutscene.TotalTimeElapsed += DT

		if g.Cutscene.TotalTimeElapsed > 2 && TheKeyDown_now {
			g.Initialize() // Restart the game
		}
		return nil

	case SCENE_Gameover_ClassEnd:

		g.Headphones.Player.Pause()
		g.Headphones.Player.Rewind()

		if g.Cutscene.Anim == nil {
			g.Cutscene.Anim = LoadAnimationAndPlayer(FILE_grefix__gameover_classover_json)
			g.Cutscene.Anim.StopAtEnd()
			MustPlay(FILE_soundz__bell_class_wav)
		}

		g.Cutscene.Anim.Next(DT)
		g.Cutscene.TotalTimeElapsed += DT

		if g.Cutscene.TotalTimeElapsed > 2 && TheKeyDown_now {
			g.Initialize() // Restart the game
		}
		return nil

	}

	//

	// Let's use the timers relative to the start of the gameplay itself
	if sceneJustSwitched {
		g.Time.TotalTime = 0
		g.Time.TotalFrames = 0
	}

	{ // Don't take too much sanity at the start
		g.Sanity.FactorAll = Clamp(g.Time.TotalTime/SecsDT(3), 0, 1)
	}

	{ // Ingame headphones audio control
		if sceneJustSwitched {
			g.Headphones.Player.Rewind()
			g.Headphones.Player.Play()
			log.Printf("playing...")
		}
		// Base the volume on how complete is the animation
		// for taking out the headphone
		vol_01 := 1 - (g.Headphones.Grabbie.Completed_01() * 0.9)
		// Set a base volume by multiplying it to the max value
		vol_01 *= 0.9
		g.Headphones.Player.SetVolume(vol_01)
	}

	{ // Class time remaining
		t := (-1) * g.Time.DT * g.RealtimeSpeedup * g.FunTimeFactor
		g.RemainingSecs += t

		if g.RemainingSecs < 0 {
			g.CurrentScene = SCENE_Gameover_ClassEnd
		}
	}

	{ // Headphone user control
		g.Headphones.CommandOutOfHead = TheKeyDown
		if g.Time.TotalTime < SecsDT(1) {
			g.Headphones.CommandOutOfHead = false
		}
		// Control logic for it comes later...
	} // and then other things that might react to it...

	// Fullscreen overlays
	for _, o := range g.OverlayLayers {
		if o.Anim != nil {
			o.Anim.Next(DT)
			o.Anim.LoopAtEnd()
		}
	}

	{ // Delete words that have been resolved already
		for w := range g.IncomingWords {

			w.DeletionTimer -= DT

			if w.DeletionTimer < 0 { // Delete the word
				// First, its individual letters
				for id, letter := range g.Letters {
					if letter.Word == w {
						delete(g.Letters, id)
						delete(g.Phys, id)
					}
				}
				// Finally, delete the word itself
				delete(g.IncomingWords, w)
			}
		}
	}

	for i, p := range g.Phys {
		p.X += p.Dx * DT
		p.Y += p.Dy * DT
		p.Rot += p.Drot * DT
		p.Scale_1 += p.Dscale * DT
		g.Phys[i] = p // Without this re-assignment we won't have any changes
	}

	{ // Sanity damage by time
		sanity := &g.Sanity
		hitvalue_01 := (g.Time.DT *
			sanity.FactorAll *
			sanity.FactorTeacherTime)
		if g.Headphones.IsInHead {
			hitvalue_01 *= -1
		}
		sanity.Teacher_01 += hitvalue_01
	}

	for id, letter := range g.Letters {
		phys := g.Phys[id]

		// Always do the timers
		letter.Timer += DT

		switch letter.EndState {
		case LETTERSTATE_INCOMING:

			// Fly towards the ear, same velocity throughout (linear)
			phys.Dx = -40
			phys.Dy = 0
			phys.Drot = 0

			if phys.X < g.EarEntranceX { // Check collision and change endstate

				letter.Timer = SecsDT(0)

				var letterState LETTERSTATE
				{ // Letter and word state updates
					if g.Headphones.IsInHead {
						letterState = LETTERSTATE_BOUNCEDOFF
					} else {
						letterState = LETTERSTATE_ASSIMILATED
					}
					letter.EndState = letterState
					letter.Word.ProcessedCounters[letterState]++
					// NOTE:  Letters get deleted in batch with the whole word
				}

				{ // Sanity damage by letter
					// TODO:  Combos and complete-word
					sanity := &g.Sanity
					var hitvalue_01 float64 = (letter.Word.Importance_01 *
						sanity.FactorAll *
						sanity.FactorStudentLetters)
					if letterState == LETTERSTATE_ASSIMILATED {
						MustPlay(FILE_soundz__jazzy_wav)
						sanity.Student_01 -= hitvalue_01
					} else {
						MustPlay(FILE_soundz__deflect_wav)
						sanity.Student_01 += hitvalue_01
					}
				}
			}

		case LETTERSTATE_ASSIMILATED:
			phys.Dx = -60
			phys.Dy = 5
			phys.Drot = 0
			phys.Dscale = -10
			// NOTE: We can lean on the letter's timer for some effects

		case LETTERSTATE_BOUNCEDOFF:
			if phys.Dy == 0 { // Do only once
				ra, rb := rand.Float64(), rand.Float64()
				phys.Dx = 10 + 50*ra   // To the right
				phys.Dy = -20 + -70*rb // Up a bit
			}
			phys.Drot = 8 //100 // Rotate the letter
			phys.Dscale = -3
			// @todo; Randomize the bounce effect
		}

		// Always update any changes in the maps
		g.Phys[id] = phys
		g.Letters[id] = letter
	}

	{ // Animating the grabby hand
		h := &g.Headphones

		// Count the state as completed only if we've completed the animation

		// NOTE: The animation starts with the headphone in head, ends with out.

		// Determine the animation direction
		// Lean on the animation system working as intended for negative numbers
		dt := DT * 2
		if h.CommandOutOfHead == false {
			h.Grabbie.Next(-dt)
		} else {
			h.Grabbie.Next(+dt)
		}
		h.Grabbie.PlaybackType = PLAYBACK_AT_END__NOTHING // Manual

		{ // Determine when the headphone should count as taken out of the head
			gr := h.Grabbie
			n, ix := float64(len(gr.PlayedTag.Frames)), float64(gr.FrameIndex)
			h.IsInHead = ix/n < 0.5
		}

		h.Grabbie.StopAtEnd() // Don't loop the grabbie
	}

	{ // Win/lose condition
		sanity := &g.Sanity

		const canEnd = true
		if canEnd {
			if sanity.Teacher_01 < 0 {
				g.CurrentScene = SCENE_Gameover_TeacherPissed
			} else if sanity.Student_01 < 0 {
				g.CurrentScene = SCENE_Gameover_StudentBoredToDeath
			}
		}

		// Put a max value to prevent overflow
		sanity.Student_01 = Clamp(sanity.Student_01, -1, 1)
		sanity.Teacher_01 = Clamp(sanity.Teacher_01, -1, 1)
	}
	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebi.Image) {
	defer GrabAndReformatPanics()
	Draw.Target = screen     // Every frame just in case it gets modified or something...
	screen.Fill(COLOR_WHITE) // Black on white works better with these colors imo

	//

	switch g.CurrentScene {
	case SCENE_Playing:
		break // Continue on

	case SCENE_Start:
		defer func() { // Draw a "[space] to start class"
			ystart := g.EarEntranceY - 5
			lineheight := 8.0
			xstart := RENDER_W * 0.4
			Draw.Letters("space to", xstart, ystart)
			Draw.Letters("start class", xstart, ystart+lineheight)
		}()
		break

	case SCENE_Gameover_TeacherPissed:
		fallthrough
	case SCENE_Gameover_ClassEnd:
		fallthrough
	case SCENE_Gameover_StudentBoredToDeath:
		if g.Cutscene.Anim != nil {
			Draw.Topleft(g.Cutscene.Anim.Sprite(), Draw.Ops(0, 0, 0))
		}
		if g.Cutscene.GameOver != nil {
			effDur := g.Cutscene.GameOver.PlayedTag.Frames[0].Duration
			hmm := 2.0
			totalTime := g.Cutscene.TotalTimeElapsed - effDur/hmm
			offset := RENDER_W
			f := Clamp(totalTime*hmm/effDur, 0, 1)
			comeInX := -offset + f*offset
			Draw.Topleft(g.Cutscene.GameOver.Sprite(), Draw.Ops(comeInX, 0, 0))
		}
		return // Don't draw anything else
	}

	//

	// Background bar
	Draw.Topleft(G.BackgroundBar, Draw.Ops(0, 0, 0))

	{ // Sanity
		sanity := g.Sanity
		var (
			w             = 29.0 // Width of the bar, gotta hardcode
			step          = 3.0
			offsetTeacher = math.Round((sanity.Teacher_01-1)*w/step) * step
			offsetStudent = math.Round((sanity.Student_01-1)*w/step) * step
		)
		Draw.Topleft(G.Sanity.BarTeacher, Draw.Ops(-offsetTeacher, 0, 0))
		Draw.Topleft(G.Sanity.BarStudent, Draw.Ops(+offsetStudent, 0, 0))
		Draw.Topleft(G.Sanity.Overlay, Draw.Ops(0, 0, 0))
	}

	// Fullscreen overlays
	topleftCorner := Draw.Ops(0, 0, 0)
	for _, o := range G.OverlayLayers {
		if o.Anim != nil {
			Draw.Topleft(o.Anim.Sprite(), topleftCorner)
		} else if o.Img != nil {
			Draw.Topleft(o.Img, topleftCorner)
		}
	}

	{ // Drawing the current time
		minutes := g.RemainingSecs / 60
		off_x, off_y := RENDER_W-8.0, RENDER_H-6.0

		except := g.CurrentScene == SCENE_Start

		if _, frac := math.Modf(minutes / 10); frac > 0.05/10 {
			tens := int(minutes / 10)
			Draw.Number(tens, (off_x), off_y)
		}
		if _, frac := math.Modf(minutes); frac > 0.05 || except {
			ones := int(minutes) % 10
			Draw.Number(ones, (off_x + 4), off_y)
		}
	}

	// Letters
	for id, letter := range g.Letters {
		phys := g.Phys[id] // Note: Don't modify the entries here, we're in draw

		if phys.X > RENDER_W*1.2 {
			continue // Don't render offscreen letters
			// (might be optimized by the lib tho)
		}

		var (
			img   = letter.Anim.Sprite()
			x, y  = math.Floor(phys.X), math.Floor(phys.Y)
			scale = Clamp(phys.Scale_1+1, 0, 1)
			geom  = ebi.GeoM{}
		)
		if scale != 1 {
			geom.Scale(scale, scale)
		}
		geom.Translate(x, y)
		Draw.Topleft(img, &ebi.DrawImageOptions{GeoM: geom})
	}

	// Animated grabby hand
	Draw.Topleft(G.Headphones.Grabbie.Sprite(), topleftCorner)

}

func (g *Game) LoadInAssets() {

	{ // The banger
		plr := LoadMP3(FILE_soundz__the_song_mp3)
		g.Headphones.Player.Player = plr
	}

	// NOTE:  Sound effects are all loaded kinda on program load so
	//   we don't need to worry about loading them here.

	// Numbers for quick printing
	g.NumberAnimation = LoadAnimation(FILE_grefix__nums_light_json)
	g.LetterAnimation = LoadAnimation(FILE_grefix__abcs_json)

	{ // Sanity HUD
		g.Sanity.Overlay = LoadImage(FILE_grefix__sanity_bar_png)
		g.Sanity.BarStudent = LoadImage(FILE_grefix__sanity_bar_sl_student_png)
		g.Sanity.BarTeacher = LoadImage(FILE_grefix__sanity_bar_sl_teacher_png)
	}

	{ // Fullscreen overlays
		// In case we need a more complicated draw order,
		// we'll leave this out of layers.
		g.BackgroundBar = LoadImage(FILE_grefix__bottom_bar_png)

		// Order determines the zindex here, last in list get drawn on top
		var layers = [][2]string{ // "path", "default tag"
			{FILE_grefix__ear_png, ""},            // Ear under the headphone
			{FILE_grefix__player_json, "playing"}, // Cassette
		}
		g.OverlayLayers = make([]Overlay, len(layers))
		for i := range layers {
			path := layers[i][0]
			if strings.HasSuffix(path, ".json") {
				// Import as animation
				tag := layers[i][1]
				a := LoadAnimationAndPlayer(path)
				a.SwitchToDefaultTag()
				if tag != "" {
					a.SwitchToTag(tag)
				}
				g.OverlayLayers[i] = Overlay{Anim: a}
			} else {
				// Import as static image
				g.OverlayLayers[i] = Overlay{Img: LoadImage(path)}
			}
		}
	}

}

func Init() error {

	InitLogger()

	InitSound()
	G.Lock()
	{
		G.LoadInAssets()
		G.Initialize()
	}
	G.Unlock()

	return nil
}

func main() {
	defer GrabAndReformatPanics()

	{ // Window settings
		ebi.SetWindowSize(int(RENDER_W*INIT_SCALE), int(RENDER_H*INIT_SCALE))
		ebi.SetWindowTitle("Nokia Jam 3")
		ebi.SetWindowResizable(true)
	}

	canPanic(Init())

	// Starts the game loop
	err := ebi.RunGame(&G)
	if err != nil {

		// RunGame returns error when
		// 1) OpenGL error happens,
		// 2) audio error happens or
		// 3) our (*Game).Update() returns an error.

		if err == quitkey {
			log.Printf("Quit the game with a Q key")
			return
		}
		panic(err)
	}
}
