/**

Whenever we need to do some quick testing of behavior or time some potentially
performance-heavy things, we can do it here.


Tests are called with  `go test -run   TestXyxy`
and benchmarks with    `go test -bench BenchmarkXyxy`.

All runnable tests should be functions named `func TestXyxy(t *testing.T)`
and benchmarks `func BenchmarkXyxy(b *testing.B)`.


If you need to see stack traces from tests, panic. Logging with `t.Log` only
shows up if the tested cases fail, giving you context only for failed tests.
Use regular logs if you need context for passing cases too.

In benchmarks, you should always use the `b.N` to allow the benchmark
to be repeated as many times as it's necessary to get reliably consistent
times across the checked range.

**/

package main

import (
	"log"
	"testing"
)

func BenchmarkLogging(b *testing.B) {
	InitLogger()
	defer GrabAndReformatPanics()
	for i := 0; i < b.N; i++ {
		log.Printf("test")
	}
}
