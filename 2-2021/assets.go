package main

import (
	"fmt"
	"image"
	_ "image/png"

	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"

	"encoding/json"
	"log"
	"time"

	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/mp3"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
)

// Loading files, either from the binary itself as constants' data
// or from the filesystem directly.

type assets_ struct{}

var assets assets_

func (a assets_) LoadBytes(path string) []byte {
	if runtime.GOOS == "js" {
		return _assetData[path]
	} else {
		bytes, err := ioutil.ReadFile(path)
		if err != nil {
			panic(err)
		}
		return bytes
	}
}

//

func LoadMP3(path string) *audio.Player {
	data := assets.LoadBytes(path)
	decoded, err := mp3.Decode(Sound.Ctx, bytes.NewReader(data))
	canPanic(err)
	player, err := audio.NewPlayer(Sound.Ctx, decoded)
	canPanic(err)
	return player
}

func loadSound(path string) (*wav.Stream, error) {
	data := assets.LoadBytes(path)
	decoded, err := wav.Decode(Sound.Ctx, bytes.NewReader(data))
	if err != nil {
		return nil, fmt.Errorf("Couldn't load file %s : %v", path, err)
	}
	return decoded, nil
}

// Get a sound player to play back
func FreePlayer(soundname string) *audio.Player {
	spams, ok := Sound.Wavs[soundname]
	if !ok {
		panic(simpleError("unknown soundname `" + soundname + "`"))
	}

	for _, p := range spams.players {
		if p.IsPlaying() == false {
			return p
		}
	}
	return nil
}
func MustPlay(soundname string) {
	plr := FreePlayer(soundname)
	if plr == nil {
		// There's no free player, gotta grab one that's not done yet
		plr = Sound.Wavs[soundname].players[0]
	}
	plr.Rewind()
	plr.Play()
}

// Prepare sound for playback
func InitSound() {
	if LOG_SOUND_INIT {

		log.Print("Importing .wav files ...")

		tstart := time.Now()
		defer func() {
			tdiff := time.Now().Sub(tstart)

			log.Printf("Done.  (loaded in %.6fs)", DurationToDt(tdiff))
		}()
	}

	// Initializing the audio context with a sample rate that we'll
	// be using for all our sounds. The sample rate can't/won't change.
	Sound.Ctx = audio.NewContext(44100)

	// Loading all sounds in the asset folder and initializing
	// their players into a map.
	Sound.Wavs = make(map[string]SpammableSound)

	// Importing all .wav files in the sounds folder
	var wavFilepaths []string = _assetGlobGroups[2]
	for _, name := range wavFilepaths {

		// Note: Player source data can't be shared between players,
		//  we have to copy the data for every player that wants the data.

		// Determines how many concurrent plays this sound can have
		var nChannels = 2
		spams := SpammableSound{
			players: make([]*audio.Player, 0, nChannels),
		}

		// Loading the bytes of the file
		wavdata := assets.LoadBytes(name)
		// Creating multiple readers on the same bit of data
		for i := 0; i < nChannels; i++ {
			reader := bytes.NewReader(wavdata)

			// Sadly, unless we pry open the API we have to decode
			// the same bit of data multiple times. Oh well.
			decoded, err := wav.Decode(Sound.Ctx, reader)
			canPanic(err)
			plr, err := audio.NewPlayer(Sound.Ctx, decoded)
			canPanic(err)

			spams.players = append(spams.players, plr)
		}

		Sound.Wavs[name] = spams
	}

}

//
//  Graphics
//

type draw_ struct {
	// The target image for all drawing done through the variable
	Target *ebi.Image
}

var Draw draw_ // @package

func (d draw_) Topleft(what *ebi.Image, op *ebi.DrawImageOptions) {
	d.Target.DrawImage(what, op) // That's the default, yo!
}
func (d draw_) Centered(what *ebi.Image, op *ebi.DrawImageOptions) {
	w, h := ImageSize(what)
	{ // Apply our recentering before all other transformations
		g := ebi.GeoM{}
		g.Translate(w/2, h/2)
		g.Concat(op.GeoM)
		op.GeoM = g
	}
	d.Target.DrawImage(what, op)
}

func (_ draw_) Ops(x, y, rot float64) *ebi.DrawImageOptions {
	g := ebi.GeoM{}
	g.Rotate(rot)
	g.Translate(x, y)
	return &ebi.DrawImageOptions{GeoM: g}
}

//
//  Image alternatives (potentially just a worse way than packing it in animations)
//

func LoadImageAlternatives(glob string) ([]*ebi.Image, []string) {
	altsPaths, err := filepath.Glob(glob)
	canPanic(err)

	alts := make([]*ebi.Image, len(altsPaths))

	for i, filepath := range altsPaths {
		s := LoadImage(filepath)
		alts[i] = s
	}
	return alts, altsPaths
}

func PickRandomImageFrom(alts []*ebi.Image) *ebi.Image {
	return alts[rand.Intn(len(alts))]
}

//
//  Reloadable assets
//

type ReloadableAsset interface {
	RecordLoaded()
	HasNewerVersion() bool

	Reload() // File reload function needs to be filled by subtypes
}

var watchedAssets []ReloadableAsset

func WatchAssetChanges(a ReloadableAsset) {
	watchedAssets = append(watchedAssets, a)
}
func MaybeReloadWatchedAssets() {
	// Check and optionally reload assets at runtime
	for _, wa := range watchedAssets {
		if wa.HasNewerVersion() {
			wa.Reload()
		}
	}
}

type Asset struct {
	WatchedFilepath string
	lastLoaded      time.Time
}

func (a *Asset) RecordLoaded() {
	a.lastLoaded = time.Now()
}
func (a Asset) HasNewerVersion() bool {
	if runtime.GOOS == "js" {
		return false // These never change as they're not on disk
	}

	s, err := os.Stat(a.WatchedFilepath)
	if err != nil {
		log.Printf("Warn: Couldn't read file's modtime: `%s`\n%s\n", a.WatchedFilepath, err)
		return false // Assume not changed on os error -- can't read it anyway
	}
	return s.ModTime().After(a.lastLoaded)
}

//
//  Sprites, single images
//

type Sprite struct {
	Asset
	*ebi.Image
}

func loadImage(path string) (*ebi.Image, error) {
	data := assets.LoadBytes(path)
	img, _, err := image.Decode(bytes.NewReader(data))
	if err != nil {
		return nil, fmt.Errorf("Couldn't decode image `%s`, %d : %v",
			path, len(data), err)
	}
	return ebi.NewImageFromImage(img), nil
}
func LoadImage(path string) *ebi.Image {
	img, err := loadImage(path)
	canPanic(err)
	return img
}

//
//  Animation player
//

// A wrapper around the animation to allow playback.
type AnimationPlayer struct {
	Animation       *Animation // Controlled animation data, can be shared
	FrameIndex      int        // Index of currently played frame
	DurationCounter SecsDT     // Playback control, counter for a single frame, gets reset
	PlayedTag       *Tag       // Currently played tag of the animation
	PlaybackType    PlaybackType_
}
type PlaybackType_ int

const (
	PLAYBACK_AT_END__LOOP PlaybackType_ = iota
	PLAYBACK_AT_END__STOP
	PLAYBACK_AT_END__NOTHING
)

// Get the current sprite, as defined by the current frame of the player
func (pl AnimationPlayer) Sprite() *ebi.Image {
	return pl.CurrentFrame().SubImage
}

// Get the current frame, as defined by player's timers and played tag
func (pl AnimationPlayer) CurrentFrame() Frame {
	return pl.FrameByIndex(pl.FrameIndex)
}

// Get a frame by its index in the currently played tag
func (pl AnimationPlayer) FrameByIndex(frameIndex int) Frame {
	return pl.PlayedTag.Frames[frameIndex]
}

// Detect the end and start of the animation, the rest of the data
// might not be correct.
// TODO:  Improve the API here.
func (pl AnimationPlayer) Completed_01() float64 {
	return float64(pl.FrameIndex) / float64(len(pl.PlayedTag.Frames)-1)
}

// Find a tag by name and switch to it. Nothing done to the FrameIndex.
func (pl *AnimationPlayer) SwitchToTag(tagName string) *AnimationPlayer {
	tag, tagFound := pl.Animation.Tags[tagName]
	if !tagFound {
		log.Printf("Error switching to tag %s::%s; ignoring switch...\n",
			pl.Animation.Asset.WatchedFilepath, tagName)
		return pl
	}
	pl.PlayedTag = tag
	return pl
}

const AnimationDefaultTag = "default"

func (pl *AnimationPlayer) TagByName(tagName string) *Tag {
	return pl.Animation.Tags[tagName]
}
func (pl *AnimationPlayer) TagDefault() *Tag {
	return pl.Animation.Tags[AnimationDefaultTag]
}

// Switch to the default aseprite tag, playing the full animation.
func (pl *AnimationPlayer) SwitchToDefaultTag() *AnimationPlayer {
	// @todo; Played tag should be denoted by an index, this is kind of
	//   weird innit. Just use the fucking index, slut
	pl.PlayedTag = pl.Animation.Tags[AnimationDefaultTag]
	return pl
}

// Start the animation; reset the frame index.
func (pl *AnimationPlayer) Start() *AnimationPlayer {
	pl.FrameIndex = 0
	pl.DurationCounter = 0
	return pl
}

// Advance the animation frame by one unconditionally.
func (pl *AnimationPlayer) NextFrame() *AnimationPlayer {
	pl.FrameIndex++
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations; Supports going into negative numbers.
// NOTE:  Does not support skipping multiple frames in one call.
func (pl *AnimationPlayer) Next(dt SecsDT) *AnimationPlayer {

	if dt > 0 {
		pl.DurationCounter += dt

		var nextSwitch SecsDT = pl.CurrentFrame().Duration
		if pl.DurationCounter >= nextSwitch {
			// Careful about carrying the overflow into the next frame
			pl.DurationCounter -= nextSwitch
			pl.FrameIndex++
		}
	} else {
		pl.DurationCounter += dt // will be negative so it's the same

		// Next switch is trivial
		if pl.DurationCounter < 0 {
			// Leave the right overflow when going backwards
			var prevSwitch SecsDT = pl.FrameByIndex(pl.FrameIndex).Duration
			pl.DurationCounter += prevSwitch
			pl.FrameIndex--
		}
	}

	{ // What to do if we're at the end of an animation
		var (
			high = len(pl.PlayedTag.Frames) - 1
			low  = 0
		)
		switch pl.PlaybackType {
		case PLAYBACK_AT_END__NOTHING:
			break
		case PLAYBACK_AT_END__LOOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = low
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = high
			}
		case PLAYBACK_AT_END__STOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = high
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = low
			}
		}
	}

	// Note: Using `Next()` directly can overflow
	return pl
}

// If past the end, loop back to the start.
func (pl *AnimationPlayer) LoopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__LOOP
	return pl
}

// If past the end, stop at the end.
func (pl *AnimationPlayer) StopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__STOP
	return pl
}

//
//  Animation data, imports from Aseprite
//

// @todo; Add a way of adding sound-trigger data into the animations

// Note: Aseprite assets have two key files, the json and the png.
// The json includes all data about frame regions and tags (what regions
// belong to which animation).
// The json also has the filepath of the png, relative to the json file.

type (
	Animation struct {
		Asset                 // The animation is watchable and reloadable at runtime
		Tags  map[string]*Tag // Sub-animations included in the asset
		Sheet *ebi.Image      `json:"-"` // Image data, all frames must be in this sheet
	}
	Tag struct {
		Name   string  // Name of the tag as defined by the Aseprite export
		Frames []Frame // Frame data containing the image
	}
	Frame struct {
		Duration SecsDT     // Duration for the frame
		SubImage *ebi.Image // Part of the spreadsheet corresponding to this frame
	}
)

func LoadAnimation(abspath string) *Animation {
	var a Animation
	a.Asset.WatchedFilepath = abspath
	a.Reload()
	return &a
}
func LoadAnimationAndPlayer(abspath string) *AnimationPlayer {
	// Note: Use this only for single-occurence animations,
	//   they are obviously not reused. Don't load animations multiple
	//   times please.
	ap := &AnimationPlayer{Animation: LoadAnimation(abspath)}
	ap.SwitchToDefaultTag() // Might not be enabled
	return ap
}

func (a *Animation) Reload() {
	defer a.RecordLoaded()

	var aseJson AseJSON
	{
		jsonBytes := assets.LoadBytes(a.Asset.WatchedFilepath)
		err := json.Unmarshal(jsonBytes, &aseJson)
		canPanic(err)
	}

	var spritesheet *ebi.Image
	{
		aseJsonDir := filepath.Dir(a.Asset.WatchedFilepath)
		loadPath := filepath.Join(aseJsonDir, aseJson.Meta.Image)

		spritesheet = LoadImage(loadPath)
	}

	animation := animationFromAseprite(aseJson, spritesheet)

	// Change the content of the animation, always use the same
	// variable when live-reloading. GC should sweep up the old overwritten data.
	a.Sheet = animation.Sheet
	a.Tags = animation.Tags
}

// The Aseprite editor's metadata format that is saved alongside
// an exported sprite sheet. It has all the information for putting
// all of the information back together as an animation or atlas.
//
// Deciding what goes in which field is done with default values,
// we don't specify the `json:"field_name"` tags for the fields
// and it works fine.
//
// @todo; Read triggers from the animation (sounds, events...)
// @todo; Read positions from single-pixel layers somehow
// @todo; Read slices for atlas usage
type (
	AseJSON struct {
		Meta   aseMeta    // File properties, sizes and individual tags
		Frames []aseFrame // Frame bounds and durations
	}
	aseMeta struct {
		Image     string        // Filename of the spritesheet image
		Size      aseSize       // Size of the spritesheet
		FrameTags []aseFrameTag // Individual animations
	}
	aseFrameTag struct {
		Name string
		From int
		To   int
	}
	aseFrame struct {
		Duration int     // Duration in ms
		Frame    aseRect // Spritesheet's cutout region
	}
	aseRect struct {
		X, Y, W, H int
	}
	aseSize struct {
		W, H int
	}
)

// Convert aseprite export metadata into a usable animation struct
func animationFromAseprite(ase AseJSON, spritesheet *ebi.Image) *Animation {

	// Extract a tag out of a picture and aseFrame data
	tagFromRange := func(name string, sheet *ebi.Image, aseFrames []aseFrame) Tag {
		var frames = make([]Frame, len(aseFrames))
		for index, it := range aseFrames {

			var r aseRect = it.Frame
			region := image.Rect(r.X, r.Y, r.X+r.W, r.Y+r.H)

			var millis = float64(it.Duration)
			var duration SecsDT = millis / 1000

			frames[index] = Frame{
				SubImage: sheet.SubImage(region).(*ebi.Image),
				Duration: duration,
			}
		}
		return Tag{Name: name, Frames: frames}
	}

	tags := make(map[string]*Tag)
	{ // Always include the full animation as a default tag
		var allFrames = ase.Frames[:]
		var tag = tagFromRange(AnimationDefaultTag, spritesheet, allFrames)
		tags[AnimationDefaultTag] = &tag
	}
	// Add the individual frame tags set in Aseprite
	for _, t := range ase.Meta.FrameTags {
		var frames = ase.Frames[t.From : t.To+1]
		var tag = tagFromRange(t.Name, spritesheet, frames)
		tags[t.Name] = &tag
	}

	return &Animation{
		Sheet: spritesheet,
		Tags:  tags,
	}
}
