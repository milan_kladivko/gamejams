
package main

// GENERATED ASSETS GO HERE

// Filenames listed as constants for autocompleting
const (
    FILE_grefix__abcs_light_json = "grefix/abcs-light.json"
    FILE_grefix__abcs_light_png = "grefix/abcs-light.png"
    FILE_grefix__abcs_json = "grefix/abcs.json"
    FILE_grefix__abcs_png = "grefix/abcs.png"
    FILE_grefix__ase__bonk_png = "grefix/ase/bonk.png"
    FILE_grefix__ase__favicon_png = "grefix/ase/favicon.png"
    FILE_grefix__ase__favicon2_png = "grefix/ase/favicon2.png"
    FILE_grefix__ase__gameover_png = "grefix/ase/gameover.png"
    FILE_grefix__ase__nichirun_png = "grefix/ase/nichirun.png"
    FILE_grefix__bottom_bar_png = "grefix/bottom_bar.png"
    FILE_grefix__cable_grab_json = "grefix/cable_grab.json"
    FILE_grefix__cable_grab_png = "grefix/cable_grab.png"
    FILE_grefix__ear_png = "grefix/ear.png"
    FILE_grefix__gameover_bar_json = "grefix/gameover-bar.json"
    FILE_grefix__gameover_bar_png = "grefix/gameover-bar.png"
    FILE_grefix__gameover_classover_json = "grefix/gameover-classover.json"
    FILE_grefix__gameover_classover_png = "grefix/gameover-classover.png"
    FILE_grefix__gameover_sanity_json = "grefix/gameover-sanity.json"
    FILE_grefix__gameover_sanity_png = "grefix/gameover-sanity.png"
    FILE_grefix__gameover_json = "grefix/gameover.json"
    FILE_grefix__gameover_png = "grefix/gameover.png"
    FILE_grefix__hit_block_1_json = "grefix/hit-block-1.json"
    FILE_grefix__hit_block_1_png = "grefix/hit-block-1.png"
    FILE_grefix__hit_block_2_json = "grefix/hit-block-2.json"
    FILE_grefix__hit_block_2_png = "grefix/hit-block-2.png"
    FILE_grefix__hit_block_3_json = "grefix/hit-block-3.json"
    FILE_grefix__hit_block_3_png = "grefix/hit-block-3.png"
    FILE_grefix__hit_block_4_json = "grefix/hit-block-4.json"
    FILE_grefix__hit_block_4_png = "grefix/hit-block-4.png"
    FILE_grefix__hit_block_5_json = "grefix/hit-block-5.json"
    FILE_grefix__hit_block_5_png = "grefix/hit-block-5.png"
    FILE_grefix__hit_ear_block_json = "grefix/hit-ear-block.json"
    FILE_grefix__hit_ear_block_png = "grefix/hit-ear-block.png"
    FILE_grefix__hit_get_1_json = "grefix/hit-get-1.json"
    FILE_grefix__hit_get_1_png = "grefix/hit-get-1.png"
    FILE_grefix__nums_light_json = "grefix/nums-light.json"
    FILE_grefix__nums_light_png = "grefix/nums-light.png"
    FILE_grefix__nums_json = "grefix/nums.json"
    FILE_grefix__nums_png = "grefix/nums.png"
    FILE_grefix__player_json = "grefix/player.json"
    FILE_grefix__player_png = "grefix/player.png"
    FILE_grefix__sanity_bar_png = "grefix/sanity-bar.png"
    FILE_grefix__sanity_bar_sl_student_png = "grefix/sanity-bar_sl-student.png"
    FILE_grefix__sanity_bar_sl_teacher_png = "grefix/sanity-bar_sl-teacher.png"
    FILE_grefix__symbols_light_json = "grefix/symbols-light.json"
    FILE_grefix__symbols_light_png = "grefix/symbols-light.png"
    FILE_grefix__symbols_json = "grefix/symbols.json"
    FILE_grefix__symbols_png = "grefix/symbols.png"
    FILE_soundz__bell_class_wav = "soundz/bell_class.wav"
    FILE_soundz__booksmack_wav = "soundz/booksmack.wav"
    FILE_soundz__byoom_wav = "soundz/byoom.wav"
    FILE_soundz__deflect_wav = "soundz/deflect.wav"
    FILE_soundz__jazzy_wav = "soundz/jazzy.wav"
    FILE_soundz__nokiatune_kick_wav = "soundz/nokiatune_kick.wav"
    FILE_soundz__runaway_wav = "soundz/runaway.wav"
    FILE_soundz__the_song_mp3 = "soundz/the_song.mp3"
)

// Groups by globs, indexed by whatever inclusion globs have been
// used as the argument when loading.
var _assetGlobGroups = [][]string{ 
    { // from '**.json'
        FILE_grefix__abcs_light_json,
        FILE_grefix__abcs_json,
        FILE_grefix__cable_grab_json,
        FILE_grefix__gameover_bar_json,
        FILE_grefix__gameover_classover_json,
        FILE_grefix__gameover_sanity_json,
        FILE_grefix__gameover_json,
        FILE_grefix__hit_block_1_json,
        FILE_grefix__hit_block_2_json,
        FILE_grefix__hit_block_3_json,
        FILE_grefix__hit_block_4_json,
        FILE_grefix__hit_block_5_json,
        FILE_grefix__hit_ear_block_json,
        FILE_grefix__hit_get_1_json,
        FILE_grefix__nums_light_json,
        FILE_grefix__nums_json,
        FILE_grefix__player_json,
        FILE_grefix__symbols_light_json,
        FILE_grefix__symbols_json,  
    }, 
    { // from '**.png'
        FILE_grefix__abcs_light_png,
        FILE_grefix__abcs_png,
        FILE_grefix__ase__bonk_png,
        FILE_grefix__ase__favicon_png,
        FILE_grefix__ase__favicon2_png,
        FILE_grefix__ase__gameover_png,
        FILE_grefix__ase__nichirun_png,
        FILE_grefix__bottom_bar_png,
        FILE_grefix__cable_grab_png,
        FILE_grefix__ear_png,
        FILE_grefix__gameover_bar_png,
        FILE_grefix__gameover_classover_png,
        FILE_grefix__gameover_sanity_png,
        FILE_grefix__gameover_png,
        FILE_grefix__hit_block_1_png,
        FILE_grefix__hit_block_2_png,
        FILE_grefix__hit_block_3_png,
        FILE_grefix__hit_block_4_png,
        FILE_grefix__hit_block_5_png,
        FILE_grefix__hit_ear_block_png,
        FILE_grefix__hit_get_1_png,
        FILE_grefix__nums_light_png,
        FILE_grefix__nums_png,
        FILE_grefix__player_png,
        FILE_grefix__sanity_bar_png,
        FILE_grefix__sanity_bar_sl_student_png,
        FILE_grefix__sanity_bar_sl_teacher_png,
        FILE_grefix__symbols_light_png,
        FILE_grefix__symbols_png,  
    }, 
    { // from '**.wav'
        FILE_soundz__bell_class_wav,
        FILE_soundz__booksmack_wav,
        FILE_soundz__byoom_wav,
        FILE_soundz__deflect_wav,
        FILE_soundz__jazzy_wav,
        FILE_soundz__nokiatune_kick_wav,
        FILE_soundz__runaway_wav,  
    }, 
    { // from '**.mp3'
        FILE_soundz__the_song_mp3,  
    },
}

// Raw binary data of the assets.
// OMMITED. Apply the "-include-data" argument.
var _assetData = map[string][]byte{}

