
# Ebiten tutorial: 
#   https://ebiten.org/documents/webassembly.html


# Don't clutter the root more than it already is
mkdir build_web

echo "Source build..."

echo "  Generating asset embedding source code"
mv gen_assets.go gen_assets.go.old         # Back up the old file
go run build_web/filegen.go -include-data  # Don't forget to include the byte data

echo "  Compiling the Go code into webassembly"
GOOS=js GOARCH=wasm go build -o build_web/bin.wasm

echo "  Rolling back the assets file"
mv gen_assets.go.old gen_assets.go         # Reset.



echo "Copying the webassembly loader javascript code"
cp $(go env GOROOT)/misc/wasm/wasm_exec.js build_web/.

echo "Writing the entrypoint index.html file"
cat > build_web/index.html <<EOF

<!DOCTYPE html>
<script src="wasm_exec.js"></script>
<script>
 
 // Polyfill
 if (!WebAssembly.instantiateStreaming) {
     WebAssembly.instantiateStreaming = async (resp, importObject) => {
         const source = await (await resp).arrayBuffer();
         return await WebAssembly.instantiate(source, importObject);
     };
 }

 const go = new Go();
 WebAssembly.instantiateStreaming(fetch("bin.wasm"), go.importObject).then(result => {

     // Hide the loading heads-up
     document.getElementById("LOADING").remove()

     // Run the game code
     go.run(result.instance);
     
 });
</script>



<div id="LOADING"
     style="font-family:monospace; text-align:center; color:#43523d; padding:20px;">
  <div style="font-size:32px">. . . LOADING . . .</div>
  <div style="font-size:16px">Sorry, we don't have a fancy slider...</div>
</div>

<style>
 body {
     height: 100%; height: 100vh;
     padding: 0; margin: 0;
     box-sizing: border-box;
     border: 4px solid #932aca;
 }
 body.in-focus {
     border: 4px solid #43523d;
 }
</style>
<script>
 function onchange() {
     var cls = "in-focus" 
     if (document.hasFocus()) {
         document.body.classList.add(cls)
     } else {
         document.body.classList.remove(cls)
     }
 }
 // We really need it to be right
 window.addEventListener("focus", onchange)
 window.addEventListener("blur", onchange)
 onchange()
 setTimeout(onchange, 1000)
 
</script>

EOF

echo "Zipping the files"
cd build_web
zip build_web.zip   bin.wasm   wasm_exec.js   index.html
