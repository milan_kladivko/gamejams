package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	C "github.com/fatih/color"

	// TODO:  This is slow for pretty writes, we should change this to something
	//   a bit more preformated or something. It's too long anyway, right?
	"encoding/json"

	"fmt"
	"log"
	"strings"
	"time"
)

// --------------------------------------------------------------------------------------
// Getting rid of the ridiculously long timestamps in logging

type logWriter struct{}

func (logWriter) Write(bytes []byte) (int, error) {
	var timestamp string
	{ // Use a timestamp prefix
		now := time.Now()
		minuteSecond := now.Format("04'05\"")
		millis := now.Format(".999")

		// Align milliseconds, pad with zeroes on the right
		if millis != "" { // Can just be empty if we have exact seconds
			millis = millis[1:] // Don't include the dot
		}
		millis = millis + strings.Repeat("0", 3-len(millis))

		const WITH_COLOR = true
		if WITH_COLOR {
			timestamp = C.YellowString("%s%s ║ ", minuteSecond, millis)
		} else {
			timestamp = fmt.Sprintf("%s%s ║ ", minuteSecond, millis)
		}
	}
	return fmt.Print(timestamp + string(bytes))
}

func InitLogger() {
	log.SetFlags(0) // Disabling to the barebones output so we can have fun ourselves
	log.SetOutput(new(logWriter))
	log.Printf("Starting the game...")
}

//
// --------------------------------------------------------------------------------------

// Get a formatted JSON printout of all fields in a `thing`.
// This doesn't follow pointers so it's kinda useless if there's
// data in a thing behind a pointer, you'll just get the pointer
// value as hex -- very meh.
func AsJSON(thing interface{}) string {
	bytes, err := json.MarshalIndent(thing, "", "  ")
	canPanic(err)
	return string(bytes)
}

// Clamp returns x clamped to the interval [min, max].
func Clamp(x, min, max float64) float64 {
	switch {
	default:
		return x
	case x < min:
		return min
	case x > max:
		return max
	}
}

func ImageSize(i *ebi.Image) (wid, hei float64) {
	b := i.Bounds()
	return float64(b.Dx()), float64(b.Dy())
}

func BoolToSign(b bool) float64 {
	if b {
		return 1
	} else {
		return 0
	}
}

// An empty geometry matrix with a position.
func GeomXY(x, y float64) (g ebi.GeoM) {
	g.Translate(x, y)
	return g
}

func AssertEq(a, b int) {
	if a != b {
		panic("assert :: not equal")
	}
}

func (draw_) Number(n int, x, y float64) {
	nums := G.NumberAnimation.Tags[AnimationDefaultTag].Frames
	Draw.Topleft(nums[n].SubImage, Draw.Ops(x, y, 0))
}
func (draw_) Letters(s string, x, y float64) {
	var (
		set_abc_order = "abcdefghijklmnopqrstuvwxyz"
		letters       = G.LetterAnimation.Tags[AnimationDefaultTag].Frames
		offx          = 0.0
	)
	for _, charByte := range []byte(s) {
		char := string(charByte)
		index := strings.Index(set_abc_order, char)
		if index >= 0 {
			img := letters[index].SubImage
			Draw.Topleft(img, Draw.Ops(offx+x, y, 0))
			offx += 4
		} else if char == " " {
			offx += 3
		} else {
			log.Printf("LETTER DRAW ERR : unsupported char `%s`", char)
		}
	}
}

func (t *GameTime) update() {

	now := time.Now()
	dt := now.Sub(t.FrameStart)
	t.FrameStart = now

	// Clamp to some small-ish value when severely lagging.
	dt_max := time.Millisecond * 200
	if dt > dt_max {
		dt = dt_max
	}

	if false {
		log.Printf("dt:  %.2fms", dt.Seconds()*1000)
		log.Printf("FPS: %.2f", ebi.CurrentFPS()) // Gets counted in the lib itself
	}

	t.DT = dt.Seconds() // Save into the global for use everywhere, don't pass it
	t.TotalTime += t.DT
	t.TotalFrames += 1 // Assuming one update per frame here
}
