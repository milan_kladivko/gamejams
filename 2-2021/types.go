package main

import (
	"sync"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"

	"image/color"
	"math/rand"
	"time"
)

type Game struct {
	// Embed the game systems that should be serialized here
	GameState
	Time     GameTime
	Cutscene Cutscene
}

type GameTime struct {
	// Timestamp of current frame's `Update()` call
	FrameStart time.Time

	DT          SecsDT // Difference in times between current and last frame's `Update()`
	TotalTime   SecsDT // Total number of elapsed time since the game's start
	TotalFrames int64  // Total number of drawn frames since the game's start
}

// In the entire project, we'll be using the same DT unit.
// Working with math is weird enough, no point in throwing in nanos
// that `time.Duration` uses. Not good for games.
// This is syntax for being able to still use `float64` for times
// interchangably, but mentioning `SecsDT` where appropriate is preferrable.
type SecsDT = float64

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function.
func DurationToDt(d time.Duration) SecsDT {
	// Note: Yes, this doesn't truncate the seconds. It returns the fractional
	// conversion of nanoseconds to seconds.
	return d.Seconds()
}

// NOTE:
//   This time around, I'd like to keep a DOD (Data-Oriented-Desing)
//   style for game objects. This approach is very database-like
//   in that every system has its "table" with domain-specific fields
//   and always has an ID to go with every entry.
//   This means that instead of reuse in terms of objects and OOP
//   (weird to do in Go anyway), the data is "normalized", "cache-local"
//   and in one place when doing a specific task.
//   Usually we'll be passing around IDs and arrays of IDs instead of
//   pointers. It's a paradigm full of queues, arrays and doing things
//   in batches for best results.
//   In terms of performance it is a definitive win but thinking about
//   data this way is a lot different from OOP and takes getting used to.
//   Look at the data structs used and you'll get the idea.

// Entity ID, just a random number to use instead of pointers in structs
// that need to be connected data from somewhere else.
type thing_id = uint32

// Struct type to be embedded (mentioned without a name) in types
// that use the DOD workflow (state separated into different )
type thing_with_id struct{ ID thing_id }

// List of entity IDs that are already taken, used for lookup
// for uniqueness whenever we need a new ID.
var TakenIDs map[thing_id]struct{}

func NewEntityID() thing_id {
	for {
		id := rand.Uint32()
		if _, taken := TakenIDs[id]; taken {
			continue
		}
		return id
	}
}

const (
	// TODO:  Probably still missing a scene with starting cutscene,
	//   but that can be the Start I suppose...
	SCENE_Start Scene_ = iota
	SCENE_Playing
	SCENE_Gameover_TeacherPissed
	SCENE_Gameover_StudentBoredToDeath
	SCENE_Gameover_ClassEnd
)

type ( // Game state and objects

	// Game scene, like the game itself, the game over screen etc
	Scene_ int

	Cutscene struct {
		Anim             *AnimationPlayer
		GameOver         *AnimationPlayer
		TotalTimeElapsed SecsDT
	}

	// TODO:  Don't put all the fucking assets here, this is fucking mental dude.
	//   Who's supposed to track all of this, just use the goddamn IDs to track
	//   everything.
	GameState struct {
		sync.RWMutex

		// The scene / mode the game currently plays
		CurrentScene, LastFrameScene Scene_
		// TODO:  We're using like 3 different techniques for detecting the start
		//   of a scene and it's ridiculous.

		// Tables for domain-specific data sets
		Phys     map[thing_id]Phys
		Particle map[thing_id]Particle

		// Fullscreen graphics that never change
		OverlayLayers []Overlay
		BackgroundBar *ebi.Image

		// How close to the end are the parties
		Sanity struct {
			Teacher_01, Student_01 float64

			FactorAll            float64
			FactorTeacherTime    float64
			FactorStudentLetters float64

			Overlay, BarTeacher, BarStudent *ebi.Image
		}

		// Do we have our shields up
		Headphones struct {
			// To add some difficulty, let's make the headphone taking out action
			// take some time and block until it's completed.
			// Might just be annoying tho.
			CommandOutOfHead bool // What direction to advance the transition
			IsInHead         bool // Does the transition count as in head or not

			// The grabbie hand will have some pretty intricate animation behavior
			// so we don't want to have it in the overlays, even tho it really should
			// be one.
			Grabbie *AnimationPlayer

			Player struct {
				*audio.Player
			}
		}

		// Teachers' annoying words  (using map for easy deletion when resolved)
		IncomingWords map[*Word]struct{} `json:"-"` // Can't use in a json
		// Letters of words, drawn separately
		Letters map[thing_id]Letter

		// Position for checking letter collision
		EarEntranceX, EarEntranceY float64

		// Seconds remaining in class
		RemainingSecs SecsDT
		// How many times faster are we compared to actual time
		RealtimeSpeedup float64
		// How fast the time is going (pretty fast if you're having fun, right?),
		// is combined with `RealtimeSpeedup`
		FunTimeFactor float64

		// Numbers showing the remaining seconds (a single animation is enough)
		NumberAnimation *Animation
		// Letters for quick access too
		LetterAnimation *Animation
	}

	// The word is the shit the teacher says. When we don't listen,
	// these include insults in the form of @todo anger icons or insults (these hit more).
	Word struct {
		// How hard the word hits, angers hit more
		Importance_01 float64
		// Count how each word was processed
		ProcessedCounters [LETTERSTATE__COUNT]int
		// Let's delete words that have obviously been in the game for too long
		DeletionTimer SecsDT
	}
	Letter struct {
		thing_with_id // Let's keep track of physics and drawing of individual letters

		// Let's remember the parent, the word is the one keeping track
		Word *Word

		// Let's use animation frames of a spritesheet to define letters to draw.
		// ( could allow us to make some cool effects for angers )
		// Don't forget to reuse the same animation for all letters
		Anim *AnimationPlayer

		Char     string
		EndState LETTERSTATE
		Timer    SecsDT
	}

	// The fullscreen overlay animated sprites. All of these should be as large
	// as the entire.
	Overlay struct {
		// ZIndex int // Sort: https://stackoverflow.com/questions/28999735/#42872183
		Anim *AnimationPlayer
		Img  *ebi.Image
	}

	Phys struct {
		thing_with_id
		X, Y, Rot       float64
		Dx, Dy, Drot    float64
		Scale_1, Dscale float64 // (Scale - 1) for a harmless 0-default
	}
	Particle struct { // We probably won't have the time to do those
		thing_with_id
		Color                       color.Color
		TimeToLive, TimeToLiveStart SecsDT
	}
)

type ( // Sound and assets
	GameSound struct {
		Ctx  *audio.Context
		Wavs map[string]SpammableSound
	}
	SpammableSound struct {
		players []*audio.Player
	}
)

// ================================================================================
//   ENUMS
// ================================================================================

type LETTERSTATE int

const (
	LETTERSTATE_INCOMING = iota
	LETTERSTATE_ASSIMILATED
	LETTERSTATE_BOUNCEDOFF
	LETTERSTATE__COUNT
)
