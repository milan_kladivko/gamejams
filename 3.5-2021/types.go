package main

import (
	"sync"

	"image/color"

	. "jam/helpers"
	. "jam/timers"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

type Game struct {
	sync.RWMutex // Lock for easy goroutine access

	// The scene / mode the game currently plays
	CurrentScene, LastFrameScene enum_scene

	Input  Input
	Time   Time
	Camera Camera

	World World
}

// ===========================================================================
//   ENUMS
// ===========================================================================

type enum_scene int

const (
	SCENE_Start enum_scene = iota
)

// ===========================================================================
//   TESTING HOOKS
// ===========================================================================

type tester interface {
	Update() error
	Draw(*ebi.Image)
}

// ===========================================================================

// TODO:  What to do with particles man?
type (
	Particles struct {
		Phys      map[EntityID]Particle_Position
		Particles map[EntityID]Particle
	}
	Particle_Position struct {
		Entity
		X, Y, Dx, Dy float64
	}
	Particle struct { // We probably won't have the time to do those
		Entity
		X, Y            float64 // Copy from position
		Color           color.Color
		TimeToLive      SecsDT
		TimeToLiveStart SecsDT
	}
)
