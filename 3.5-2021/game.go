package main

import (
	"sort"

	ebi "github.com/hajimehoshi/ebiten/v2"
	ebiutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"

	"jam/assets"
	. "jam/assets/files"
	. "jam/helpers"
	. "jam/timers"

	"log"
	"math/rand"
)

var (
	PLAYER_ID EntityID = 0

	REND_COLOR_BACKGROUND = RGBFromHex("#f0f") // kick-in-the-face magenta
	REND_OUTLINE_XY       = [4][2]float64{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}

	REND_BACKGROUNDS_ANIM *assets.Animation

	GROUND_Y = float64(RENDER_H * 0.65)
)

type (
	World struct {
		WidthTotal       float64
		WidthExploredMin float64 // TODO:  Update based on player
		WidthExploredMax float64

		ThePlayer ThePlayer
		// Buidings map[entity_id]___
		// Enemies  map[entity_id]___
		Allies map[EntityID]Ally

		// This list might have to get modified at runtime, at which point
		// I should probably use a map insead.
		Clouds []EntityID

		Sprites   map[EntityID]AnimatedSprite
		Positions Positions
		Particles Particles
	}

	ThePlayer struct {
		Entity
		MoveToX float64
	}
	Ally struct {
		Entity
		Type enum_ally_type

		Dx              float64
		TimerJump       *Timer
		TimerNextAction *Timer
	}

	AnimatedSprite struct {
		Entity
		AP      *assets.AnimationPlayer
		X, Y    float64     // Copy over from positions
		Origin  enum_origin // Controls how the image is aligned for X,Y
		Z       ZIndex      // Dictates render order and parallaxing
		Flip    bool
		Outline enum_outline
	}
	ZIndex struct {
		//Render        uint64
		ParallaxDepth int // Can be both negative and positive
		// @see methods of this type for constants
	}

	enum_ally_type int
	enum_origin    int
	enum_outline   int

	Positions struct {
		OfEntity map[EntityID]Position
		// TODO:  We'll probably want position regions to limit the amount
		//   of collision checking we have to do.
		//   That means bucketing entities into more specific maps
		//   based on their Y position.
	}
	Position struct {
		Entity
		X, Y float64
		W, H float64
	}
)

// =========================================================================

func (wPtr *World) Init() {
	{
		REND_BACKGROUNDS_ANIM = mustLoadStaticAssetsAsAnimation()

		REND_ANIMATION_PEOPLES = assets.LoadAnimation(
			FILE_grefix__export__peoples_json)
		REND_ANIMATION_CONCUBINES = assets.LoadAnimation(
			FILE_grefix__export__ent_concubines_json)
	}

	var (
		wTotal  = RENDER_W * 5 // screens' worth of world
		wCenter = wTotal / 2

		p, pSprite, pPos = NewPlayer()
	)

	pPos.X = wCenter

	w := World{
		WidthTotal:       wTotal,
		WidthExploredMin: wCenter,
		WidthExploredMax: wCenter,

		Positions: Positions{
			OfEntity: map[EntityID]Position{p.ID: pPos},
		},
		ThePlayer: p,
		Allies:    map[EntityID]Ally{},

		Sprites: map[EntityID]AnimatedSprite{p.ID: pSprite},

		Particles: Particles{
			Phys:      map[EntityID]Particle_Position{},
			Particles: map[EntityID]Particle{},
		},
	}

	var cloudIDs []EntityID
	{ // Background sprites
		cloudIDs = w.nRandSprites(300, "cloud", randOps{
			InSky:    true,
			DepthMax: MAX_DEPTH * DOWN,
			DepthMin: MAX_DEPTH * DOWN * 0.6,
		})
		w.nRandSprites(50, "trees", randOps{
			Type:     TYPE_FOREGROUND,
			DepthMax: MAX_DEPTH * UP,
			DepthMin: MAX_DEPTH * UP * 0.6,
		})
		w.nRandSprites(50, "foliage", randOps{})
		w.nRandSprites(20, "rocks", randOps{})
		w.nRandSprites(80, "back", randOps{
			DepthMax: MAX_DEPTH * DOWN,
			DepthMin: MAX_DEPTH * DOWN * 0.5,
		})
	}
	w.Clouds = cloudIDs

	w.AddRandomAllies(10, ALLY_CONCUBINE)

	*wPtr = w // Overwrite
}

//

func NewPlayer() (ThePlayer, AnimatedSprite, Position) {

	Assert(PLAYER_ID == 0, "tried to make 2 players")
	PLAYER_ID = NewEntityID()

	const w, h = 11, 10

	var (
		e      = Entity{PLAYER_ID, TYPE_PLAYER}
		player = ThePlayer{Entity: e}
		sprite = AnimatedSprite{
			Entity: e,
			Origin: ORIGIN_CENTERBOTTOM,
			AP: NewAnimationPlayer(REND_ANIMATION_PEOPLES, PLAYER_ID).
				SetLayer("king2").
				SwitchToTag("idle"),
			Outline: OUTLINE_BLACK,
		}
		position = Position{
			X: 0, Y: 0, // Filled outside
			W: w, H: h,
		}
	)

	return player, sprite, position
}

func LoadAnimationAndPlayer(
	abspath string, forEntity EntityID) *assets.AnimationPlayer {
	// Note: Use this only for single-occurence animations,
	//   they are obviously not reused. Don't load animations multiple
	//   times please.
	return NewAnimationPlayer(assets.LoadAnimation(abspath), forEntity)
}
func NewAnimationPlayer(
	anim *assets.Animation, forEntity EntityID) *assets.AnimationPlayer {
	ap := &assets.AnimationPlayer{
		Entity:    Entity{forEntity, TYPE_ANIMATION_PLAYER},
		Animation: anim,

		FrameIndex:      0,
		DurationCounter: 0,

		PlayedTag:     assets.Tag{},
		PlayedTagName: "",
		PlaybackType:  0,
	}
	ap.SwitchToDefaultTag()

	// Registering the timer stuff is done immediatelly
	GAME.Time.AnimationPlayers[ap.ID] = ap
	// TODO:  When deleting enemies, make sure this gets deleted too

	return ap
}

//

const (
	ALLY_CONCUBINE enum_ally_type = iota // That's the default~~
	ALLY_BUILDER
	ALLY_FARMER
	ALLY_SHOOTER
	ALLY_PERSON
)

var (
	__layers_concubines = []string{
		"Layer 1", "Layer 2", "Layer 3", "Layer 4", "Layer 5",
		"Layer 6", "Layer 7", "Layer 8", "Layer 9"}
	__layers_peoples = map[enum_ally_type][]string{
		ALLY_BUILDER: {"builder1", "builder2", "builder3"},
		ALLY_FARMER:  {"famer1", "famer2", "famer3"},
		ALLY_SHOOTER: {"fighter1", "fighter2", "fighter3", "fighter4"},
		ALLY_PERSON:  {"person"},
	}
	// ...and some others, idc

	REND_ANIMATION_CONCUBINES *assets.Animation
	REND_ANIMATION_PEOPLES    *assets.Animation
)

func NewAlly(typ enum_ally_type) (Ally, AnimatedSprite, Position) {
	var (
		id = NewEntityID()
		e  = Entity{id, TYPE_ALLY}
	)
	ally := Ally{Entity: e}

	alts := __layers_concubines
	randLayer := alts[rand.Intn(len(alts))]

	sprite := AnimatedSprite{Entity: e,
		Origin: ORIGIN_CENTERBOTTOM,
		AP: NewAnimationPlayer(REND_ANIMATION_CONCUBINES, e.ID).
			SetLayer(randLayer).
			SwitchToTag("idle"),
		Outline: OUTLINE_WHITE,
	}

	const w, h = 2, 5
	pos := Position{W: w, H: h}

	return ally, sprite, pos
}
func (wrd *World) AddRandomAllies(n int, typ enum_ally_type) {
	for i := 0; i < n; i++ {
		ally, sprite, pos := NewAlly(typ)

		pos.X = wrd.WidthTotal/2 + (rand.Float64()*200 - 100)

		wrd.Allies[ally.ID] = ally
		wrd.Sprites[ally.ID] = sprite
		wrd.Positions.OfEntity[ally.ID] = pos
	}
}

//

type randOps struct {
	InSky              bool
	DepthMin, DepthMax int
	Type               EntityType
}

func (w *World) nRandSprites(N int, T tagname, OPS randOps) []EntityID {

	typ := OPS.Type
	if typ == TYPE_UNSET {
		typ = TYPE_BACKGROUND
	}

	sprites := make([]AnimatedSprite, N)
	for i := range sprites {

		var x = rand.Float64() * w.WidthTotal
		var y float64 = 0 // Ground, exactly
		if OPS.InSky {
			y = RandRangeFloat(UP*GROUND_Y, UP*GROUND_Y*0.4)
		}
		var z int = 0
		if OPS.DepthMin != 0 || OPS.DepthMax != 0 {
			z = RandRangeInt(OPS.DepthMin, OPS.DepthMax)
		}

		var id = NewEntityID()
		var ap = NewAnimationPlayer(REND_BACKGROUNDS_ANIM, id).
			SwitchToTag(T)
		ap.FrameIndex = rand.Intn(len(ap.PlayedTag.Frames))
		ap.PlaybackType = assets.PLAYBACK_PAUSE
		sprites[i] = AnimatedSprite{Entity: Entity{ID: id, Type: typ},
			AP: ap, X: x, Y: y, Z: ZIndex{ParallaxDepth: z},
			Origin: ORIGIN_CENTERBOTTOM,
			Flip:   rand.Float64() > .5,
		}
	}

	ids := make([]EntityID, 0, len(sprites))
	for _, c := range sprites {
		w.Sprites[c.ID] = c
		ids = append(ids, c.ID)
	}
	return ids
}

// =========================================================================

func (p *ThePlayer) Update(dt SecsDT, inp Input, w World) {
	id := p.ID

	var dx float64
	{
		pos, ok := w.Positions.OfEntity[id]
		Assert(ok, "player needs a position")
		defer func() { w.Positions.OfEntity[id] = pos }()

		var ( //@conf
			walkspeed = 60.0 // per second
			// TODO:  Some easing on the movement
		)

		switch in := inp.Player; {
		case in.Left.IsPressed:
			p.MoveToX = pos.X + walkspeed*LEFT*dt
		case in.Right.IsPressed:
			p.MoveToX = pos.X + walkspeed*RIGHT*dt
		case inp.Mouse.ClickLeft.IsPressed:
			p.MoveToX = inp.Mouse.WorldX
		}

		dx = p.MoveToX - pos.X
		dx = Clamped(dx, -walkspeed*dt, +walkspeed*dt)
		pos.X += dx
	}

	{
		sprite, ok := w.Sprites[id]
		Assert(ok, "player sprite flipping")
		defer func() { w.Sprites[id] = sprite }()
		if Abs(dx) > (0.01 /*world pos diff*/) {
			sprite.Flip = dx < 0
		}
	}
}

func (w *World) UpdateAllies(dt SecsDT) {
	// AI
	for id, ally := range w.Allies {
		ally.updateBehavior(dt, *w)
		w.Allies[id] = ally // overwrite
	}
	// Position info, jumping
	for id, ally := range w.Allies {
		pos := w.Positions.OfEntity[id]

		pos.X += ally.Dx * dt
		if ally.TimerJump != nil {
			const height = 1.5 //px // @conf
			f := ally.TimerJump.Completed_01()
			pos.Y = UP * height * (1 - f)
		} else {
			pos.Y = 0
		}

		w.Positions.OfEntity[id] = pos
	}
	// Flipping
	for id, ally := range w.Allies {
		sprite := w.Sprites[id]
		if ally.Dx > 0 {
			sprite.Flip = false
		} else if ally.Dx < 0 {
			sprite.Flip = true
		}
		w.Sprites[id] = sprite
	}
}

func (a *Ally) updateBehavior(dt SecsDT, w World) {
	if a.IsWaitingForNextAction() {
		// Queue next action switch
		var nextActionIn = SecsDT(2 + rand.Float64()*3) // @conf
		a.TimerNextAction = GAME.Time.NewTimer(TimerOps{KillAfter: nextActionIn})

		const speed = 10 // @conf
		if a.Dx == 0 {
			roll := SignFloat(rand.Float64() - .5) // @conf
			a.Dx = speed * roll
		} else {
			a.Dx = 0
		}
	}

	var jumptime = SecsDT(0.2 + rand.Float64()*0.1) // @conf
	if a.IsOnGround() && Abs(a.Dx) > 0.1 {
		a.TimerJump = GAME.Time.NewTimer(TimerOps{KillAfter: jumptime})
	}
}

func (a *Ally) IsWaitingForNextAction() bool {
	return a.TimerNextAction == nil || a.TimerNextAction.Completed_01() == 1
}
func (a *Ally) IsOnGround() bool {
	return a.TimerJump == nil || a.TimerJump.Completed_01() == 1
}

func (w *World) UpdatePositionsToSprites() {
	var (
		positions = w.Positions.OfEntity
		sprites   = w.Sprites
	)
	for id, pos := range positions {
		sprite, ok := sprites[id]
		if !ok {
			Printf("positioned entity without a sprite %d", id)
			continue
		}
		{
			sprite.X, sprite.Y = pos.X, pos.Y
		}
		sprites[id] = sprite
	}
}
func (w *World) MoveClouds(dt SecsDT) {
	for _, id := range w.Clouds {
		sprite := w.Sprites[id]
		{
			const CLOUD_SPEED = 50 // px/sec, give parallaxing into account
			sprite.X += dt * CLOUD_SPEED
		}
		w.Sprites[id] = sprite
	}
}

// =========================================================================

const (
	// Enum defines the alignment based on X,Y
	ORIGIN_LEFTTOP enum_origin = iota // The default
	ORIGIN_LEFTBOTTOM
	ORIGIN_CENTERBOTTOM
)
const (
	OUTLINE_NONE enum_outline = iota
	OUTLINE_BLACK
	OUTLINE_WHITE
)

func (wrd World) DrawSprites(screen *ebi.Image, cam Camera) {

	drawStaticBG(screen)         // Doesn't care about camera at all
	defer drawGroundLine(screen) // Neither does the ground line

	var (
		// Ignore Y camera position as this game shouldn't move
		// the camera up and down at all.
		camera = cam.GeomCamNoY()
		center = cam.GeomScreenCenter()
	)

	if true {

		xyGeom := ebi.GeoM{}
		{
			// Sprite positions are relative to the ground, 0 means on the ground
			xyGeom.Translate(0, +GROUND_Y)
			// Apply the screen centering
			xyGeom.Concat(center)
			// Move the images with the camera
			xyGeom.Concat(camera)
		}
		xMove := wrd.ThePlayer.MoveToX
		defer drawMovementIndicator(screen, xyGeom, xMove)
	}

	// TODO:  Filter out offscreen sprites, even before sorting

	_, parallaxLayers := wrd.zSortedSprites()

	for _, layer := range parallaxLayers {

		// TODO:  We should apply an overlay over the parallax layer for
		//   coloring. Such as: Things in the background should be more blue
		//   from all the air inbetween and things very close should
		//   just be darker than the things in the 0 layer.
		//   Should be pretty easy at this point.

		// The camera should actually just say where we want the positions,
		// and it shouldn't talk about the scaling of the image itself.
		//
		// I presume this would disable any zooming, which would complicate
		// things quite a bit.
		xyGeom := ebi.GeoM{}
		{
			// Sprite positions are relative to the ground, 0 means on the ground
			xyGeom.Translate(0, +GROUND_Y)
			// Apply the screen centering
			xyGeom.Concat(center)
			// Move the images with the camera
			xyGeom.Concat(camera)
			// Multiply every position by parallax scaling, scaling the camera
			// position too.
			sc := 1 - layer[0].Z.ParallaxScale_01()
			{
				// @bug;  The clouds don't get translated correctly to their
				//   position on screen -- It all gets squished down to the 0 coord
				//   of the world. I'm not sure how to do it because there are
				//   two ways of doing things...
				//   -- Either I read the parallax scale for the layer
				//      I'm putting the cloud on and adjust the starting position
				//   -- Or I'll have to adjust the cloud movement by the
				//      scaling or have some separate scaling of the cloud speed
				//      based on the layer.
				//   I don't know if I can do it without messing with the parallax
				//   definition of scale... I'll have to think about it
				//
				//   2021-04-24:  Coming back to it, I think just exposing
				//    the scale amount and reading it when placing things is perfectly
				//    fine. It's not even that hard to implement, should be pretty
				//    straightforward to do, actually.
				sc = sc * sc
			}
			xyGeom.Scale(sc, 1)
		}

		for _, s := range layer {
			sprite := s.AP.CurrentFrame().SubImage
			w, h := float64(sprite.Bounds().Dx()), float64(sprite.Bounds().Dy())

			geom := ebi.GeoM{}

			// Flipping and centering of the sprite
			switch s.Origin {
			case ORIGIN_LEFTTOP:
				// Nothing, that's the default
			case ORIGIN_LEFTBOTTOM:
				geom.Translate(w*0, -h*1)
			case ORIGIN_CENTERBOTTOM:
				geom.Translate(-w/2, -h*1)
			}

			if s.Flip {
				geom.Scale(-1, 1)
			}

			x, y := xyGeom.Apply(s.X, s.Y)
			x, y = Ceil(x), Ceil(y) // Prevent camera float-sum off-by-1 glitches
			geom.Translate(x, y)

			maybeDrawOutline(screen, sprite, geom, s.Outline)
			screen.DrawImage(sprite, &ebi.DrawImageOptions{GeoM: geom})
		}
	}
}

//

var ___indicatorOnscreen *ebi.Image = func() *ebi.Image {
	var color = Hex("#fff")
	const (
		size        = 7
		full, empty = '-', ' '
		pixels      = "" +
			"   -   " +
			"   -   " +
			"   -   " +
			" ----- " +
			"--   --" +
			" ----- " +
			"   -   "
	)

	// NOTE:  There's probably a way better way to do this thing,
	//   but I'm really rusty and I didn't experiment with doing
	//   debug stuff out of strings enough to know the best way.

	img := ebi.NewImage(size, size)
	for i, p := range []byte(pixels) {
		if p == full {
			x, y := float64(i%size), float64(i/size)
			ebiutil.DrawRect(img, x, y, 1, 1, color)
		}
	}
	return img
}()

func drawMovementIndicator(dest *ebi.Image, cam ebi.GeoM, wx float64) {
	const ypos = 1

	var (
		img  *ebi.Image = ___indicatorOnscreen
		geom ebi.GeoM
	)

	x, _ := cam.Apply(wx, 0)
	switch {
	case x < 0: // Left offscreen
		fallthrough
	case x > RENDER_W: // Right offscreen
		log.Printf("[FIXME] WHAT TO DO OFF SCREEN?")
	default: // On Screen
		img = ___indicatorOnscreen
		wid := float64(img.Bounds().Dx())

		geom.Concat(cam) // Use the cam as the basis
		geom.Translate(Ceil(wx), ypos)
		geom.Translate(-wid, 0) // Center the image on the position

	}

	maybeDrawOutline(dest, img, geom, OUTLINE_BLACK)
	dest.DrawImage(img, &ebi.DrawImageOptions{GeoM: geom})
}

//

func drawStaticBG(dest *ebi.Image) {
	xa, ya, xb, yb := _groundLine()
	// grass underline
	ebiutil.DrawLine(dest, xa, ya+1, xb, yb+1, Hex("#6b6"))
	// sky
	x, y, w, h := xa, ya+2, RENDER_W, RENDER_H-ya
	ebiutil.DrawRect(dest, x, y, w, h, Hex("#66b"))
	// water
	x, y, w, h = 0, 0, RENDER_W, +GROUND_Y
	ebiutil.DrawRect(dest, x, y, w, h, Hex("#69c"))
}
func drawGroundLine(dest *ebi.Image) {
	xa, ya, xb, yb := _groundLine()
	ebiutil.DrawLine(dest, xa, ya, xb, yb, Hex("#111"))
}
func _groundLine() (xa, ya, xb, yb float64) {
	xa, ya, xb, yb = 0, +GROUND_Y, RENDER_W, +GROUND_Y
	return
}

//

func maybeDrawOutline(dest, src *ebi.Image, g ebi.GeoM, outline enum_outline) {
	if outline == OUTLINE_NONE {
		return
	}

	var colorm ebi.ColorM
	switch outline {
	case OUTLINE_BLACK:
		colorm.Scale(0, 0, 0, 1) // Kills all color, black outline
	case OUTLINE_WHITE:
		colorm.Translate(999, 999, 999, 0) // Pure white outline
	default:
		log.Printf("unknown outline: %d", outline)
		panic(true)
	}

	for _, xy := range REND_OUTLINE_XY {
		var geomOffset ebi.GeoM = g // copy
		geomOffset.Translate(xy[0], xy[1])
		dest.DrawImage(src, &ebi.DrawImageOptions{GeoM: geomOffset, ColorM: colorm})
	}
}

//

// TODO:  Filter out offscreen sprites

func (wrd World) zSortedSprites() ([]AnimatedSprite, [][]AnimatedSprite) {

	// NOTE:  This is a lot of useless stuff that we do. It'll almost
	//   never change so unless some new entities are introduced,
	//   there won't be any modifications in this list. It calls
	//   for some kind of tree, but I don't really know how
	//   to do that in Go yet. I'll see how this will impact
	//   performance.

	var arr = make([]AnimatedSprite, len(wrd.Sprites))
	{ // Pull into a sortable array
		var i = 0
		for _, s := range wrd.Sprites {
			arr[i] = s
			i++
		}
	}

	// Dictate a drawing order based on entity type. This is then
	// used in sorting.
	// TODO:  Make only once
	var typeLayers = map[EntityType]int{}
	{
		order := []EntityType{
			TYPE_BACKGROUND,

			TYPE_PLAYER,
			TYPE_ENEMY,
			TYPE_ALLY,
			TYPE_PICKUP,

			TYPE_FOREGROUND,
			TYPE_UNSET, // So we see it clearly
		}
		for i := range order {
			typeLayers[order[i]] = i
		}
	}

	sort.Slice(arr, func(i, j int) bool {
		a, b := arr[i], arr[j]
		if a.Z.ParallaxDepth == b.Z.ParallaxDepth {
			if a.Entity.Type == b.Entity.Type {
				// Fallback to the entity ID so that the order is preserved
				// between the frames. No z-fighting please.
				return a.ID > b.ID
			}
			// Sort by the ordering of the enum. This means that the enum
			// dictates the drawing order, which might not be expected.
			return typeLayers[a.Entity.Type] < typeLayers[b.Entity.Type]
		}
		// Sort by parallax depth. This has priority.
		return a.Z.ParallaxDepth > b.Z.ParallaxDepth
	})

	// When sorted, grab all parallax layers as slices into the same
	// underlying array -- no additional allocations please.
	var slices [][]AnimatedSprite
	{
		var breaks = make([]int, 0, 2*MAX_DEPTH) // both up and down
		prev := -999999                          // Just something out of range
		for i := range arr {                     // ...also ensure breaks[0] == 0
			d := arr[i].Z.ParallaxDepth
			if d != prev {
				breaks = append(breaks, i)
			}
			prev = d
		}
		slices = make([][]AnimatedSprite, 0, len(breaks)-1)
		for idx := 0; idx < len(breaks)-1; idx++ {
			start, end := breaks[idx], breaks[idx+1]
			slices = append(slices, arr[start:end])
		}
	}

	return arr, slices
}

const MAX_DEPTH = +10
const MAX_FOREGROUND = -100

func (z ZIndex) ParallaxScale_01() float64 {
	depth := float64(z.ParallaxDepth)
	f := Clamped(depth/MAX_DEPTH, -1, 1)
	return f
}

func (z ZIndex) HasParallax() bool { return z.ParallaxDepth != 0 }

//
//

func mustLoadStaticAssetsAsAnimation() *assets.Animation {
	// NOTE:  This animation will not reload when changed as we're
	//   loading multiple files into one animation.

	// TODO:  Once again, we should've applied DOD -- instead of having
	//   an asset carrying the file data, we should have a map
	//   of filenames to animation pointers without burdening the
	//   animation object with it.

	var tags = map[string]assets.Tag{}
	for tagname, paths := range backgroundAssetImages {
		var frames = make([]assets.Frame, len(paths))
		for i := range paths {
			img := assets.MustLoadImage(paths[i])
			frames[i] = assets.Frame{
				Duration: 0,
				SubImage: img,
			}
		}
		tags[tagname] = assets.Tag{
			Name:   tagname,
			Frames: frames,
		}
	}

	return &assets.Animation{
		Asset: assets.Asset{},
		Tags:  tags,
		Sheet: nil, // There is no single sheet, it's multiple images
	}
}

type (
	tagname = string
	path    = string
)

var backgroundAssetImages = map[tagname][]path{
	"foliage": {
		FILE_grefix__export__Foliage1_png,
		FILE_grefix__export__Foliage2_png,
		FILE_grefix__export__Foliage3_png,
		FILE_grefix__export__Foliage4_png,
		FILE_grefix__export__Foliage5_png,
		FILE_grefix__export__Foliage6_png,
		FILE_grefix__export__Foliage7_png,
	},
	"rocks": {
		FILE_grefix__export__Rocks1_png,
		FILE_grefix__export__Rocks2_png,
	},
	"back": {
		FILE_grefix__export__Back1_png,
		FILE_grefix__export__Back2_png,
		FILE_grefix__export__Back3_png,
		FILE_grefix__export__Back4_png,
		FILE_grefix__export__Back5_png,
	},
	"cloud": {
		FILE_grefix__export__Cloud1_png,
		FILE_grefix__export__Cloud2_png,
		FILE_grefix__export__Cloud3_png,
		FILE_grefix__export__Cloud4_png,
		FILE_grefix__export__Cloud5_png,
		FILE_grefix__export__Cloud6_png,
		FILE_grefix__export__Cloud7_png,
		FILE_grefix__export__Cloud8_png,
	},
	"trees": {
		FILE_grefix__export__Trees1_png,
		FILE_grefix__export__Trees2_png,
		FILE_grefix__export__Trees3_png,
	},
	"water": {
		FILE_grefix__export__Water1_png,
		FILE_grefix__export__Water2_png,
	},
}
