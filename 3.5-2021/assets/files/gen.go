
package files

// GENERATED ASSETS GO HERE

// Filenames listed as constants for autocompleting
const (
    FILE_build_web__glasspain_logo2_png = "build_web/glasspain_logo2.png"
    FILE_grefix__export__BCloud1_png = "grefix/export/BCloud1.png"
    FILE_grefix__export__BCloud2_png = "grefix/export/BCloud2.png"
    FILE_grefix__export__BCloud3_png = "grefix/export/BCloud3.png"
    FILE_grefix__export__BCloud5_png = "grefix/export/BCloud5.png"
    FILE_grefix__export__Back1_png = "grefix/export/Back1.png"
    FILE_grefix__export__Back2_png = "grefix/export/Back2.png"
    FILE_grefix__export__Back3_png = "grefix/export/Back3.png"
    FILE_grefix__export__Back4_png = "grefix/export/Back4.png"
    FILE_grefix__export__Back5_png = "grefix/export/Back5.png"
    FILE_grefix__export__Back6_png = "grefix/export/Back6.png"
    FILE_grefix__export__Back7_png = "grefix/export/Back7.png"
    FILE_grefix__export__Back8_png = "grefix/export/Back8.png"
    FILE_grefix__export__Cloud1_png = "grefix/export/Cloud1.png"
    FILE_grefix__export__Cloud2_png = "grefix/export/Cloud2.png"
    FILE_grefix__export__Cloud3_png = "grefix/export/Cloud3.png"
    FILE_grefix__export__Cloud4_png = "grefix/export/Cloud4.png"
    FILE_grefix__export__Cloud5_png = "grefix/export/Cloud5.png"
    FILE_grefix__export__Cloud6_png = "grefix/export/Cloud6.png"
    FILE_grefix__export__Cloud7_png = "grefix/export/Cloud7.png"
    FILE_grefix__export__Cloud8_png = "grefix/export/Cloud8.png"
    FILE_grefix__export__FCloud1_png = "grefix/export/FCloud1.png"
    FILE_grefix__export__Foliage1_png = "grefix/export/Foliage1.png"
    FILE_grefix__export__Foliage2_png = "grefix/export/Foliage2.png"
    FILE_grefix__export__Foliage3_png = "grefix/export/Foliage3.png"
    FILE_grefix__export__Foliage4_png = "grefix/export/Foliage4.png"
    FILE_grefix__export__Foliage5_png = "grefix/export/Foliage5.png"
    FILE_grefix__export__Foliage6_png = "grefix/export/Foliage6.png"
    FILE_grefix__export__Foliage7_png = "grefix/export/Foliage7.png"
    FILE_grefix__export__Ground1_png = "grefix/export/Ground1.png"
    FILE_grefix__export__Rocks1_png = "grefix/export/Rocks1.png"
    FILE_grefix__export__Rocks2_png = "grefix/export/Rocks2.png"
    FILE_grefix__export__Sky1_png = "grefix/export/Sky1.png"
    FILE_grefix__export__Slice_9_png = "grefix/export/Slice 9.png"
    FILE_grefix__export__Trees1_png = "grefix/export/Trees1.png"
    FILE_grefix__export__Trees2_png = "grefix/export/Trees2.png"
    FILE_grefix__export__Trees3_png = "grefix/export/Trees3.png"
    FILE_grefix__export__Water1_png = "grefix/export/Water1.png"
    FILE_grefix__export__Water2_png = "grefix/export/Water2.png"
    FILE_grefix__export__concubines_json = "grefix/export/concubines.json"
    FILE_grefix__export__concubines_png = "grefix/export/concubines.png"
    FILE_grefix__export__ent_concubines_json = "grefix/export/ent-concubines.json"
    FILE_grefix__export__ent_concubines_png = "grefix/export/ent-concubines.png"
    FILE_grefix__export__ent_peoples_json = "grefix/export/ent-peoples.json"
    FILE_grefix__export__ent_peoples_png = "grefix/export/ent-peoples.png"
    FILE_grefix__export__env_clouds_json = "grefix/export/env-clouds.json"
    FILE_grefix__export__env_clouds_png = "grefix/export/env-clouds.png"
    FILE_grefix__export__env_nature_trees_json = "grefix/export/env-nature-trees.json"
    FILE_grefix__export__env_nature_trees_png = "grefix/export/env-nature-trees.png"
    FILE_grefix__export__peoples_json = "grefix/export/peoples.json"
    FILE_grefix__export__peoples_png = "grefix/export/peoples.png"
)

// Groups by globs, indexed by whatever inclusion globs have been
// used as the argument when loading.
var GlobGroups = [][]string{ 
    { // from '**.json'
        FILE_grefix__export__concubines_json,
        FILE_grefix__export__ent_concubines_json,
        FILE_grefix__export__ent_peoples_json,
        FILE_grefix__export__env_clouds_json,
        FILE_grefix__export__env_nature_trees_json,
        FILE_grefix__export__peoples_json,  
    }, 
    { // from '**.png'
        FILE_build_web__glasspain_logo2_png,
        FILE_grefix__export__BCloud1_png,
        FILE_grefix__export__BCloud2_png,
        FILE_grefix__export__BCloud3_png,
        FILE_grefix__export__BCloud5_png,
        FILE_grefix__export__Back1_png,
        FILE_grefix__export__Back2_png,
        FILE_grefix__export__Back3_png,
        FILE_grefix__export__Back4_png,
        FILE_grefix__export__Back5_png,
        FILE_grefix__export__Back6_png,
        FILE_grefix__export__Back7_png,
        FILE_grefix__export__Back8_png,
        FILE_grefix__export__Cloud1_png,
        FILE_grefix__export__Cloud2_png,
        FILE_grefix__export__Cloud3_png,
        FILE_grefix__export__Cloud4_png,
        FILE_grefix__export__Cloud5_png,
        FILE_grefix__export__Cloud6_png,
        FILE_grefix__export__Cloud7_png,
        FILE_grefix__export__Cloud8_png,
        FILE_grefix__export__FCloud1_png,
        FILE_grefix__export__Foliage1_png,
        FILE_grefix__export__Foliage2_png,
        FILE_grefix__export__Foliage3_png,
        FILE_grefix__export__Foliage4_png,
        FILE_grefix__export__Foliage5_png,
        FILE_grefix__export__Foliage6_png,
        FILE_grefix__export__Foliage7_png,
        FILE_grefix__export__Ground1_png,
        FILE_grefix__export__Rocks1_png,
        FILE_grefix__export__Rocks2_png,
        FILE_grefix__export__Sky1_png,
        FILE_grefix__export__Slice_9_png,
        FILE_grefix__export__Trees1_png,
        FILE_grefix__export__Trees2_png,
        FILE_grefix__export__Trees3_png,
        FILE_grefix__export__Water1_png,
        FILE_grefix__export__Water2_png,
        FILE_grefix__export__concubines_png,
        FILE_grefix__export__ent_concubines_png,
        FILE_grefix__export__ent_peoples_png,
        FILE_grefix__export__env_clouds_png,
        FILE_grefix__export__env_nature_trees_png,
        FILE_grefix__export__peoples_png,  
    }, 
    { // from '**.wav'  
    }, 
    { // from '**.mp3'  
    },
}

// Raw binary data of the assets.
// OMMITED. Apply the "-include-data" argument.
var Data = map[string][]byte{}

