package assets

import (
	"encoding/json"
	"log"
	"path/filepath"
	"strings"

	ebi "github.com/hajimehoshi/ebiten/v2"

	. "jam/helpers"
	. "jam/timers"
)

var LOG_ASSET_RELOADS = true

//
//  Animation player
//

// A wrapper around the animation to allow playback.
type AnimationPlayer struct {
	Entity
	Animation      *Animation // Controlled animation data, can (and should) be shared
	AnimationLayer string     // One animation can have multiple layers

	FrameIndex      int    // Index of currently played frame
	DurationCounter SecsDT // Playback control, counter for a single frame, gets reset

	PlayedTag     Tag // Currently played tag of the animation
	PlayedTagName string

	PlaybackType PlaybackType_ // What to do when the player is at the end
}

//

type PlaybackType_ int

const (
	PLAYBACK_AT_END__LOOP PlaybackType_ = iota
	PLAYBACK_AT_END__STOP
	PLAYBACK_AT_END__NOTHING
	PLAYBACK_PAUSE
)

//

// Get the current sprite, as defined by the current frame of the player
func (pl AnimationPlayer) Sprite() *ebi.Image {
	return pl.CurrentFrame().SubImage
}

// Get the current frame, as defined by player's timers and played tag
func (pl AnimationPlayer) CurrentFrame() Frame {
	return pl.FrameByIndex(pl.FrameIndex)
}

// Get a frame by its index in the currently played tag
func (pl AnimationPlayer) FrameByIndex(frameIndex int) Frame {
	return pl.PlayedTag.Frames[frameIndex]
}

// CompletedFrames_01 returns the completion percentage by frame count.
// This is faster to check than checking time, but almost never what
// we actually want.
func (pl AnimationPlayer) CompletedFrames_01() float64 {
	return float64(pl.FrameIndex) / float64(len(pl.PlayedTag.Frames)-1)
}

// Completed_01 returns the completion percentage by time expired
// in the animation currently played.
func (pl AnimationPlayer) Completed_01() float64 {
	return pl.DurationCounter / pl.PlayedTag.TotalDuration()
}

const ANIMATION_TAG_DEFAULT = "default"

// Switch to the default aseprite tag, playing the full animation.
func (pl *AnimationPlayer) SwitchToDefaultTag() *AnimationPlayer {
	pl.PlayedTag = pl.Animation.TagDefault()
	pl.PlayedTagName = ANIMATION_TAG_DEFAULT
	return pl
}
func (pl *AnimationPlayer) SetLayer(layerName string) *AnimationPlayer {
	pl.AnimationLayer = layerName
	// TODO:  Validate
	return pl
}
func (pl *AnimationPlayer) SwitchToTag(tagName string) *AnimationPlayer {

	if pl.AnimationLayer != "" {
		// Prepend the tagName with the animation layer only
		// if that hasn't happened already. Might be "just probing".
		layerPrefix := pl.AnimationLayer + "/" // @layertag
		if !strings.HasPrefix(tagName, layerPrefix) {
			tagName = layerPrefix + tagName
		}
	}

	tag, isValidTag := pl.Animation.TagByName(tagName)
	if !isValidTag {
		if !WEBMODE { //@noreload
			_, stillExists := pl.Animation.TagByName(pl.PlayedTagName)
			if !stillExists {
				log.Printf(
					"And the tag we had isn't there anymore, switching to default")
				pl.SwitchToDefaultTag()
			}
		}
		log.Printf("[ERR] Tagswitch %s#%s; ignoring...\n",
			pl.Animation.Asset.WatchedFilepath, tagName)
		return pl
	}

	pl.PlayedTag = tag
	pl.PlayedTagName = tagName

	return pl
}

// Start the animation; reset the frame index.
func (pl *AnimationPlayer) Start() *AnimationPlayer {
	pl.FrameIndex = 0
	pl.DurationCounter = 0
	return pl
}

// Advance the animation frame by one unconditionally.
func (pl *AnimationPlayer) NextFrame() *AnimationPlayer {
	pl.FrameIndex++
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations; Supports going into negative numbers.
// NOTE:  Does not support skipping multiple frames in one call.
func (pl *AnimationPlayer) Next(dt SecsDT) *AnimationPlayer {

	// Some animations aren't actually animations
	if pl.PlaybackType == PLAYBACK_PAUSE {
		return pl
	}

	// TODO:  Introduce some "noreload" build tag or something :: @noreload
	if !WEBMODE {
		pl.SwitchToTag(pl.PlayedTagName)
	}

	if dt > 0 {
		pl.DurationCounter += dt

		var nextSwitch SecsDT = pl.CurrentFrame().Duration
		if pl.DurationCounter >= nextSwitch {
			// Careful about carrying the overflow into the next frame
			pl.DurationCounter -= nextSwitch
			pl.FrameIndex++
		}
	} else {
		pl.DurationCounter += dt // will be negative so it's the same

		// Next switch is trivial
		if pl.DurationCounter < 0 {
			// Leave the right overflow when going backwards
			var prevSwitch SecsDT = pl.FrameByIndex(pl.FrameIndex).Duration
			pl.DurationCounter += prevSwitch
			pl.FrameIndex--
		}
	}

	{ // What to do if we're at the end of an animation
		var (
			high = len(pl.PlayedTag.Frames) - 1
			low  = 0
		)
		switch pl.PlaybackType {
		case PLAYBACK_AT_END__NOTHING:
			break
		case PLAYBACK_AT_END__LOOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = low
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = high
			}
		case PLAYBACK_AT_END__STOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = high
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = low
			}
		}
	}

	// Note: Using `Next()` directly can overflow
	return pl
}

// If past the end, loop back to the start.
func (pl *AnimationPlayer) LoopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__LOOP
	return pl
}

// If past the end, stop at the end.
func (pl *AnimationPlayer) StopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__STOP
	return pl
}

//
//  Animation data, imports from Aseprite
//

/**  REWRITE:
  We don't have a good way to manage multiple animations
  in one file -- like we want to do when working with layers.

  There should be one more layer of 1:N connection between
  a single spreadsheet and multiple animations via layers.
  At the moment, each layer is a new animation that has
  to be reloaded -- we're reading the same file N times
  for every layer, and thus animation, it contains.

  I would definitely prefer having some kind of store that
  the user doesn't have to know about that would hold
  these things as a system instead of objects.
                                         -- MK 2021-03-17
*/

// @todo; Add a way of adding sound-trigger data into the animations

// Note: Aseprite assets have two key files, the json and the png.
// The json includes all data about frame regions and tags (what regions
// belong to which animation).
// The json also has the filepath of the png, relative to the json file.

type (
	Animation struct {
		Asset                // Watchable and reloadable at runtime
		Tags  map[string]Tag // Sub-animations included in the asset
		Sheet *ebi.Image     `json:"-"`
	}
	Tag struct {
		Name   string  // Name of the tag as defined by the Aseprite export
		Frames []Frame // Frame data containing the image
	}
	Frame struct {
		Duration SecsDT     // Duration for the frame
		SubImage *ebi.Image `json:"-"` // Sheet region for this frame
	}
)

func (an *Animation) TagByName(tagName string) (Tag, bool) {
	t, isInMap := an.Tags[tagName]
	return t, isInMap
}
func (an *Animation) TagDefault() Tag {
	return an.Tags[ANIMATION_TAG_DEFAULT]
}

func (t Tag) TotalDuration() (dur SecsDT) {
	for _, f := range t.Frames {
		dur += f.Duration
	}
	return dur
}

func LoadAnimation(abspath string) *Animation {
	var a = Animation{
		Asset: Asset{WatchedFilepath: abspath},
	}
	a.Reload()
	AssetWatcher.Add(&a) // Watch every anim loaded this way
	return &a
}
func (a *Animation) Reload() {
	defer a.RecordLoaded() // Should record even failed reloads

	// When reloading, don't panic under any circumstances.
	// If the reload fails, don't just crash the whole thing
	// but just log that it happened.
	// The only crash-on-load is when the game starts.

	err := a.reload()
	if err != nil {
		// TODO:  Colorize
		log.Printf("[ERRO] animation reload: %v", err)
	}

	if LOG_ASSET_RELOADS {
		log.Printf("RELOADED: `%s`", a.WatchedFilepath)
	}
}
func (a *Animation) reload() error {
	var aseJson AseJSON
	{
		jsonBytes, err := loadBytes(a.Asset.WatchedFilepath)
		if err != nil {
			return err
		}
		err = json.Unmarshal(jsonBytes, &aseJson)
		if err != nil {
			return err
		}
	}

	var spritesheet *ebi.Image
	{
		aseJsonDir := filepath.Dir(a.Asset.WatchedFilepath)
		loadPath := filepath.Join(aseJsonDir, aseJson.Meta.Image)

		img, err := loadImage(loadPath)
		if err != nil {
			return err
		}
		spritesheet = img
	}

	var tags = defineTags(aseJson, spritesheet)

	// Change the content of the animation, always use the same animation instance
	// when live-reloading.

	a.Sheet = spritesheet
	a.Tags = tags

	// NOTE:  Tags are by value. You cannot preserve the
	// TODO:  Actually make []Tag arrays for animation tags,
	//   played tag be *[]Tag (pointer to played tag array)
	//   and "db index" of string(tagname) -> array index. We can simplify
	//   @noreload

	return nil
}
