package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"log"
	"strings"

	. "jam/timers"
)

// NOTE:  If we use the `inpututil`, it's gonna track **ALL** input
//   there is and can be. Because we want to track specific stuff,
//   let's not use that unless we have to (gamepads maybe).

// We won't be using the input methods directly but rather define
// our gameplay toggleable actions as collections of information
// about any of the chosen input methods.
type Input struct {
	Player  struct{ Left, Right inputTracked }
	UI      struct{ Up, Down, Left, Right, Select, Back inputTracked }
	QuitNow inputTracked

	Mouse struct {
		PrevScreenX, PrevScreenY int
		ScreenX, ScreenY         int

		WorldX, WorldY float64
		ClickLeft      inputTracked
		ClickRight     inputTracked
	}
	Keymap map[ebi.Key]*inputTracked
}

func (i *Input) UpdateMouse(dt SecsDT, cam Camera) {
	{
		m := &i.Mouse

		x, y := ebi.CursorPosition()
		m.ScreenX, m.ScreenY = x, y

		// TODO:  We should have the world-to-screen matrix here...
		geom := cam.GeomCamNoY()
		geom.Translate(0, +GROUND_Y)
		geom.Concat(cam.GeomScreenCenter())

		geom.Invert()

		wx, wy := geom.Apply(float64(x), float64(y))
		m.WorldX, m.WorldY = wx, wy
	}
	{
		lClick := &i.Mouse.ClickLeft
		key := ebi.MouseButtonLeft

		lClick.IsPressed = ebi.IsMouseButtonPressed(key)

		if lClick.IsPressed {
			lClick.IsPressedTimer += dt
		} else {
			lClick.IsPressedTimer = 0
		}

		lClick.IsJustPressed = inpututil.IsMouseButtonJustPressed(key)
		lClick.IsJustReleased = inpututil.IsMouseButtonJustReleased(key)
	}
	{
		rClick := &i.Mouse.ClickRight
		key := ebi.MouseButtonRight

		rClick.IsPressed = ebi.IsMouseButtonPressed(key)

		if rClick.IsPressed {
			rClick.IsPressedTimer += dt
		} else {
			rClick.IsPressedTimer = 0
		}

		rClick.IsJustPressed = inpututil.IsMouseButtonJustPressed(key)
		rClick.IsJustReleased = inpututil.IsMouseButtonJustReleased(key)
	}
}
func (i *Input) UpdateKeys(dt SecsDT) {
	if len(i.Keymap) == 0 {
		panic("input probably not initialized")
	}

	avoidDupes := map[*inputTracked]struct{}{}
	for key, tracker := range i.Keymap {
		if _, handledAlready := avoidDupes[tracker]; handledAlready {
			continue
		}

		tracker.IsPressed = ebi.IsKeyPressed(key)

		if tracker.IsPressed {
			avoidDupes[tracker] = struct{}{}
			tracker.IsPressedTimer += dt
		} else {
			tracker.IsPressedTimer = 0
		}

		tracker.IsJustPressed = inpututil.IsKeyJustPressed(key)
		tracker.IsJustReleased = inpututil.IsKeyJustReleased(key)
	}
}

//

func (i *Input) SetKeymapWithFunc(fn func(*Input) map[ebi.Key]*inputTracked) {
	i.Keymap = fn(i)
}
func (i Input) ListCurrentBindings() {
	log.Printf("CURRENT BINDINGS: \n%s", i.currentBindingsFormatted())
}
func (i Input) currentBindingsFormatted() string {
	var sb strings.Builder
	// TODO:  Have a way (probably `reflect`) to find the variable names
	//   of these bindings without me actually naming them.
	//   Or we can do struct tags, but I'd rather not do that.
	for key := range i.Keymap {
		sb.WriteString(key.String())
		sb.WriteRune('\n')
	}
	return sb.String()
}

func input__milky(i *Input) map[ebi.Key]*inputTracked {
	return map[ebi.Key]*inputTracked{
		ebi.KeyQ:     &i.QuitNow,
		ebi.KeyLeft:  &i.Player.Left,
		ebi.KeyH:     &i.Player.Left,
		ebi.KeyA:     &i.Player.Left,
		ebi.KeyRight: &i.Player.Right,
		ebi.KeyN:     &i.Player.Right,
		ebi.KeyD:     &i.Player.Right,
	}
}
func input__normalPerson(i *Input) map[ebi.Key]*inputTracked {
	return input__milky(i)
}

//

type inputTracked struct {
	IsPressed      bool
	IsJustPressed  bool
	IsJustReleased bool
	IsPressedTimer SecsDT
}
