package helpers

import (
	C "github.com/fatih/color"

	"fmt"
	"log"
	"path/filepath"
	"runtime"
	"strings"
)

func Assert(mustBeTrue bool, panicValue string) {
	if mustBeTrue == false {
		panic("assert: " + panicValue)
	}
}

func CanPanic(err error) {
	if err != nil {
		panic(err)
	}
}

//
//  Custom error values
//

type SimpleError string

var Quitkey SimpleError = "quit by key"

func (s SimpleError) Error() string { return string(s) }

//
//  Panic Traces
//

// Intercepting panics in the system to create prettier stack traces
// to debug and read.

type frame struct {
	filepath, functionName string
	line                   int
	isOurFile              bool
}

// Colors for the panic output
var (
	c_ownedFile  = C.New(C.Bold, C.FgHiMagenta).Sprint
	c_lib        = C.New(C.Faint).Sprint
	c_panicVal   = C.New(C.Bold, C.FgRed).Sprint
	c_panicStart = C.New(C.Bold, C.BgRed, C.FgWhite).Sprint
)

// Recover from panics by grabbing a stack trace ourselves,
// reformatting it and fatalling the output to end the program.
//
// Note: This func has to be called in every goroutine in order
//   to recover from that goroutine's panic, otherwise we'll get
//   the ugly output.
func GrabAndReformatPanics() {
	panicValue := recover() // The thing in `panic(thing)`
	if panicValue == nil {
		// If a `panic(nil)` was called or if there no panic at all
		return
	}

	stacktrace := formattedStackTrace()
	log.Fatalf(c_panicStart(" ---------- Panic!!")+"\n"+
		"%s\n"+
		"%s"+ // Stacktrace has its own breakline
		"%s\n",
		c_panicVal(panicValue), stacktrace, c_panicVal(panicValue))
}

func grabstacktrace() []frame {
	frames := make([]frame, 0)

	// How many layers of functions does this error handling occupy
	// until we get to the actual error getting thrown.
	const HideLoggingFunctions = 0

	for i := HideLoggingFunctions; ; i++ {
		pc, path, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		fn := runtime.FuncForPC(pc)

		if strings.Contains(path, "go/src/runtime") {
			// Skip go's internals, we can't do anything about it
			continue
		}

		frames = append(frames, frame{
			functionName: fn.Name(),
			filepath:     path,
			line:         line,
			isOurFile:    strings.Contains(path, "gamejams"),
		})
	}
	return frames
}
func formattedStackTrace() string {
	var stacktrace string

	// @todo; Probably add some colors to it, shuffle things around

	frames := grabstacktrace()
	var entries = make([]string, 0, len(frames))
	for _, f := range frames {
		var str string
		if f.isOurFile {
			str = fmt.Sprintf(""+
				" %s \n"+
				"  %5d : %s\n",
				f.functionName, f.line,
				filepath.Base(f.filepath),
			)
			// Apply a color
			str = c_ownedFile(str)
		} else {
			str = fmt.Sprintf(""+
				" %s\n"+
				"  %5d : %s\n",
				f.functionName, f.line,
				f.filepath,
			)
		}
		entries = append(entries, str)
	}
	stacktrace += strings.Join(entries, "")

	return stacktrace
}
