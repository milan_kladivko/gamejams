package helpers

import (
	"math/rand"
)

// =========================================================================
//   Entity IDs, DOD
// =========================================================================

type (
	EntityID   uint32
	EntityType uint

	Entity struct {
		ID   EntityID
		Type EntityType
	}

	EntitySet   map[EntityID]struct{}
	EntitySlice []EntityID
)

// Set of entity IDs that are already taken, used for lookup
// for uniqueness whenever we need a new ID.
var takenIDs map[EntityID]struct{}

func NewEntityID() EntityID {
	for {
		id := EntityID(rand.Uint32())
		if _, taken := takenIDs[id]; taken {
			continue
		}
		return id
	}
}

//

const (
	TYPE_UNSET  EntityType = iota // =0
	TYPE_PLAYER                   // =1
	TYPE_ALLY
	TYPE_ENEMY
	TYPE_PICKUP
	TYPE_FLOOR
	TYPE_BACKGROUND
	TYPE_FOREGROUND

	TYPE_PARTICLE

	// Not visible (logical) entities
	TYPE_TIMER EntityType = iota + 100
	TYPE_ANIMATION_PLAYER
)

var (
	TYPE_CHARS = map[EntityType]byte{}
	TYPE_ENUMS = map[byte]EntityType{}
)

func init() {
	TYPE_CHARS = map[EntityType]byte{
		TYPE_PLAYER: '*',
		TYPE_ENEMY:  '!',
		TYPE_PICKUP: 'n',
		TYPE_FLOOR:  '_',
	}
	for en, ch := range TYPE_CHARS {
		TYPE_ENUMS[ch] = en
	}
}
func (t EntityType) String() string {
	return string(TYPE_CHARS[t]) // defaults to "" empty string
}
