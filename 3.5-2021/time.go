package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	"jam/assets"
	. "jam/helpers"
	. "jam/timers"

	"log"
	"time"
)

type Time struct {
	// Timestamp of current frame's `Update()` call
	FrameStart time.Time

	DT          SecsDT // Diff Time between current and last frame's `Update()`
	TotalTime   SecsDT // Total number of elapsed time since the game's start
	TotalFrames int64  // Total number of state updates since the game's start

	DeletedTimers    map[EntityID]struct{} // Like a queue for deleted timers
	Timers           map[EntityID]*Timer
	AnimationPlayers map[EntityID]*assets.AnimationPlayer
}

//

func (t *Time) UpdateDTAndTotals() SecsDT {
	now := time.Now()
	dt := now.Sub(t.FrameStart)
	t.FrameStart = now

	// Clamp to some small-ish value when severely lagging.
	dt_max := time.Millisecond * 200
	if dt > dt_max {
		dt = dt_max
	}

	if LOG_FRAMERATE {
		log.Printf("dt:  %.2fms", dt.Seconds()*1000)
		log.Printf("FPS: %.2f", ebi.CurrentFPS()) // Gets averaged by lib
	}

	t.DT = dt.Seconds() // :: Saved into the global for testing access
	t.TotalTime += t.DT
	t.TotalFrames += 1 // Assuming one update per frame here

	return t.DT // :: Returned for utility
}
func (t *Time) UpdateAnimationPlayers(dt SecsDT) {
	for _, playerPtr := range t.AnimationPlayers {
		playerPtr.Next(t.DT)
	}
}

//

type TimerOps struct {
	// Reversing could be a thing we might need
	KillAfter SecsDT
}

func (t *Time) NewTimer(ops TimerOps) *Timer {
	id := NewEntityID()
	timer := Timer{
		Entity:   Entity{id, TYPE_TIMER},
		Deadline: ops.KillAfter,
	}
	t.Timers[id] = &timer
	return &timer
}
func (t *Time) UpdateAndDeleteTimers(dt SecsDT) {

	// NOTE:  Deletion here means that we'll delete the pointers
	//   to the timers from our map. This doesn't mean that
	//   the timer itself is destroyed -- just that when the timer
	//   expires, we won't update it anymore.

	toDelete := map[EntityID]struct{}{}

	for id := range t.Timers {
		tim := t.Timers[id]
		if tim.Deadline > 0 {
			if tim.Secs >= tim.Deadline {
				toDelete[id] = struct{}{}
				continue
			}
		}
		tim.Secs += dt
		tim.Updates++
		t.Timers[id] = tim
	}

	for id := range toDelete {
		delete(t.Timers, id)
	}

	// Overwrite the old list, might be an empty map
	t.DeletedTimers = toDelete
}
func (t *Time) HasTimerJustEnded(id EntityID) bool {
	// NOTE:  Retrieving from a nil-map would still work (returns false)
	_, ok := t.DeletedTimers[id]
	return ok
}
