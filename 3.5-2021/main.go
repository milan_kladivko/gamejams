/*
  The jam's page: https://itch.io/jam/nokiajam3
*/
package main

import (
	"log"
	"time"

	"jam/assets"
	. "jam/helpers"
	. "jam/timers"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

// ==========================================================================
//   Constants and flags
// ==========================================================================

var ( // Printing and logging constants @conf
	PRETTY_PRINT_PANICS = true
	LOG_SOUNDLOAD_INIT  = true
	LOG_FRAMERATE       = false // TODO:  Draw an in-game framerate graph
	LOG_UPDATE_TIME     = false
	LOG_DRAW_TIME       = false
)

var ( // Float is better for calculations, avoid casts
	RENDER_W   = float64(512)
	RENDER_H   = float64(40)
	INIT_SCALE = 2.0 // Scaling the whole window up a bit
)

// ==========================================================================
//   Important globals
// ==========================================================================

var (
	GAME Game

	_tester tester // Hook testing code in build-tagged files `*_tagtest.go`
)

func main() {
	defer GrabAndReformatPanics()

	CanPanic(Init())

	// Starts the game loop
	err := ebi.RunGame(&GAME)
	if err != nil {

		// RunGame returns error when
		// 1) OpenGL error happens,
		// 2) audio error happens or
		// 3) our (*Game).Update() returns an error.

		if err == Quitkey {
			log.Printf("Quit the game with a Q key")
			return
		}
		panic(err)
	}
}

func Init() error {

	{ // Window settings
		w, h := int(RENDER_W*INIT_SCALE), int(RENDER_H*INIT_SCALE)
		ebi.SetWindowSize(w, h)
		log.Printf("INITIAL WINDOW SIZE: %d x %d", w, h)

		ebi.SetWindowTitle("Kingdom Demake")
		ebi.SetWindowResizable(true)
	}

	assets.Sound.Init(LOG_SOUNDLOAD_INIT)
	GAME.Init()

	return nil
}
func (g *Game) Init() {
	g.Lock()
	defer g.Unlock()

	g.Time.AnimationPlayers = map[EntityID]*assets.AnimationPlayer{}
	g.Time.Timers = map[EntityID]*Timer{}
	g.Time.DeletedTimers = nil // Gets overwritten every frame

	g.World.Init()

	if WEBMODE {
		g.Input.SetKeymapWithFunc(input__normalPerson)
	} else {
		g.Input.SetKeymapWithFunc(input__milky)
	}
}

// Update proceeds the game state.
// Update is called every render tick by default.
// If an error is passed, it will get returned by `ebi.RunGame(g)`.
func (g *Game) Update() error {
	defer GrabAndReformatPanics()
	// TODO:  Ideally we'd be locking the used maps separately
	//   and not the entire game state.
	// As soon as we run a separate goroutine under a timer,
	// we'll get slammed by this.
	g.Lock()
	defer g.Unlock()

	if LOG_UPDATE_TIME {
		start := time.Now()
		defer func() { log.Printf("Update: %s", time.Now().Sub(start)) }()
	}

	dt := g.Time.UpdateDTAndTotals()
	g.Time.UpdateAndDeleteTimers(dt)
	g.Time.UpdateAnimationPlayers(dt)

	if LOG_FRAMERATE {
		log.Printf("FPS: %.3f", 1.0/dt)
	}

	//

	g.Input.UpdateMouse(dt, g.Camera)
	g.Input.UpdateKeys(dt)
	if g.Input.QuitNow.IsJustPressed && !WEBMODE {
		return Quitkey
	}

	{ // Testing hook
		if _tester != nil { // Testing skips scenes with the testing one
			return _tester.Update()
		}
	}

	// Scene switches

	if g.LastFrameScene != g.CurrentScene {
		defer func() { g.LastFrameScene = g.CurrentScene }()
		log.Printf("scene switched")
	}

	switch g.CurrentScene {
	case SCENE_Start:
		w := &g.World
		w.ThePlayer.Update(dt, g.Input, g.World)
		w.UpdateAllies(dt)

		{
			rclick := g.Input.Mouse.ClickRight
			if rclick.IsJustReleased || rclick.IsJustPressed { // Focus will change
				g.Camera.TargetSwitched()
			}
			if rclick.IsPressed {
				screenX := float64(g.Input.Mouse.ScreenX)
				wPos := screenX / RENDER_W * g.World.WidthTotal
				g.Camera.EasingFactor = 0.2
				g.Camera.TargetMovedTo(wPos, GROUND_Y)
			} else {
				playerPos, ok := w.Positions.OfEntity[w.ThePlayer.ID]
				Assert(ok, "")
				g.Camera.EasingFactor = 0
				g.Camera.TargetMovedTo(playerPos.X+playerPos.W/2, GROUND_Y)
			}
		}

		w.UpdatePositionsToSprites()
		w.MoveClouds(dt)
	}

	// Camera updates; after the game state updates
	g.Camera.Update(dt)

	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebi.Image) {
	defer GrabAndReformatPanics()

	g.Lock()
	defer g.Unlock()

	screen.Fill(REND_COLOR_BACKGROUND)

	if LOG_DRAW_TIME {
		start := time.Now()
		defer func() { log.Printf("Draw: %s", time.Now().Sub(start)) }()
	}

	if _tester != nil {
		_tester.Draw(screen)
		return
	}

	g.World.DrawSprites(screen, g.Camera)
}

// Layout takes the outside size (e.g., the window size) and returns
// the (logical) screen size.
// If you don't have to adjust the screen size with the outside size,
// just return a fixed size.
func (g *Game) Layout(win_w, win_h int) (render_w, render_h int) {
	var before = RENDER_W
	{
		RENDER_W = float64(win_w) / INIT_SCALE
		// TODO:  Support non-default scaling
	}
	if RENDER_W != before {
		log.Printf("Set new render width: %.0f", RENDER_W)
	}
	return int(RENDER_W), int(RENDER_H)
}
