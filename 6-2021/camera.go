package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"math"

	. "jam/helpers"
)

const EASING_DURATION = SecsDT(1)

type Camera struct {
	X, Y float64

	TargetX, TargetY         float64
	TargetXPrev, TargetYPrev float64

	EasingTimer  Timer // Not connected to the .Time part
	EasingFactor float64
}

// The way Camera should be called is that whenever the same
// target moves, `TargetMovedTo()` should be called, with the new
// positions.
// If we're focusing on a different target, call `TargetSwitched()`
// and expect a lerp from the most recent position to the position
// of the supposed new target.
// This allows the camera a lot of freedom in the way it handles
// changes in focus and easing between positions.

func (c *Camera) TargetMovedTo(x, y float64) {
	dx, dy := x-c.TargetX, y-c.TargetY
	ease := c.EasingFactor
	if ease != 0 {
		dx *= ease
		dy *= ease
		//Printf("%f", ease)
	}
	c.TargetX += dx
	c.TargetY += dy
}
func (c *Camera) TargetSwitched() {
	c.TargetXPrev, c.TargetYPrev = c.TargetX, c.TargetY
	c.EasingTimer.Reverse() // Reset the timer
}

func (c *Camera) Update(dt SecsDT) {
	{ // Manual timer update, we need a bit of control
		c.EasingTimer.Deadline = EASING_DURATION // @conf
		c.EasingTimer.Secs += dt
		Clampp(&c.EasingTimer.Secs, 0, c.EasingTimer.Deadline)
		c.EasingTimer.Updates++
	}

	// Lerp it all into one
	var (
		tx, ty   = c.TargetX, c.TargetY
		txp, typ = c.TargetXPrev, c.TargetYPrev
		t        = c.EasingTimer.Completed_01()
	)
	t = Easing_Quad(t)
	c.X = Lerp(txp, tx, t)
	c.Y = Lerp(typ, ty, t)
}

// GeoM returns the camera matrix with a screen-centering offset
// and a translation by the camera's position.
func (c Camera) GeomCamNoY() (g ebi.GeoM) {
	g.Translate(
		math.Round(-c.X),
		0) // No Y
	return g
}
func (c Camera) GeomScreenCenter() (g ebi.GeoM) {
	g.Translate(
		math.Floor(RENDER_W/2),
		0)
	return g
}
