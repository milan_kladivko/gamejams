package helpers

import (
	"image/color"
)

// ==========================================================================
//   Parse colors from Hex strings
//   https://stackoverflow.com/questions/54197913
// ==========================================================================

func RGBFromHex(s string) (c color.RGBA) {
	c.A = 0xff // Always true

	if s[0] != '#' {
		panic("colorhex parse: needs to start with a hash")
	}

	hexToByte := func(b byte) byte {
		switch {
		case b >= '0' && b <= '9':
			return b - '0'
		case b >= 'a' && b <= 'f':
			return b - 'a' + 10
		case b >= 'A' && b <= 'F':
			return b - 'A' + 10
		}
		panic("colorhex parse: invalid character " + string(b))
	}

	switch len(s) {
	case 7:
		c.R = hexToByte(s[1])<<4 + hexToByte(s[2])
		c.G = hexToByte(s[3])<<4 + hexToByte(s[4])
		c.B = hexToByte(s[5])<<4 + hexToByte(s[6])
	case 4:
		c.R = hexToByte(s[1]) * 17
		c.G = hexToByte(s[2]) * 17
		c.B = hexToByte(s[3]) * 17
	default:
		panic("colorhex parse: invalid string length")
	}
	return
}

var __usedHexColors = map[string]color.RGBA{}

func Hex(s string) color.RGBA {
	if cached, ok := __usedHexColors[s]; ok {
		return cached
	} else {
		new := RGBFromHex(s)
		__usedHexColors[s] = new
		return new
	}
}
