package helpers

import (
	"time"
)

// In the entire project, we'll be using the same DT unit.
// Working with math is weird enough, no point in throwing in nanos
// that `time.Duration` uses. Not good for games.
// This is syntax for being able to still use `float64` for times
// interchangably, but mentioning `SecsDT` where appropriate is preferrable.
type SecsDT = float64

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function.
func DurationToDt(d time.Duration) SecsDT {
	// Note: Yes, this doesn't truncate the seconds. It returns the fractional
	// conversion of nanoseconds to seconds.
	return d.Seconds()
}

//

type Timer struct {
	Entity
	Secs     SecsDT // Elapsed seconds
	Deadline SecsDT
	Updates  uint64
}

func (tr Timer) Elapsed() SecsDT { return tr.Secs }

func (tr Timer) HasDeadline() bool { return tr.Deadline > 0 }
func (tr Timer) Remaining() SecsDT {
	if !tr.HasDeadline() {
		return -1
	}
	return tr.Deadline - tr.Secs
}
func (tr *Timer) Reverse() {
	tr.Secs = tr.Remaining()
	// .ElapsedTTL stays the same
	// .Updates are not touched
}
func (tr Timer) Completed_01() float64 {
	if !tr.HasDeadline() {
		return 0 // Never elapses its deadline
	}
	return Clamped(tr.Secs/tr.Deadline, 0, 1)
}
func (tr Timer) Remaining_01() float64 {
	if !tr.HasDeadline() {
		return 1 // Always has the whole life ahead of it
	}
	return Clamped(1-(tr.Secs/tr.Deadline), 0, 1)
}
