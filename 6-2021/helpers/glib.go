package helpers

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
)

// Constants for directions that might be different for some libraries,
// namely Pixel.
const (
	UP    = -1
	DOWN  = +1
	LEFT  = -1
	RIGHT = +1
)

func ImageSize(i *ebi.Image) (wid, hei float64) {
	b := i.Bounds()
	return float64(b.Dx()), float64(b.Dy())
}
