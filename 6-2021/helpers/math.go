package helpers

import (
	"math"
)

// Clamped returns x clamped to the interval [min, max].
func Clamped(x, min, max float64) float64 {
	switch {
	default:
		return x
	case x < min:
		return min
	case x > max:
		return max
	}
}
func Clampp(x *float64, min, max float64) {
	*x = Clamped(*x, min, max)
}

func BoolToSign(b bool) float64 {
	if b {
		return 1
	} else {
		return 0
	}
}
func SignInt(a int) int {
	switch {
	case a >= +1:
		return +1
	case a <= -1:
		return -1
	default:
		return 0
	}
}
func SignFloat(a float64) float64 {
	switch {
	case a > 0:
		return +1
	case a < 0:
		return -1
	default:
		return 0
	}
}
func Abs(a float64) float64    { return math.Abs(a) }
func Floor(a float64) float64  { return math.Floor(a) }
func Ceil(a float64) float64   { return math.Ceil(a) }
func Round(a float64) float64  { return math.Round(a) }
func Pow(n, p float64) float64 { return math.Pow(n, p) }

//
//  Easing (Lerps)
//

func Lerp(a, b, t float64) float64 { return a*(1-t) + b*t }
func Easing_Cubic(t float64) float64 { // https://easings.net#easeInOutCubic
	if t < 0.5 {
		t = 4 * t * t * t
	} else {
		t = 1 - Pow(-2*t+2, 3)/2
	}
	return t
}
func Easing_Quad(t float64) float64 { // https://easings.net#easeInOutQuad
	if t < 0.5 {
		t = 2 * t * t
	} else {
		t = 1 - Pow(-2*t+2, 2)/2
	}
	return t
}
