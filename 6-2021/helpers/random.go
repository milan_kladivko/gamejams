package helpers

import (
	"math/rand"
	"time"
)

func init() { rand.Seed(time.Now().Unix()) }

func RandRangeInt(min, max int) int {
	if min > max {
		min, max = max, min // swap
	}
	return rand.Intn(max-min) + min
}
func RandRangeFloat(min, max float64) float64 {
	if min > max {
		min, max = max, min // swap
	}
	return rand.Float64()*(max-min) + min
}
