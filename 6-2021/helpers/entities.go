package helpers

import (
	"math/rand"
)

/*
   To achieve "generic" code in Go, we'll be using entities and many maps
   of these entities.

   Hopefully, this won't limit us too much.

   The idea is to basically never have pointers but instead have IDs to these
   entities and look them up in one map to make sure we have the correct data;
   or use the structs without a pointer, being completely sure that it's a copy
   and will not affect any other code in unexpected ways.

   TLDR;  I don't really know what I'm doing here, it's an experiment.
*/

type (
	EntityID   uint32
	EntityType uint

	Entity struct {
		ID   EntityID
		Type EntityType
	}

	EntitySet   map[EntityID]struct{}
	EntitySlice []EntityID
)

// Set of entity IDs that are already taken, used for lookup
// for uniqueness whenever we need a new ID.
var takenIDs map[EntityID]struct{}

func NewEntityID() EntityID {
	for {
		id := EntityID(rand.Uint32())
		if _, taken := takenIDs[id]; taken {
			continue
		}
		return id
	}
}

//

const (
	TYPE_UNSET  EntityType = iota // =0
	TYPE_PLAYER                   // =1
	TYPE_ALLY
	TYPE_ENEMY
	TYPE_PICKUP
	TYPE_FLOOR
	TYPE_BACKGROUND
	TYPE_FOREGROUND

	TYPE_PARTICLE

	// Not visible (logical) entities
	TYPE_TIMER EntityType = iota + 100
	TYPE_ANIMATION_PLAYER
)

var (
	TYPE_CHARS = map[EntityType]byte{}
	TYPE_ENUMS = map[byte]EntityType{}
)

func init() {
	TYPE_CHARS = map[EntityType]byte{
		TYPE_PLAYER: '*',
		TYPE_ENEMY:  '!',
		TYPE_PICKUP: 'n',
		TYPE_FLOOR:  '_',
	}
	for en, ch := range TYPE_CHARS {
		TYPE_ENUMS[ch] = en
	}
}
func (t EntityType) String() string {
	return string(TYPE_CHARS[t]) // defaults to "" empty string
}
