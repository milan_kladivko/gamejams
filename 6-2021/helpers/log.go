package helpers

import (
	stdlog "log"
	"strings"
	"time"

	C "github.com/fatih/color"

	"fmt"

	"github.com/davecgh/go-spew/spew"
)

//
//   Pretty logging with colors and fit-for-games timestamps
//

// TODO:  Add a configurable bool that will enable showing where
//   the log is being called from. (@conf)

func init() {
	{ // Spew setup
		c := spew.Config
		c.Indent = "  "
	}
	{ // Default std `log` setup
		stdlog.SetFlags(0) // Disabling builtin timestamp and other decor
		if WEBMODE {
			stdlog.SetOutput(new(webNilWriter))
		} else {
			stdlog.SetOutput(new(logWriter))
		}
		stdlog.Printf("Pretty logging initialized.")
	}
}

// https://github.com/davecgh/go-spew#sample-formatter-output
func Spew(o interface{}) string { return spew.Sdump(o) }
func LogSpew(o interface{})     { stdlog.Print(Spew(o)) }

func Printf(format string, args ...interface{}) {
	stdlog.Printf(format, args...)
}

//
//  Webmode /vs/ native loggers
//

// Logging to a console in a browser is ridiculously slow for logging
// anything every frame in a game. Just don't do it.
// Panics should still go through though, if I'm not mistaken...

// TODO:  Instead of throwing out the logs, make a log HTML element
//   which should be faster to write to.

// TODO:  Check our panic recovery if logs in the browser; @errors.go.

type webNilWriter struct{}

func (webNilWriter) Write(bytes []byte) (int, error) {
	return len(bytes), nil
}

// We want pretty logging though, when we develop.

type logWriter struct{}

func (logWriter) Write(bytes []byte) (int, error) {
	var timestamp string
	{ // Use a timestamp prefix
		now := time.Now()
		minuteSecond := now.Format("04'05\"")
		millis := now.Format(".999")

		// Align milliseconds, pad with zeroes on the right
		if millis != "" { // Can just be empty if we have exact seconds
			millis = millis[1:] // Don't include the dot
		}
		millis = millis + strings.Repeat("0", 3-len(millis))

		const WITH_COLOR = true
		if WITH_COLOR {
			timestamp = C.YellowString("%s%s ║ ", minuteSecond, millis)
		} else {
			timestamp = fmt.Sprintf("%s%s ║ ", minuteSecond, millis)
		}
	}
	return fmt.Print(timestamp + string(bytes))
}
