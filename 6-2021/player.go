package main

import (
	"jam/assets"
	. "jam/helpers"
)

type ThePlayer struct {
	Entity
	FacingLeft bool

	Dx, Dy float64
}

func NewPlayer() (ThePlayer, AnimatedSprite, Position) {

	Assert(PLAYER_ID == 0, "tried to make 2 players")
	PLAYER_ID = NewEntityID()

	const w, h = 11, 10

	var (
		__e    = Entity{ID: PLAYER_ID, Type: TYPE_PLAYER}
		player = ThePlayer{
			Entity:     __e,
			FacingLeft: false,
		}
		sprite = AnimatedSprite{
			Entity:  __e,
			Origin:  ORIGIN_CENTERBOTTOM,
			AP:      assets.DebugAnimation(w, h, Hex("#a11"), __e),
			Outline: OUTLINE_BLACK,
		}
		position = Position{
			Entity: __e,

			X: 0, Y: 0, // Initialized outside
			W: w, H: h,
		}
	)

	return player, sprite, position
}

func (dude *ThePlayer) Update(dt SecsDT, input Input, world World) {
	id := dude.ID

	pos, ok := world.Positions.OfEntity[id]
	Assert(ok, "player needs a position")
	defer func() { world.Positions.OfEntity[id] = pos }()

	//

	var (
		//@conf
		Xmax = 60.0 // (x per second)
		Xacc = 1.0

		//
		inp = input.Player // alias
		dx  = 0.0
	)

	switch {
	case inp.Left.IsPressed:
		dx += Xacc * dt * LEFT
	case inp.Right.IsPressed:
		dx += Xacc * dt * RIGHT
	}

	Clampp(&dx, -Xmax*dt, +Xmax*dt)
	pos.X += dx

	{
		sprite, ok := world.Sprites[id]
		Assert(ok, "player sprite flipping")
		defer func() { world.Sprites[id] = sprite }()
		if Abs(dx) > (0.01 /*world pos diff*/) {
			sprite.Flip = dx < 0
		}
	}
}
