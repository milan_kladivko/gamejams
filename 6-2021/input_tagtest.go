//+build inputtest

package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	ut "github.com/hajimehoshi/ebiten/v2/ebitenutil"

	"fmt"
	"image/color"
	"log"
)

func init() {
	// Override normal behavior, run our update+draw instead
	// of scenes.
	_tester = inputTesting{}
	log.Printf("\n\n\n            INPUT TESTING  \n\n\n")
	LOG_FRAMERATE = false

	RENDER_W = 600
	RENDER_H = 800
	INIT_SCALE = 1
}

//

type inputTesting struct{}

func (t inputTesting) Update() error { return nil } // Do nothing, it's handled already

func (t inputTesting) Draw(s *ebi.Image) {
	s.Fill(color.Black)

	y := s.Bounds().Min.Y + 2
	line := func(in inputTracked, title string) {
		const x = 2

		ut.DebugPrintAt(s, fmt.Sprintf("%s: %s", title, &in), x, y)

		linelen := float64(in.IsPressedTimer * 100)
		ut.DrawLine(s, x+100, float64(y), x+100+linelen, float64(y), color.White)

		const CHAR_HEIGHT = 16
		y += CHAR_HEIGHT * 2
	}

	{
		i := &GAME.Input
		line(i.Player.Jump, "Jump:  ")
		line(i.Player.Down, "Down:  ")
		line(i.Player.Left, "Left:  ")
		line(i.Player.Right, "Right: ")
	}
}
