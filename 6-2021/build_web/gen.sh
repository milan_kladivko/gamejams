
# Ebiten tutorial: 
#   https://ebiten.org/documents/webassembly.html

# Don't clutter the root more than it already is

cd ..   # We want to be at the same level as the go files

echo "Source build..."
echo "  Generating asset embedding source code"


# Back up the old file
mv  \
   assets/files/gen.go  \
   assets/files/gen.go.old

# Don't forget to include the byte data
go run generator/filegen.go \
   -include-data \
   -out assets/files/gen.go
   -dir .
   

echo "  Compiling the Go code into webassembly"
GOOS=js GOARCH=wasm go build -o build_web/bin.wasm
#--tinygo build -o build_web/bin.wasm -target=wasm .


echo "  Rolling back the assets file"
# Put the backup back.
mv assets/files/gen.go.old assets/files/gen.go




echo "Copying the webassembly loader javascript code"
cp  $(go env GOROOT)/misc/wasm/wasm_exec.js        build_web/.
#--cp  $(tinygo env TINYGOROOT)/targets/wasm_exec.js  build_web/.




echo "Writing the entrypoint index.html file"
cat > build_web/index.html <<EOF

<!DOCTYPE html>
<script src="wasm_exec.js"></script>
<script>
 
 // Polyfill
 if (!WebAssembly.instantiateStreaming) {
     WebAssembly.instantiateStreaming = async (resp, importObject) => {
         const source = await (await resp).arrayBuffer();
         return await WebAssembly.instantiate(source, importObject);
     };
 }

 const go = new Go();
 WebAssembly.instantiateStreaming(fetch("bin.wasm"), go.importObject).then(result => {

     // Hide the loading heads-up
     document.getElementById("LOADING").remove()

     // Run the game code
     go.run(result.instance);
     
 });
</script>



<div id="LOADING"
     style="font-family:monospace; text-align:center; color:black; padding:20px;">
  <div style="font-size:32px">... LOADING ...</div>
  <div style="font-size:16px">Sorry, we don't have a fancy slider...</div>
</div>

<style>
 body {
     height: 100%; height: 100vh;
     padding: 0; margin: 0;
     box-sizing: border-box;
     border: 4px solid #a00; /*Not in focus*/
 }
 body.in-focus {
     border: 4px solid #111; /*Is fine, in focus*/
 }
</style>
<script>
 function onchange() {
     var cls = "in-focus" 
     if (document.hasFocus()) {
         document.body.classList.add(cls)
     } else {
         document.body.classList.remove(cls)
     }
 }
 // We really need it to be right
 window.addEventListener("focus", onchange)
 window.addEventListener("blur", onchange)
 onchange()
 setTimeout(onchange, 1000)
 
</script>

EOF




echo "Zipping the files"
cd build_web
zip build_web.zip \
    bin.wasm \
    wasm_exec.js \
    index.html
