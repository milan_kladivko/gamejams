package assets

import (
	"log"
	"strings"

	ebi "github.com/hajimehoshi/ebiten/v2"

	. "jam/helpers"
)

//

func LoadAnimationAndPlayer(
	abspath string, forEntity EntityID) *AnimationPlayer {
	// Note: Use this only for single-occurence animations,
	//   they are obviously not reused. Don't load animations multiple
	//   times please.
	return NewAnimationPlayer(LoadAnimation(abspath), forEntity)
}
func NewAnimationPlayer(
	anim *Animation, forEntity EntityID) *AnimationPlayer {
	ap := &AnimationPlayer{
		Entity:    Entity{forEntity, TYPE_ANIMATION_PLAYER},
		Animation: anim,

		FrameIndex:      0,
		DurationCounter: 0,

		PlayedTag:     Tag{},
		PlayedTagName: "",
		PlaybackType:  0,
	}
	ap.SwitchToDefaultTag()

	return ap
}

//

// A wrapper around the animation to allow playback.

type AnimationPlayer struct {
	Entity
	Animation      *Animation // Controlled animation data, can (and should) be shared
	AnimationLayer string     // One animation can have multiple layers

	FrameIndex      int    // Index of currently played frame
	DurationCounter SecsDT // Playback control, counter for a single frame, gets reset

	PlayedTag     Tag // Currently played tag of the animation
	PlayedTagName string

	PlaybackType PlaybackType // What to do when the player is at the end
}

//

type PlaybackType int

const (
	PLAYBACK_AT_END__LOOP PlaybackType = iota
	PLAYBACK_AT_END__STOP
	PLAYBACK_AT_END__NOTHING
	PLAYBACK_PAUSE
)

//

// Get the current sprite, as defined by the current frame of the player
func (pl AnimationPlayer) Sprite() *ebi.Image {
	return pl.CurrentFrame().SubImage
}

// Get the current frame, as defined by player's timers and played tag
func (pl AnimationPlayer) CurrentFrame() Frame {
	return pl.FrameByIndex(pl.FrameIndex)
}

// Get a frame by its index in the currently played tag
func (pl AnimationPlayer) FrameByIndex(frameIndex int) Frame {
	return pl.PlayedTag.Frames[frameIndex]
}

// CompletedFrames_01 returns the completion percentage by frame count.
// This is faster to check than checking time, but almost never what
// we actually want.
func (pl AnimationPlayer) CompletedFrames_01() float64 {
	return float64(pl.FrameIndex) / float64(len(pl.PlayedTag.Frames)-1)
}

// Completed_01 returns the completion percentage by time expired
// in the animation currently played.
func (pl AnimationPlayer) Completed_01() float64 {
	return pl.DurationCounter / pl.PlayedTag.TotalDuration()
}

const ANIMATION_TAG_DEFAULT = "default"

// Switch to the default aseprite tag, playing the full animation.
func (pl *AnimationPlayer) SwitchToDefaultTag() *AnimationPlayer {
	pl.PlayedTag = pl.Animation.TagDefault()
	pl.PlayedTagName = ANIMATION_TAG_DEFAULT
	return pl
}
func (pl *AnimationPlayer) SetLayer(layerName string) *AnimationPlayer {
	pl.AnimationLayer = layerName
	// TODO:  Validate
	return pl
}
func (pl *AnimationPlayer) SwitchToTag(tagName string) *AnimationPlayer {

	if pl.AnimationLayer != "" {
		// Prepend the tagName with the animation layer only
		// if that hasn't happened already. Might be "just probing".
		layerPrefix := pl.AnimationLayer + "/" // @layertag
		if !strings.HasPrefix(tagName, layerPrefix) {
			tagName = layerPrefix + tagName
		}
	}

	tag, isValidTag := pl.Animation.TagByName(tagName)
	if !isValidTag {
		if !WEBMODE { //@noreload
			_, stillExists := pl.Animation.TagByName(pl.PlayedTagName)
			if !stillExists {
				log.Printf(
					"And the tag we had isn't there anymore, switching to default")
				pl.SwitchToDefaultTag()
			}
		}
		log.Printf("[ERR] Tagswitch %s#%s; ignoring...\n",
			pl.Animation.Asset.WatchedFilepath, tagName)
		return pl
	}

	pl.PlayedTag = tag
	pl.PlayedTagName = tagName

	return pl
}

// Start the animation; reset the frame index.
func (pl *AnimationPlayer) Start() *AnimationPlayer {
	pl.FrameIndex = 0
	pl.DurationCounter = 0
	return pl
}

// Advance the animation frame by one unconditionally.
func (pl *AnimationPlayer) NextFrame() *AnimationPlayer {
	pl.FrameIndex++
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations; Supports going into negative numbers.
// NOTE:  Does not support skipping multiple frames in one call.
func (pl *AnimationPlayer) Next(dt SecsDT) *AnimationPlayer {

	// Some animations aren't actually animations
	if pl.PlaybackType == PLAYBACK_PAUSE {
		return pl
	}

	// TODO:  Introduce some "noreload" build tag or something :: @noreload
	if !WEBMODE {
		pl.SwitchToTag(pl.PlayedTagName)
	}

	if dt > 0 {
		pl.DurationCounter += dt

		var nextSwitch SecsDT = pl.CurrentFrame().Duration
		if pl.DurationCounter >= nextSwitch {
			// Careful about carrying the overflow into the next frame
			pl.DurationCounter -= nextSwitch
			pl.FrameIndex++
		}
	} else {
		pl.DurationCounter += dt // will be negative so it's the same

		// Next switch is trivial
		if pl.DurationCounter < 0 {
			// Leave the right overflow when going backwards
			var prevSwitch SecsDT = pl.FrameByIndex(pl.FrameIndex).Duration
			pl.DurationCounter += prevSwitch
			pl.FrameIndex--
		}
	}

	{ // What to do if we're at the end of an animation
		var (
			high = len(pl.PlayedTag.Frames) - 1
			low  = 0
		)
		switch pl.PlaybackType {
		case PLAYBACK_AT_END__NOTHING:
			break
		case PLAYBACK_AT_END__LOOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = low
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = high
			}
		case PLAYBACK_AT_END__STOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = high
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = low
			}
		}
	}

	// Note: Using `Next()` directly can overflow
	return pl
}

// If past the end, loop back to the start.
func (pl *AnimationPlayer) LoopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__LOOP
	return pl
}

// If past the end, stop at the end.
func (pl *AnimationPlayer) StopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__STOP
	return pl
}
