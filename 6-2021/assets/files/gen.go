
package files

// GENERATED ASSETS GO HERE

// Filenames listed as constants for autocompleting
const (
    FILE_build_web__glasspain_logo2_png = "build_web/glasspain_logo2.png"
    FILE_grefix__export__SFX_run_png = "grefix/export/SFX-run.png"
    FILE_grefix__export__ninigi_json = "grefix/export/ninigi.json"
    FILE_grefix__export__ninigi_png = "grefix/export/ninigi.png"
    FILE_grefix__export__regalia_json = "grefix/export/regalia.json"
    FILE_grefix__export__regalia_png = "grefix/export/regalia.png"
    FILE_grefix__wip__reg_png = "grefix/wip/reg.png"
)

// Groups by globs, indexed by whatever inclusion globs have been
// used as the argument when loading.
var GlobGroups = [][]string{ 
    { // from '**.json'
        FILE_grefix__export__ninigi_json,
        FILE_grefix__export__regalia_json,  
    }, 
    { // from '**.png'
        FILE_build_web__glasspain_logo2_png,
        FILE_grefix__export__SFX_run_png,
        FILE_grefix__export__ninigi_png,
        FILE_grefix__export__regalia_png,
        FILE_grefix__wip__reg_png,  
    }, 
    { // from '**.wav'  
    }, 
    { // from '**.mp3'  
    },
}

// Raw binary data of the assets.
// OMMITED. Apply the "-include-data" argument.
var Data = map[string][]byte{}

