package assets

import (
	"fmt"
	"image"
	_ "image/png"

	"bytes"
	"os"

	"log"
	"time"

	ebi "github.com/hajimehoshi/ebiten/v2"

	"jam/assets/files"
	. "jam/helpers"
)

// Loading files, either from the binary itself as constants' data
// or from the filesystem directly.
func mustLoadBytes(path string) []byte {
	b, e := loadBytes(path)
	CanPanic(e)
	return b
}
func loadBytes(path string) ([]byte, error) {
	if WEBMODE {
		return files.Data[path], nil
	}

	bytes, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return bytes, nil
}

//
//  Reloadable assets
//

type reloadable interface {
	HasNewerVersion() bool // These two can be filled into a type ...
	RecordLoaded()         // by embedding the `Asset` struct.

	Reload() // File reload function needs to be filled by subtypes.
}
type assetWatcher struct {
	Assets []reloadable
}

var AssetWatcher assetWatcher

func (a *assetWatcher) Add(assets ...reloadable) {
	if WEBMODE {
		return // noop
	}
	a.Assets = append(a.Assets, assets...)
}
func (a assetWatcher) TryReloadingAll() {
	for _, wa := range a.Assets {
		if wa.HasNewerVersion() {
			wa.Reload()
		}
	}
}

type Asset struct { // Embeddable
	WatchedFilepath string
	lastLoaded      time.Time
}

func (a *Asset) RecordLoaded() {
	a.lastLoaded = time.Now()
}
func (a Asset) HasNewerVersion() bool {
	if WEBMODE {
		return false // These never change as they're not on disk
	}

	s, err := os.Stat(a.WatchedFilepath)
	if err != nil {
		log.Printf("Warn: Couldn't read file's modtime: `%s`\n%s\n",
			a.WatchedFilepath, err)
		return false // Assume not changed on os error -- can't read it anyway
	}
	modified := s.ModTime()
	return modified.After(a.lastLoaded)
}

//
//  Graphics
//

type draw_ struct {
	// The target image for all drawing done through the variable
	TheScreen *ebi.Image
}

var Draw draw_

// Drawing util
func (d draw_) Topleft(what *ebi.Image, op *ebi.DrawImageOptions) {
	d.TheScreen.DrawImage(what, op) // That's the default, yo!
}
func (d draw_) Centered(what *ebi.Image, op *ebi.DrawImageOptions) {
	w, h := ImageSize(what)
	{ // Apply our recentering before all other transformations
		g := ebi.GeoM{}
		g.Translate(w/2, h/2)
		g.Concat(op.GeoM)
		op.GeoM = g
	}
	d.TheScreen.DrawImage(what, op)
}
func (_ draw_) Ops(x, y, rot float64) *ebi.DrawImageOptions {
	g := ebi.GeoM{}
	g.Rotate(rot)
	g.Translate(x, y)
	return &ebi.DrawImageOptions{GeoM: g}
}

//
//  Sprites, single images
//

type Sprite struct {
	Asset
	*ebi.Image
}

func MustLoadImage(path string) *ebi.Image {
	img, err := loadImage(path)
	CanPanic(err)
	return img
}
func loadImage(path string) (*ebi.Image, error) {
	data := mustLoadBytes(path)
	img, _, err := image.Decode(bytes.NewReader(data))
	if err != nil {
		return nil, fmt.Errorf("Couldn't decode image `%s`, %d : %v",
			path, len(data), err)
	}
	return ebi.NewImageFromImage(img), nil
}
