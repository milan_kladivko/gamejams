package main

import (
	"log"
	"sort"

	ebi "github.com/hajimehoshi/ebiten/v2"

	"jam/assets"
	. "jam/helpers"
)

var (
	PLAYER_ID EntityID = 0

	REND_COLOR_BACKGROUND = Hex("#f0f") // kick-in-the-face magenta
	REND_OUTLINE_XY       = [4][2]float64{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}

	REND_BACKGROUNDS_ANIM *assets.Animation

	GROUND_Y = float64(RENDER_H * 0.65)
)

type (
	World struct {
		ThePlayer ThePlayer
		Sprites   map[EntityID]AnimatedSprite
		Positions Positions
		Particles Particles
	}

	AnimatedSprite struct {
		Entity
		AP      *assets.AnimationPlayer
		X, Y    float64        // Copy over from positions
		Origin  DrawOriginType // Controls how the image is aligned for X,Y
		Flip    bool
		Outline OutlineType
	}

	DrawOriginType int
	OutlineType    int

	Positions struct {
		OfEntity map[EntityID]Position
		// TODO:  We'll probably want position regions to limit the amount
		//   of collision checking we have to do.
		//   That means bucketing entities into more specific maps
		//   based on their Y position.
	}
	Position struct {
		Entity
		X, Y float64
		W, H float64
	}
)

// =========================================================================

func (wPtr *World) Init() {

	p, pSprite, pPos := NewPlayer()

	pPos.X = RENDER_W * (5 / 2)
	pPos.Y = RENDER_H * 0.5

	*wPtr = World{
		ThePlayer: p,
		Positions: Positions{
			OfEntity: map[EntityID]Position{p.ID: pPos},
		},

		Sprites: map[EntityID]AnimatedSprite{p.ID: pSprite},

		Particles: Particles{
			Phys:      map[EntityID]Particle_Position{},
			Particles: map[EntityID]Particle{},
		},
	}
}

//

type randOps struct {
	InSky              bool
	DepthMin, DepthMax int
	Type               EntityType
}

// =========================================================================

func (w *World) UpdatePositionsToSprites() {
	var (
		positions = w.Positions.OfEntity
		sprites   = w.Sprites
	)
	for id, pos := range positions {
		sprite, ok := sprites[id]
		if !ok {
			Printf("positioned entity without a sprite %d", id)
			continue
		}
		{
			sprite.X, sprite.Y = pos.X, pos.Y
		}
		sprites[id] = sprite
	}
}

// =========================================================================

const (
	// Enum defines the alignment based on X,Y
	ORIGIN_LEFTTOP DrawOriginType = iota // The default
	ORIGIN_LEFTBOTTOM
	ORIGIN_CENTERBOTTOM
)
const (
	OUTLINE_NONE OutlineType = iota
	OUTLINE_BLACK
	OUTLINE_WHITE
)

func (wrd World) DrawSprites(screen *ebi.Image, cam Camera) {

	// TODO:  Filter out obviously offscreen sprites, even before sorting
	sprites := wrd.zSortedSprites()

	layerView := func(cam Camera) (layerView ebi.GeoM) {
		// Sprite positions are relative to the ground, 0 means on the ground
		layerView.Translate(0, +GROUND_Y)
		// Apply the screen centering
		layerView.Concat(cam.GeomScreenCenter())
		// Move the images with the camera
		layerView.Concat(cam.GeomCamNoY())
		return
	}(cam)

	for _, sprite := range sprites {

		img := sprite.AP.CurrentFrame().SubImage

		geom := spriteAlignedGeoM(sprite, img)

		x, y := layerView.Apply(sprite.X, sprite.Y)
		x, y = Ceil(x), Ceil(y) // Prevent camera float-sum off-by-1 glitches
		geom.Translate(x, y)

		maybeDrawOutline(screen, img, geom, sprite.Outline)
		screen.DrawImage(img, &ebi.DrawImageOptions{GeoM: geom})
	}
}

func spriteAlignedGeoM(s AnimatedSprite, img *ebi.Image) (mat ebi.GeoM) {

	w, h := float64(img.Bounds().Dx()), float64(img.Bounds().Dy())

	// Flipping and centering of the sprite,
	switch s.Origin {
	case ORIGIN_LEFTTOP:
		// Nothing, that's the default in the lib
	case ORIGIN_LEFTBOTTOM:
		mat.Translate(w*0, -h*1)
	case ORIGIN_CENTERBOTTOM:
		mat.Translate(-w/2, -h*1)
	}

	if s.Flip {
		mat.Scale(-1, 1)
	}
	return
}

//

func maybeDrawOutline(dest, src *ebi.Image, g ebi.GeoM, outline OutlineType) {
	if outline == OUTLINE_NONE {
		return
	}

	var colorm ebi.ColorM
	switch outline {
	case OUTLINE_BLACK:
		colorm.Scale(0, 0, 0, 1) // Kills all color, black outline
	case OUTLINE_WHITE:
		colorm.Translate(999, 999, 999, 0) // Pure white outline
	default:
		log.Printf("unknown outline: %d", outline)
		panic(true)
	}

	for _, xy := range REND_OUTLINE_XY {
		var geomOffset ebi.GeoM = g // copy
		geomOffset.Translate(xy[0], xy[1])
		dest.DrawImage(src, &ebi.DrawImageOptions{GeoM: geomOffset, ColorM: colorm})
	}
}

//

// TODO:  Filter out offscreen sprites

func (wrd World) zSortedSprites() []AnimatedSprite {

	// NOTE:  This is a lot of useless stuff that we do. It'll almost
	//   never change so unless some new entities are introduced,
	//   there won't be any modifications in this list. It calls
	//   for some kind of tree, but I don't really know how
	//   to do that in Go yet. I'll see how this will impact
	//   performance.

	typeLayers := layerOrderByEntityType
	arr := flattenedAnimatedSprites(wrd.Sprites)

	sort.Slice(arr, func(i, j int) bool {
		a, b := arr[i], arr[j]

		if a.Entity.Type == b.Entity.Type {
			// Fallback to the entity ID so that the order is preserved
			// between the frames. No z-fighting please.
			return a.ID > b.ID
		}
		// Sort by the ordering of the enum. This means that the enum
		// dictates the drawing order, which might not be expected.
		return typeLayers[a.Entity.Type] < typeLayers[b.Entity.Type]
	})

	return arr
}

var layerOrderByEntityType = func() (numberedLayers map[EntityType]int) {

	numberedLayers = map[EntityType]int{}

	order := []EntityType{
		TYPE_BACKGROUND,

		TYPE_PLAYER,
		TYPE_ENEMY,
		TYPE_ALLY,
		TYPE_PICKUP,

		TYPE_FOREGROUND,
		TYPE_UNSET, // So we see it clearly
	}

	for i := range order {
		numberedLayers[order[i]] = i
	}
	return numberedLayers
}()

func flattenedAnimatedSprites(m map[EntityID]AnimatedSprite) []AnimatedSprite {
	arr := make([]AnimatedSprite, len(m))
	{ // Pull into a sortable array
		var i = 0
		for _, s := range m {
			arr[i] = s
			i++
		}
	}
	return arr
}

//
