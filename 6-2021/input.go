package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"log"
	"strings"

	. "jam/helpers"
)

// NOTE:  If we use the `inpututil`, it's gonna track **ALL** input
//   there is and can be. Because we want to track specific stuff,
//   let's not use that unless we have to (gamepads maybe).

// We won't be using the input methods directly but rather define
// our gameplay toggleable actions as collections of information
// about any of the chosen input methods.
type Input struct {
	keymap map[ebi.Key]*inputTracked

	//

	QuitNow inputTracked

	Player struct{ Left, Right, Down, Jump inputTracked }
	UI     struct{ Up, Down, Left, Right, Select, Back inputTracked }

	Mouse struct {
		PrevScreenX, PrevScreenY int
		ScreenX, ScreenY         int

		WorldX, WorldY float64
		ClickLeft      inputTracked
		ClickRight     inputTracked
	}
}

func (i *Input) UpdateMouse(dt SecsDT, cam Camera) {
	{
		m := &i.Mouse

		x, y := ebi.CursorPosition()
		m.ScreenX, m.ScreenY = x, y

		// TODO:  We should have the world-to-screen matrix here...
		geom := cam.GeomCamNoY()
		geom.Translate(0, +GROUND_Y)
		geom.Concat(cam.GeomScreenCenter())

		geom.Invert()

		wx, wy := geom.Apply(float64(x), float64(y))
		m.WorldX, m.WorldY = wx, wy
	}
	{
		lClick := &i.Mouse.ClickLeft
		key := ebi.MouseButtonLeft

		lClick.IsPressed = ebi.IsMouseButtonPressed(key)

		if lClick.IsPressed {
			lClick.IsPressedTimer += dt
		} else {
			lClick.IsPressedTimer = 0
		}

		lClick.IsJustPressed = inpututil.IsMouseButtonJustPressed(key)
		lClick.IsJustReleased = inpututil.IsMouseButtonJustReleased(key)
	}
	{
		rClick := &i.Mouse.ClickRight
		key := ebi.MouseButtonRight

		rClick.IsPressed = ebi.IsMouseButtonPressed(key)

		if rClick.IsPressed {
			rClick.IsPressedTimer += dt
		} else {
			rClick.IsPressedTimer = 0
		}

		rClick.IsJustPressed = inpututil.IsMouseButtonJustPressed(key)
		rClick.IsJustReleased = inpututil.IsMouseButtonJustReleased(key)
	}
}
func (i *Input) UpdateKeys(dt SecsDT) {
	if len(i.keymap) == 0 {
		panic("input probably not initialized")
	}

	inv := map[*inputTracked][]ebi.Key{}
	for key, tk := range i.keymap {
		inv[tk] = append(inv[tk], key)
	}

	for tk, keys := range inv {
		before := tk.IsPressed
		tk.IsPressed = false

		for _, key := range keys {
			pressed := ebi.IsKeyPressed(key)
			if pressed {
				tk.IsPressed = true
			}
		}

		if tk.IsPressed != before {
			*tk = inputTracked{
				IsPressed:      tk.IsPressed,
				IsJustPressed:  tk.IsPressed,
				IsJustReleased: !tk.IsPressed,
				IsPressedTimer: 0,
			}
		} else {
			tk.IsJustPressed = false
			tk.IsJustReleased = false
			if tk.IsPressed {
				tk.IsPressedTimer += dt
			} else {
				tk.IsPressedTimer -= dt
			}
		}
	}
}

func (in *inputTracked) String() string {
	s := ""
	if in.IsPressed {
		s += "##"
	} else {
		s += "--"
	}
	if in.IsJustPressed {
		s += "!#"
	}
	if in.IsJustReleased {
		s += "!-"
	}
	return s
}

//

func (i *Input) SetKeymapWithFunc(fn func(*Input) map[ebi.Key]*inputTracked) {
	i.keymap = fn(i)
}
func (i Input) ListCurrentBindings() {
	log.Printf("CURRENT BINDINGS: \n%s", i.currentBindingsFormatted())
}
func (i Input) currentBindingsFormatted() string {
	var sb strings.Builder
	// TODO:  Have a way (probably `reflect`) to find the variable names
	//   of these bindings without me actually naming them.
	//   Or we can do struct tags, but I'd rather not do that.
	for key := range i.keymap {
		sb.WriteString(key.String())
		sb.WriteRune('\n')
	}
	return sb.String()
}

func input__milky(i *Input) map[ebi.Key]*inputTracked {
	return map[ebi.Key]*inputTracked{
		ebi.KeyQ:     &i.QuitNow,
		ebi.KeyLeft:  &i.Player.Left,
		ebi.KeyH:     &i.Player.Left,
		ebi.KeyA:     &i.Player.Left,
		ebi.KeyRight: &i.Player.Right,
		ebi.KeyN:     &i.Player.Right,
		ebi.KeyD:     &i.Player.Right,
	}
}
func input__normalPerson(i *Input) map[ebi.Key]*inputTracked {
	return input__milky(i)
}

//

type inputTracked struct {
	IsPressed      bool
	IsJustPressed  bool
	IsJustReleased bool
	IsPressedTimer SecsDT
}
