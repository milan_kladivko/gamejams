package main

/*

   # About

   For web builds to work correctly, we need to have to load files differently
   from a plain app that sits on a computer. We have two options:

   - Upload the files somewhere public and load the files as web files.
     That's not very good as we'd have to host those files somewhere
     and I'm not sure if itch.io lets us do that for web games.

   - Include the files' binary data right in the compiled go code.
     Preferrable but a bit harder to execute, perhaps.
     **This is the thing that we'll do here.**

   The solution is to basically read our assets that we'll be reading and
   format the data itself into valid Go code as if we hand-wrote the bytes
   ourselves.

   Loads of open-source solutions exist but most of them are geared towards
   deploying files into web servers, which isn't our case. Thus, I'm doing
   it myself.


   This file has a part with generating the asset binary data code
   and functions to load the binary data or the originals.

   Obviously, we'll want to use the actual files when developing,
   always use the generated embedded binary data on the web and also test
   the binary data if we need to.



   # GO 1.16 -- The `embed` package

   The new version of Go has an `embed` package that does embedding as we did.
   Their solution is very different from what we do right now. And different
   doesn't necessarily mean better.

   The std way is bundling the files at compile time via special comment
   directive `//go:embed ---filename---`. This then fills a variable
   that we might want.

   Their solution is obviously much faster than the "generate the whole file
   as source code that is included in the build as any other constant".
   As to what exactly it does -- that's up to some article to tell me.

   The solution immediatelly calls for a build tag file alternative for
   any large files that might slow down the process. Our file reloads
   would also suffer from it.

   TODO:  I'll think about using the `embed` package going forward.
     The solution we have right now isn't really good.

*/

import (
	"flag"

	templ "text/template"

	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"path/filepath"
)

var flags struct {
	// These are pointers just so we can parse them and fill them,
	// the values will never be nil after `flag.Parse()`.
	dir         *string
	includeData *bool
	verbose     *bool
}

// CLI arguments reading and help definition
func init() {
	defer flag.Parse()
	flags.dir = flag.String("dir", "", "[NECESSARY] Directory to scan")
	flags.verbose = flag.Bool("v", false, "Log extra information")
	flags.includeData = flag.Bool("include-data", false, ""+
		"Include asset byte data into the file.\n\n"+
		"The default only builds filename constants etc., no data is embedded.\n\n"+
		"NOTE:  Having the data in the source code slows down any parsing "+
		"which includes both building the binary and the language server "+
		"of most editors.")
}

func main() {
	log.SetFlags(0)

	if *flags.dir == "" {
		log.Println("The `-dir` flag has to be set.")
		flag.Usage()
	}

	log.Printf("Building assets...")
	filecontent := ParseDir(*flags.dir, "**.json", "**.png", "**.wav", "**.mp3")

	log.Printf("Writing the file...")
	err := ioutil.WriteFile("gen_assets.go", filecontent, 0644)
	if err != nil {
		panic(err)
	}

	log.Printf("Done.")
}

//

// Number of bytes in a written line, for readability?
var BlockWidth = 12

var EmbedFileTemplate = templ.Must(templ.New("file").Parse(`
package main

// GENERATED ASSETS GO HERE

// Filenames listed as constants for autocompleting
const (   
  {{- range .Files }}
    {{ .ConstName }} = "{{ .Filepath }}"
  {{- end }}
)

// Groups by globs, indexed by whatever inclusion globs have been
// used as the argument when loading.
var _assetGlobGroups = [][]string{
  {{- range .GlobGroups }} 
    { // from '{{ .Glob }}'
      {{- range .ConstNames }}
        {{ . }}, 
      {{- end }}  
    }, 
  {{- end }}
}

// Raw binary data of the assets.
{{- if .Config.IncludeData }}
var _assetData = map[string][]byte{
  {{ range .Files }}
    "{{ .Filepath }}": {
        {{ .ByteString }}
    },
  {{- end }}
}
{{- else }}
// OMMITED. Apply the "-include-data" argument.
var _assetData = map[string][]byte{} 
{{- end }}

`))

func ParseDir(dirpath string, includeglobs ...string) []byte {
	verbf := func(f string, a ...interface{}) {
		if *flags.verbose {
			log.Printf(f, a...)
		}
	}

	// Open up matching files, read their byte data and format it
	// into a piece of golang code for writing byte arrays.

	log.Printf("path: %s, globs: %v", dirpath, includeglobs)

	type globGroup struct {
		Glob       string
		ConstNames []string
	}
	var globGroups = make([]globGroup, len(includeglobs))
	for i := range globGroups {
		globGroups[i].Glob = includeglobs[i]
	}
	// TODO:  A single file should be able to be contained in multiple
	//   glob groups, as a player png could be both in the player category
	//   and the png category.

	type readFile struct {
		ByteString string // String with valid golang code with the raw data
		Filepath   string // Original file path of the file
		ConstName  string // Go-safe name for use instead of the filepath
	}
	var files []readFile
	err := filepath.Walk(dirpath, func(fpath string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil // Can't open directories, can I now
		}

		verbf("trying `%s`...", fpath)

		var matches bool
		var globGroupIndex int
		for i, g := range includeglobs {
			ok, err := filepath.Match(g, filepath.Base(fpath))
			if err != nil {
				return nil
			}
			if ok {
				verbf("... Matched `%s`", g)
				globGroupIndex = i
				matches = true
				break
			}
		}
		if matches {
			verbf("... Opening the file %s", fpath)
			file, err := os.Open(fpath)
			if err != nil {
				return err
			}
			bytedataString, err := reader(file, 0)
			if err != nil {
				return err
			}
			var constName = fpath
			{ // Transform filename into a valid go variable name
				constName = strings.ReplaceAll(constName, "/", "__")
				constName = strings.ReplaceAll(constName, ".", "_")
				constName = strings.ReplaceAll(constName, "-", "_")
				constName = strings.ReplaceAll(constName, " ", "_")
				// NOTE: Obviously, other symbols are not good

				constName = "FILE_" + constName // Prefix for autocomplete
			}

			// Append the entry with data necessary for the template
			files = append(files, readFile{
				Filepath:   fpath,
				ByteString: bytedataString,
				ConstName:  constName,
			})
			globGroups[globGroupIndex].ConstNames = append(
				globGroups[globGroupIndex].ConstNames, constName)

			log.Printf(" [%d]  +`%s`", globGroupIndex, constName)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}

	// Shove the data into the template to append other cruft that will
	// make the data a valid file.
	var (
		buf  bytes.Buffer
		data = map[string]interface{}{
			"Files":      files,
			"GlobGroups": globGroups,
			"Config": map[string]interface{}{
				"IncludeData": *flags.includeData,
			},
		}
	)
	log.Printf("Generating the file data... ")
	EmbedFileTemplate.Execute(&buf, data)
	log.Printf("Done.")

	return buf.Bytes() // The caller decides if we write it as a file or not
}

// Reads a file and outputs its bytes in Go byte array formatting.
//
// Stolen from...
// github.com/omeid/go-resources/blob/eb442c910d6342892725c94146135227da179e7d/resources.go
//
// @perf; If this is slow, we can just dump it in a barely-readable format
func reader(input io.Reader, indent int) (string, error) {
	var (
		buff      bytes.Buffer
		err       error
		curblock  = 0
		linebreak = "\n" + strings.Repeat("\t", indent)
	)

	b := make([]byte, BlockWidth)

	for n, e := input.Read(b); e == nil; n, e = input.Read(b) {
		for i := 0; i < n; i++ {
			_, e = fmt.Fprintf(&buff, "0x%02x,", b[i])
			if e != nil {
				err = e
				break
			}
			curblock++
			if curblock < BlockWidth {
				buff.WriteRune(' ')
				continue
			}
			buff.WriteString(linebreak)
			curblock = 0
		}
	}

	return buff.String(), err
}
