package main

import (
	"encoding/json"
	"log"

	"image"
	"path/filepath"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

//
//  Animation player
//

// A wrapper around the animation to allow playback.
type AnimationPlayer struct {
	entity
	Animation *Animation // Controlled animation data, can (and should) be shared

	FrameIndex      int    // Index of currently played frame
	DurationCounter SecsDT // Playback control, counter for a single frame, gets reset

	PlayedTag     Tag // Currently played tag of the animation
	PlayedTagName string

	PlaybackType PlaybackType_ // What to do when the player is at the end
}

//

type PlaybackType_ int

const (
	PLAYBACK_AT_END__LOOP PlaybackType_ = iota
	PLAYBACK_AT_END__STOP
	PLAYBACK_AT_END__NOTHING
)

//

// Get the current sprite, as defined by the current frame of the player
func (pl AnimationPlayer) Sprite() *ebi.Image {
	return pl.CurrentFrame().SubImage
}

// Get the current frame, as defined by player's timers and played tag
func (pl AnimationPlayer) CurrentFrame() Frame {
	return pl.FrameByIndex(pl.FrameIndex)
}

// Get a frame by its index in the currently played tag
func (pl AnimationPlayer) FrameByIndex(frameIndex int) Frame {
	return pl.PlayedTag.Frames[frameIndex]
}

// Detect the end and start of the animation, the rest of the data
// might not be correct.
// TODO:  Improve the API here.
func (pl AnimationPlayer) Completed_01() float64 {
	return float64(pl.FrameIndex) / float64(len(pl.PlayedTag.Frames)-1)
}

const ANIMATION_TAG_DEFAULT = "default"

// Switch to the default aseprite tag, playing the full animation.
func (pl *AnimationPlayer) SwitchToDefaultTag() *AnimationPlayer {
	pl.PlayedTag = pl.Animation.TagDefault()
	pl.PlayedTagName = ANIMATION_TAG_DEFAULT
	return pl
}
func (pl *AnimationPlayer) SwitchToTag(tagName string) *AnimationPlayer {
	tag, isValidTag := pl.Animation.TagByName(tagName)
	if !isValidTag {
		if !WEBMODE { //@noreload
			_, stillExists := pl.Animation.TagByName(pl.PlayedTagName)
			if !stillExists {
				log.Printf(
					"And the tag we had isn't there anymore, switching to default")
				pl.SwitchToDefaultTag()
			}
		}
		log.Printf("Error switching to tag %s::%s; ignoring switch...\n",
			pl.Animation.Asset.WatchedFilepath, tagName)
		return pl
	}

	pl.PlayedTag = tag
	pl.PlayedTagName = tagName

	return pl
}

// Start the animation; reset the frame index.
func (pl *AnimationPlayer) Start() *AnimationPlayer {
	pl.FrameIndex = 0
	pl.DurationCounter = 0
	return pl
}

// Advance the animation frame by one unconditionally.
func (pl *AnimationPlayer) NextFrame() *AnimationPlayer {
	pl.FrameIndex++
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations; Supports going into negative numbers.
// NOTE:  Does not support skipping multiple frames in one call.
func (pl *AnimationPlayer) Next(dt SecsDT) *AnimationPlayer {

	// TODO:  Introduce some "noreload" build tag or something :: @noreload
	if !WEBMODE {
		pl.SwitchToTag(pl.PlayedTagName)
	}

	if dt > 0 {
		pl.DurationCounter += dt

		var nextSwitch SecsDT = pl.CurrentFrame().Duration
		if pl.DurationCounter >= nextSwitch {
			// Careful about carrying the overflow into the next frame
			pl.DurationCounter -= nextSwitch
			pl.FrameIndex++
		}
	} else {
		pl.DurationCounter += dt // will be negative so it's the same

		// Next switch is trivial
		if pl.DurationCounter < 0 {
			// Leave the right overflow when going backwards
			var prevSwitch SecsDT = pl.FrameByIndex(pl.FrameIndex).Duration
			pl.DurationCounter += prevSwitch
			pl.FrameIndex--
		}
	}

	{ // What to do if we're at the end of an animation
		var (
			high = len(pl.PlayedTag.Frames) - 1
			low  = 0
		)
		switch pl.PlaybackType {
		case PLAYBACK_AT_END__NOTHING:
			break
		case PLAYBACK_AT_END__LOOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = low
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = high
			}
		case PLAYBACK_AT_END__STOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = high
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = low
			}
		}
	}

	// Note: Using `Next()` directly can overflow
	return pl
}

// If past the end, loop back to the start.
func (pl *AnimationPlayer) LoopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__LOOP
	return pl
}

// If past the end, stop at the end.
func (pl *AnimationPlayer) StopAtEnd() *AnimationPlayer {
	pl.PlaybackType = PLAYBACK_AT_END__STOP
	return pl
}

//
//  Animation data, imports from Aseprite
//

// @todo; Add a way of adding sound-trigger data into the animations

// Note: Aseprite assets have two key files, the json and the png.
// The json includes all data about frame regions and tags (what regions
// belong to which animation).
// The json also has the filepath of the png, relative to the json file.

type (
	Animation struct {
		Asset                // The animation is watchable and reloadable at runtime
		Tags  map[string]Tag // Sub-animations included in the asset
		Sheet *ebi.Image     `json:"-"` // Image data, all frames must be in this sheet
	}
	Tag struct {
		Name   string  // Name of the tag as defined by the Aseprite export
		Frames []Frame // Frame data containing the image
	}
	Frame struct {
		Duration SecsDT     // Duration for the frame
		SubImage *ebi.Image // Part of the spreadsheet corresponding to this frame
	}
)

func (an *Animation) TagByName(tagName string) (Tag, bool) {
	t, isInMap := an.Tags[tagName]
	return t, isInMap
}
func (an *Animation) TagDefault() Tag {
	return an.Tags[ANIMATION_TAG_DEFAULT]
}

func LoadAnimationAndPlayer(abspath string, forEntity entity_id) *AnimationPlayer {
	// Note: Use this only for single-occurence animations,
	//   they are obviously not reused. Don't load animations multiple
	//   times please.
	return NewAnimationPlayer(LoadAnimation(abspath), forEntity)
}
func LoadAnimation(abspath string) *Animation {
	var a Animation
	a.Asset.WatchedFilepath = abspath
	a.Reload()
	AssetWatcher.Add(&a)
	return &a
}
func NewAnimationPlayer(anim *Animation, forEntity entity_id) *AnimationPlayer {
	ap := &AnimationPlayer{
		entity:    entity{forEntity, TYPE_ANIMATION_PLAYER},
		Animation: anim,

		FrameIndex:      0,
		DurationCounter: 0,

		PlayedTag:     Tag{},
		PlayedTagName: "",
		PlaybackType:  0,
	}
	ap.SwitchToDefaultTag()

	// Registering the timer stuff is done immediatelly
	GAME.Time.AnimationPlayers[ap.ID] = ap
	// TODO:  When deleting enemies, make sure this gets deleted too

	return ap
}

func (a *Animation) Reload() {
	defer a.RecordLoaded() // Should record even failed reloads

	// When reloading, don't panic under any circumstances.
	// If the reload fails, don't just crash the whole thing
	// but just log that it happened.
	// The only crash-on-load is when the game starts.

	err := a.reload()
	if err != nil {
		// TODO:  Colorize
		log.Printf("[ERRO] animation reload: %v", err)
	}

	if LOG_ASSET_RELOADS {
		log.Printf("RELOADED: `%s`", a.WatchedFilepath)
	}
}
func (a *Animation) reload() error {
	var aseJson AseJSON
	{
		jsonBytes, err := loadBytes(a.Asset.WatchedFilepath)
		if err != nil {
			return err
		}
		err = json.Unmarshal(jsonBytes, &aseJson)
		if err != nil {
			return err
		}
	}

	var spritesheet *ebi.Image
	{
		aseJsonDir := filepath.Dir(a.Asset.WatchedFilepath)
		loadPath := filepath.Join(aseJsonDir, aseJson.Meta.Image)

		img, err := loadImage(loadPath)
		if err != nil {
			return err
		}
		spritesheet = img
	}

	tags := defineTags(aseJson, spritesheet)

	// Change the content of the animation, always use the same animation instance
	// when live-reloading.

	a.Sheet = spritesheet
	a.Tags = tags

	// NOTE:  Tags are by value. You cannot preserve the
	// TODO:  Actually make []Tag arrays for animation tags,
	//   played tag be *[]Tag (pointer to played tag array)
	//   and "db index" of string(tagname) -> array index. We can simplify
	//   @noreload

	return nil
}

// The Aseprite editor's metadata format that is saved alongside
// an exported sprite sheet. It has all the information for putting
// all of the information back together as an animation or atlas.
//
// Deciding what goes in which field is done with default values,
// we don't specify the `json:"field_name"` tags for the fields
// and it works fine.
//
// @todo; Read triggers from the animation (sounds, events...)
// @todo; Read positions from single-pixel layers somehow
// @todo; Read slices for atlas usage
type (
	AseJSON struct {
		Meta   aseMeta    // File properties, sizes and individual tags
		Frames []aseFrame // Frame bounds and durations
	}
	aseMeta struct {
		Image     string        // Filename of the spritesheet image
		Size      aseSize       // Size of the spritesheet
		FrameTags []aseFrameTag // Individual animations
	}
	aseFrameTag struct {
		Name string
		From int
		To   int
	}
	aseFrame struct {
		Duration int     // Duration in ms
		Frame    aseRect // Spritesheet's cutout region
	}
	aseRect struct {
		X, Y, W, H int
	}
	aseSize struct {
		W, H int
	}
)

// Convert aseprite export metadata into a usable animation struct
func defineTags(ase AseJSON, spritesheet *ebi.Image) map[string]Tag {

	// Extract a tag out of a picture and aseFrame data
	tagFromRange := func(name string, sheet *ebi.Image, aseFrames []aseFrame) Tag {
		var frames = make([]Frame, len(aseFrames))
		for index, it := range aseFrames {

			var r aseRect = it.Frame
			region := image.Rect(r.X, r.Y, r.X+r.W, r.Y+r.H)

			var millis = float64(it.Duration)
			var duration SecsDT = millis / 1000

			frames[index] = Frame{
				SubImage: sheet.SubImage(region).(*ebi.Image),
				Duration: duration,
			}
		}
		return Tag{Name: name, Frames: frames}
	}

	tags := make(map[string]Tag)
	{
		// Always include the full animation as a default tag
		var (
			allFrames = ase.Frames[:]
			tag       = tagFromRange(ANIMATION_TAG_DEFAULT, spritesheet, allFrames)
		)
		tags[ANIMATION_TAG_DEFAULT] = tag

		// Add the individual frame tags set in Aseprite
		for _, t := range ase.Meta.FrameTags {
			var (
				frameRange = ase.Frames[t.From : t.To+1]
				tag        = tagFromRange(t.Name, spritesheet, frameRange)
			)
			tags[t.Name] = tag
		}
	}
	return tags
}
