package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	// TODO:  This is slow for pretty writes, we should change this to something
	//   a bit more preformated or something. It's too long anyway, right?
	"encoding/json"

	"fmt"
	"image/color"
	"log"
	"strings"

	C "github.com/fatih/color"

	"time"
)

// ----------------------------------------------------------------------------------
//   Pretty logging with colors and fit-for-games timestamps

func InitLogger() {
	log.SetFlags(0) // Disabling builtin timestamp and other decor
	if WEBMODE {
		log.SetOutput(new(webNopWriter))
	} else {
		log.SetOutput(new(logWriter))
	}
	log.Printf("Pretty logging initialized.")
}

// Logging to a console in a browser is ridiculously slow for logging
// anything every frame in a game. Just don't do it.
// Panics should still go through though, if I'm not mistaken...
// TODO:  Check our panic recovery if that logs or not in the browser; @errors.go.

type webNopWriter struct{}

func (webNopWriter) Write(bytes []byte) (int, error) { return len(bytes), nil }

// We want pretty logging though, when we develop.

type logWriter struct{}

func (logWriter) Write(bytes []byte) (int, error) {
	var timestamp string
	{ // Use a timestamp prefix
		now := time.Now()
		minuteSecond := now.Format("04'05\"")
		millis := now.Format(".999")

		// Align milliseconds, pad with zeroes on the right
		if millis != "" { // Can just be empty if we have exact seconds
			millis = millis[1:] // Don't include the dot
		}
		millis = millis + strings.Repeat("0", 3-len(millis))

		const WITH_COLOR = true
		if WITH_COLOR {
			timestamp = C.YellowString("%s%s ║ ", minuteSecond, millis)
		} else {
			timestamp = fmt.Sprintf("%s%s ║ ", minuteSecond, millis)
		}
	}
	return fmt.Print(timestamp + string(bytes))
}

//
// ----------------------------------------------------------------------------------

// ==================================================================================
//    Other Utilities
// ==================================================================================

// Get a formatted JSON printout of all fields in a `thing`.
// This doesn't follow pointers so it's kinda useless if there's
// data in a thing behind a pointer, you'll just get the pointer
// value as hex -- very meh.
func AsJSON(thing interface{}) string {
	bytes, err := json.MarshalIndent(thing, "", "  ")
	canPanic(err)
	return string(bytes)
}
func (t Tile) MarshalText() ([]byte, error) { // Enable JSON output on the type
	return []byte(fmt.Sprintf("x%d:y%d", t.X, t.Y)), nil
}

func AssertIntsEqual(a, b int) {
	if a != b {
		panic("assert :: not equal")
	}
}

// Clamp returns x clamped to the interval [min, max].
func Clamp(x, min, max float64) float64 {
	switch {
	default:
		return x
	case x < min:
		return min
	case x > max:
		return max
	}
}

func BoolToSign(b bool) float64 {
	if b {
		return 1
	} else {
		return 0
	}
}
func SignInt(a int) int {
	switch {
	case a >= +1:
		return +1
	case a <= -1:
		return -1
	default:
		return 0
	}
}

func ImageSize(i *ebi.Image) (wid, hei float64) {
	b := i.Bounds()
	return float64(b.Dx()), float64(b.Dy())
}

// ==================================================================================
//   Parse colors from Hex strings
//   https://stackoverflow.com/questions/54197913/parse-hex-string-to-image-color
// ==================================================================================

func RGBFromHex(s string) (c color.RGBA) {
	c.A = 0xff // Always true

	if s[0] != '#' {
		panic("colorhex parse: needs to start with a hash")
	}

	hexToByte := func(b byte) byte {
		switch {
		case b >= '0' && b <= '9':
			return b - '0'
		case b >= 'a' && b <= 'f':
			return b - 'a' + 10
		case b >= 'A' && b <= 'F':
			return b - 'A' + 10
		}
		panic("colorhex parse: invalid character " + string(b))
	}

	switch len(s) {
	case 7:
		c.R = hexToByte(s[1])<<4 + hexToByte(s[2])
		c.G = hexToByte(s[3])<<4 + hexToByte(s[4])
		c.B = hexToByte(s[5])<<4 + hexToByte(s[6])
	case 4:
		c.R = hexToByte(s[1]) * 17
		c.G = hexToByte(s[2]) * 17
		c.B = hexToByte(s[3]) * 17
	default:
		panic("colorhex parse: invalid string length")
	}
	return
}
