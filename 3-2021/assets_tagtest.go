// +build assettest

package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"image/color"
	"log"
	"time"
)

//
//
//  Testing the assets
//
//  TODO:  Should use actual `testing` and `assets_test.go`
//
//

func init() {
	// Override normal behavior, run our update+draw instead
	// of scenes.
	_tester = newAssetTesting()
	log.Printf("\n\n\n            ASSET TESTING \n\n\n")
	LOG_FRAMERATE = false
}

type assetTesting struct {
	ap *AnimationPlayer
}

func newAssetTesting() assetTesting {
	ap := LoadAnimationAndPlayer(FILE_grefix__anim_dae_hermaeus_mora_json)
	{
		AssetWatcher.Add(ap.Animation)
	}
	size := ap.CurrentFrame().SubImage.Bounds()
	w, h := size.Dx(), size.Dy()
	RENDER_W, RENDER_H = float64(w), float64(h)
	go func() {
		for {
			<-time.After(time.Millisecond * 500)
			AssetWatcher.TryReloadingAll()
		}
	}()

	return assetTesting{
		ap: ap,
	}
}

func (t assetTesting) Update() error {
	t.ap.Next(G.Time.DT)
	t.ap.LoopAtEnd()
	return nil
}
func (t assetTesting) Draw(screen *ebi.Image) {
	screen.Fill(color.White)
	{
		screen.DrawImage(t.ap.CurrentFrame().SubImage, &ebi.DrawImageOptions{
			GeoM:          ebi.GeoM{},
			ColorM:        ebi.ColorM{},
			CompositeMode: 0,
			Filter:        0,
		})
	}
}
