/*
  The jam's page: https://itch.io/jam/nokiajam3
*/
package main

import (
	"log"
	"runtime"
	"time"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

// ===================================================================================
//   Constants and flags
// ===================================================================================

var WEBMODE = (runtime.GOOS == "js")

var ( // Printing and logging constants
	PRETTY_PRINT_PANICS = true
	LOG_SOUNDLOAD_INIT  = true
	LOG_FRAMERATE       = false // TODO:  Draw an in-game framerate graph instead
	LOG_ASSET_RELOADS   = true
	LOG_DRAW_TIME       = false
	LOG_UPDATE_TIME     = false
)

var (
	TRANSITION_DURATION = SecsDT(0.25)
)

var ( // Float is better for calculations, avoid casts
	RENDER_W   = float64(TILE_SIZE * VISIBLE_TILES_W)
	RENDER_H   = float64(TILE_SIZE * VISIBLE_TILES_H)
	INIT_SCALE = 2.0 // Scaling the whole window up a bit
)

// ===================================================================================
//   Important globals
// ===================================================================================

var (
	GAME  Game
	Sound GameSound

	_tester tester // Hook for testing code in build-tagged files `*_tagtest.go`
)

// Layout takes the outside size (e.g., the window size) and returns
// the (logical) screen size.
// If you don't have to adjust the screen size with the outside size,
// just return a fixed size.
func (g *Game) Layout(win_w, win_h int) (render_w, render_h int) {
	return int(RENDER_W), int(RENDER_H)
}

// Update proceeds the game state.
// Update is called every render tick by default.
// If an error is passed, it will get returned by `ebi.RunGame(g)`.
func (g *Game) Update() error {
	defer GrabAndReformatPanics()
	// TODO:  Ideally we'd be locking the used maps separately
	//   and not the entire game state.
	// As soon as we run a separate goroutine under a timer,
	// we'll get slammed by this.
	g.Lock()
	defer g.Unlock()

	if LOG_UPDATE_TIME {
		start := time.Now()
		defer func() { log.Printf("Update: %s", time.Now().Sub(start)) }()
	}

	dt := g.Time.UpdateDTAndTotals()
	g.Time.UpdateAndDeleteTimers(dt)
	g.Time.UpdateAnimationPlayers(dt)
	g.Time.GameTime.UpdateTransition(dt)

	if LOG_FRAMERATE {
		log.Printf("FPS: %.3f", 1.0/dt)
	}

	//

	g.Input.Update(dt)
	if g.Input.QuitNow.IsJustPressed && !WEBMODE {
		return quitkey
	}

	//

	if _tester != nil { // Testing skips scenes with the one and only scene
		return _tester.Update()
	}

	//

	{ // Scene switches
		if g.LastFrameScene != g.CurrentScene {
			defer func() { g.LastFrameScene = g.CurrentScene }()
			log.Printf("scene switched")
		}

		switch g.CurrentScene {
		case SCENE_Start:
			// g.Environment.Update(DT)

			turnBefore := g.Time.GameTime.TurnNumber
			g.Environment.Player.Update(
				dt, g.Input,
				&g.Environment, &g.Time.GameTime, &g.Camera)
			nTurnsForward := g.Time.GameTime.TurnNumber - turnBefore

			if nTurnsForward == 0 {
				g.Environment.UpdateEnemies(dt, false)
			} else if nTurnsForward > 0 {
				g.Environment.UpdateEnemies(dt, true)
			} else {
				panic("going back in time requires keeping game state history")
			}

		}
	}

	{ // Camera updates; after the code might've modified what we want to look at.
		g.Camera.Update(dt)
	}

	//
	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebi.Image) {
	defer GrabAndReformatPanics()

	g.Lock()
	defer g.Unlock()

	screen.Fill(REND_COLOR_BACKGROUND)

	if LOG_DRAW_TIME {
		start := time.Now()
		defer func() { log.Printf("Draw: %s", time.Now().Sub(start)) }()
	}

	if _tester != nil {
		_tester.Draw(screen)
		return
	}

	// TODO:  Would make more sense if it was called Level or something
	env := g.Environment
	cam := g.Camera
	env.Draw(screen, cam)
	env.DrawEnemies(screen, env.Position, g.Time.GameTime, cam)
	env.Player.Draw(screen, env.Position, g.Time.GameTime, cam)
}

func main() {
	defer GrabAndReformatPanics()

	canPanic(Init())

	// Starts the game loop
	err := ebi.RunGame(&GAME)
	if err != nil {

		// RunGame returns error when
		// 1) OpenGL error happens,
		// 2) audio error happens or
		// 3) our (*Game).Update() returns an error.

		if err == quitkey {
			log.Printf("Quit the game with a Q key")
			return
		}
		panic(err)
	}
}

func Init() error {

	InitLogger()

	{ // Window settings
		w, h := int(RENDER_W*INIT_SCALE), int(RENDER_H*INIT_SCALE)
		ebi.SetWindowSize(w, h)
		log.Printf("INITIAL WINDOW SIZE: %d x %d", w, h)

		ebi.SetWindowTitle("7DRL Challenge | Roguelike")
		ebi.SetWindowResizable(true)
	}

	Sound.Init()
	GAME.Init()

	return nil
}

func (g *Game) Init() {
	g.Lock()
	defer g.Unlock()

	g.Time.GameTime.Transition = Timer{Deadline: TRANSITION_DURATION, Secs: 0}

	g.Time.AnimationPlayers = map[entity_id]*AnimationPlayer{}
	g.Time.Timers = map[entity_id]Timer{}
	g.Time.DeletedTimers = nil // Gets overwritten every frame
	g.Phys = map[entity_id]Phys{}
	g.Particles = map[entity_id]Particle{}

	g.Environment = NewEnvironment()
	if WEBMODE {
		g.Input.SetMapWithFunc(input__normalPerson)
	} else {
		g.Input.SetMapWithFunc(input__milky)
	}
}
