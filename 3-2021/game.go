package main

import (
	"image/color"
	"log"
	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
	ebiutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

var (
	TILE_SIZE       = 24
	TILE_SIZE_f     = float64(TILE_SIZE)
	VISIBLE_TILES_W = 14
	VISIBLE_TILES_H = 11
)

type (
	Environment struct {
		Position TilePosition

		Player  ThePlayer
		Enemies map[entity_id]Enemy
		Dudes   map[entity_id]Dude

		Floors  map[entity_id]Floor
		Walls   map[entity_id]Wall
		Pickups map[entity_id]Pickup
	}
	TilePosition struct {
		OfEntity map[entity_id]Tile
		OnTile   map[Tile]entity_set
	}

	Wall   struct{ entity }
	Floor  struct{ entity }
	Pickup struct{ entity }

	ThePlayer struct{ Dude }

	Enemy struct {
		entity
	}

	Dude struct {
		entity
		PrevTile       Tile // For tweening positions TODO:  Maybe xy floats here
		Health_01      float64
		AP             *AnimationPlayer
		IdleFor        SecsDT
		GotHitThisTurn bool
	}
)

func NewPosition() TilePosition {
	return TilePosition{
		OfEntity: make(map[entity_id]Tile),
		OnTile:   make(map[Tile]entity_set), // reverse index, 1:N
	}
}
func NewEnvironment() Environment {
	env := Environment{
		Position: NewPosition(),
		Player:   ThePlayer{Dude{AP: nil}},
		Enemies:  map[entity_id]Enemy{},
		Dudes:    map[entity_id]Dude{},
		Floors:   map[entity_id]Floor{},
		Walls:    map[entity_id]Wall{},
		Pickups:  map[entity_id]Pickup{},
	}

	env.LoadFromString(`
@@@@@@@@@@@@@@@
@_________!___@@@@@@@@@@@@@@
@___*______________________@   @@@@@@
@_______!_____@@@@@@@@@____@  @@@@@@@@
@@@@@_@@@@@@@@@       @____@@@@@____@@
    @_@               @@@@________!_@@
    @@@                  @@@@@@@____@@
                              @@@@@@@@
                               @@@@@@`)

	return env
}

var PLAYER_ID entity_id

func NewPlayer() ThePlayer {

	if PLAYER_ID != 0 {
		panic("tried to make 2 players here")
	}
	PLAYER_ID = NewEntityID()

	ap := LoadAnimationAndPlayer(FILE_grefix__assets__e_p_attack_json, PLAYER_ID)
	ap.SwitchToTag("idle").LoopAtEnd().Start()

	return ThePlayer{Dude{
		entity:    entity{PLAYER_ID, TYPE_PLAYER},
		PrevTile:  Tile{0, 0},
		Health_01: 1.0,
		AP:        ap,
		IdleFor:   0,
	}}
}

var (
	ENEMY_YOLK *Animation = nil
	ENEMY_EGG  *Animation = nil
)

func NewEnemy() Dude {
	if ENEMY_YOLK == nil {
		ENEMY_YOLK = LoadAnimation(FILE_grefix__assets__e_e_egg_yolky_json)
	}

	id := NewEntityID()
	return Dude{
		entity:    entity{id, TYPE_ENEMY},
		PrevTile:  Tile{},
		Health_01: 1.0,
		AP:        NewAnimationPlayer(ENEMY_YOLK, id),
		IdleFor:   0,
	}
}

// ...keeping lookups everywhere is weird... I shouldn't probably
//   do it this way, i mean look at this...
func (pos TilePosition) Set(id entity_id, tileTo Tile) {
	tileBefore, ok := pos.OfEntity[id]
	if ok {
		delete(pos.OnTile[tileBefore], id)
	}

	pos.OfEntity[id] = tileTo //

	if pos.OnTile[tileTo] == nil {
		pos.OnTile[tileTo] = make(entity_set)
	}

	pos.OnTile[tileTo][id] = struct{}{} //
}
func (pos TilePosition) Delete(id entity_id) {
	t, ok := pos.OfEntity[id]
	if ok {
		delete(pos.OnTile[t], id)
	}
	delete(pos.OfEntity, id)
}

func (env *Environment) UpdateEnemies(dt SecsDT, nextTurn bool) {

	playerPos := env.Position.OfEntity[env.Player.ID]

	for id, cp := range env.Dudes {
		cp.Update(dt) // Timers, always

		cpPos := env.Position.OfEntity[id]

		dx, dy := playerPos.X-cpPos.X, playerPos.Y-cpPos.Y
		sx, sy := SignInt(dx), SignInt(dy)

		if nextTurn {

			// @test, go toward player in a dumb way
			var (
				moved     bool
				blockedBy entity
			)
			if sx != 0 && sy != 0 {
				moved, blockedBy = cp.MoveBy(sx, sy, env)
				if !moved && blockedBy.Type != TYPE_PLAYER {
					moved, blockedBy = cp.MoveBy(sx, NOP, env)
					if !moved && blockedBy.Type != TYPE_PLAYER {
						moved, blockedBy = cp.MoveBy(NOP, sy, env)
					}
				}
			} else {
				moved, blockedBy = cp.MoveBy(sx, sy, env)
			}

			switch blockedBy.Type {
			case TYPE_ENEMY:
				log.Printf("bumped into an another enemy!")
			case TYPE_PLAYER:
				// Hit the player
				log.Printf("HIT")
				env.Player.GotHitThisTurn = true
			case TYPE_WALL:
				// Play a hit sound
				log.Printf("bumped into a wall")
			}

			cp.AP.Start() // @test, probably have some walk
		}

		env.Dudes[id] = cp
	}
}

// func (env *Environment) Update(dt SecsDT) {}
// ... if the environment has some kind of animated background or something...
// ... maybe lighting goes here?

// Directions of the tiles, I never remember what is where
const (
	UP    = -1
	DOWN  = +1
	LEFT  = -1
	RIGHT = +1
	NOP   = 0
)

func (dude *Dude) Update(dt SecsDT) {
	dude.IdleFor += dt

	// TODO:  Dude updates
}
func (p *ThePlayer) Update(
	dt SecsDT, inp Input,
	env *Environment, gt *GameTime, cam *Camera) {

	anim := p.AP
	const ( // These have to be hardcoded, so let's at least autocomplete them
		IDLE               = "idle"
		IDLE_FLIP          = "kniflip"
		IDLE_HEADTURN      = "headturn"
		WALK               = "walk"
		ATTACK             = "attack"
		TRIGGER_IDLE_AFTER = SecsDT(1.2)
	)

	{ // Update timers
		p.IdleFor += dt
	}

	{ // Moving around
		moveIssued := true
		dx, dy := NOP, NOP
		switch m := inp.Player.Move; {
		default:
			moveIssued = false
		case m.Up.IsJustPressed:
			dy = UP
		case m.Down.IsJustPressed:
			dy = DOWN
		case m.Left.IsJustPressed:
			dx = LEFT
		case m.Right.IsJustPressed:
			dx = RIGHT
		}

		if moveIssued {
			moveOk, blockedBy := p.MoveBy(dx, dy, env)
			switch {
			case moveOk:
				GAME.StepTurn()
				cam.FocusOnceOn(p.ID, env.Position)
			case blockedBy.Type == TYPE_ENEMY:
				GAME.StepTurn()

				cp := env.Dudes[blockedBy.ID]
				{
					// TODO:  Block the enemy's movement
					cp.GotHitThisTurn = true
				}
				env.Dudes[blockedBy.ID] = cp

			case blockedBy.Type == TYPE_WALL:
				log.Printf("bumped") // TODO:  Bump into a wall like a scrub
			}

			anim.SwitchToTag(WALK).StopAtEnd().Start()
			anim.NextFrame().NextFrame() // hack it, it's too slow
			p.IdleFor = -999

			return // Accept no other input
		}
	}

	// Walk, then idle
	if anim.PlayedTagName == WALK && anim.Completed_01() == 1 {
		p.IdleFor = 0
		anim.SwitchToTag(IDLE).LoopAtEnd().Start()
	}

	{ // Attacking, if not moving (but that's not really it, I just wanna play the anim)
		if inp.Player.Attack.IsJustPressed {
			anim.SwitchToTag(ATTACK).StopAtEnd().Start()
			p.IdleFor = -999 // Invalidate idle throughout attack
			return           // Accept no other input
		}
		if anim.PlayedTagName == ATTACK && anim.Completed_01() == 1 {
			anim.SwitchToTag(IDLE).LoopAtEnd().Start()
			p.IdleFor = 0
		}
	}

	var IDLE_VARIANTS = []string{IDLE_FLIP, IDLE_HEADTURN}
	var playingIdleVariant = false
	for _, v := range IDLE_VARIANTS {
		if anim.PlayedTagName == v {
			playingIdleVariant = true
		}
	}

	if p.IdleFor > TRIGGER_IDLE_AFTER {
		randIdle := IDLE_VARIANTS[rand.Intn(len(IDLE_VARIANTS))]
		anim.SwitchToTag(randIdle).StopAtEnd().Start()
		p.IdleFor = -999
	}
	if playingIdleVariant && anim.Completed_01() == 1 {
		anim.SwitchToTag(IDLE).LoopAtEnd().Start()
		p.IdleFor = 0
	}
}

// TODO:  Maybe we'd like to pass around tiles and not xys,
//   we'll see about that. Difference in Tiles would be strange
//   but that might be fine.
func (p *Dude) MoveBy(dx, dy int, env *Environment) (bool, entity) {
	p.PrevTile = env.Position.OfEntity[p.ID]
	x, y := p.PrevTile.xy_ints()
	return p.MoveTo(x+dx, y+dy, env)
}
func (p *Dude) MoveTo(x, y int, env *Environment) (bool, entity) {
	p.PrevTile = env.Position.OfEntity[p.ID] // TODO:  Done twice for MoveBy, oof

	moveto := Tile{x, y}
	IS_PLAYER := p.ID == PLAYER_ID

	occupiedBy := env.Position.OnTile[moveto]

	floorEnt := entity{}
	for id := range occupiedBy {
		if floor, ok := env.Floors[id]; ok {
			floorEnt = floor.entity
			continue
		}

		if wall, ok := env.Walls[id]; ok {
			return false, wall.entity
		}

		if !IS_PLAYER && id == PLAYER_ID {
			return false, env.Player.entity
		}

		// Enemies block both players and enemies
		if enemy, ok := env.Enemies[id]; ok {
			return false, enemy.entity
		}
	}

	if floorEnt.ID == 0 {
		panic("cannot walk on thin air, a wall is probably missing")
		//return false entity{}
	}

	// Finally, if nothing has blocked our movement, we can just walk
	env.Position.Set(p.ID, moveto)
	// Giving out the floor, as a nice-to-have default
	return true, floorEnt
}

////

var (
	REND_COLOR_BACKGROUND = RGBFromHex("#4a1c39")

	// debug tiles
	REND_TILE_FLOOR_DEBUG = tileOverlayImage(
		color.RGBA{0x50, 0x50, 0x50, 0xff},
		color.RGBA{0x30, 0x30, 0x30, 0xff},
	)
	REND_TILE_WALL_DEBUG = tileOverlayImage(
		color.RGBA{0xF0, 0, 0, 0xff},
		color.RGBA{0xA0, 0, 0, 0xff},
	)

	REND_TILE_FLOOR = MustLoadImage(FILE_grefix__assets__t_b_5_png)
	REND_TILE_WALL  = MustLoadImage(FILE_grefix__assets__t_f_5_png)

	// @skip
	REND_DUDESHADOW = ebi.NewImage(0, 0) // MustLoadImage(FILE_grefix__FxShadow_png)
)

func tileOverlayImage(outline, inside color.RGBA) *ebi.Image {
	img := ebi.NewImage(TILE_SIZE, TILE_SIZE)
	img.Fill(outline)
	ebiutil.DrawRect(img,
		1, 1, float64(TILE_SIZE-1), float64(TILE_SIZE-1),
		inside)
	return img
}

// Draw the walls onto the screen
func (env *Environment) Draw(screen *ebi.Image, cam Camera) {

	// TODO:  Filter out off-screen elements when we have a camera for a perform. boost
	camGeom := cam.GeoM()

	for _, o := range env.Floors {
		geom := geomTranslateToTileLeftTop(camGeom, env.Position.OfEntity[o.ID])
		screen.DrawImage(REND_TILE_FLOOR, &ebi.DrawImageOptions{GeoM: geom})
	}
	for _, o := range env.Walls {
		geom := geomTranslateToTileLeftTop(camGeom, env.Position.OfEntity[o.ID])
		screen.DrawImage(REND_TILE_WALL, &ebi.DrawImageOptions{GeoM: geom})
	}
}
func (env *Environment) DrawEnemies(
	screen *ebi.Image,
	pos TilePosition, gt GameTime, cam Camera) {
	for _, o := range env.Dudes {
		o.Draw(screen, pos, gt, cam)
	}
}

// Draw the player, based on its position and position transition timer
// TODO:  Batch these draws, we'll need to filter the dudes' types
//   and maybe do some of these drawing calculations in stages.
func (dude *Dude) Draw(screen *ebi.Image, pos TilePosition, gt GameTime, cam Camera) {

	DRAWING_THE_PLAYER := dude.ID == PLAYER_ID

	sprite := dude.AP.CurrentFrame().SubImage
	tileNow := pos.OfEntity[dude.ID]
	camGeom := cam.GeoM()

	geom := geomTranslateToTileLeftTop(camGeom, tileNow)

	remaining := gt.Transition.Remaining_01()
	f := remaining * remaining * remaining // Good enough wwww

	{ // Interpolate between the last tile position and the current one
		ax, ay := dude.PrevTile.xy_floats()
		bx, by := tileNow.xy_floats()
		geom.Translate(
			(ax-bx)*f*float64(TILE_SIZE),
			(ay-by)*f*float64(TILE_SIZE),
		)
	}

	// @skip
	if false { // Draw a shadow
		// NOTE:  Hopefully this lines up with the character offset...
		screen.DrawImage(REND_DUDESHADOW, &ebi.DrawImageOptions{GeoM: geom})
	}

	if DRAWING_THE_PLAYER {
		// A bit of a spring into the steps when the player moves
		// https://www.desmos.com/calculator/lxgvhz8jls ...an inverted U
		f = -((2*f - 1) * (2*f - 1)) + 1
		geom.Translate(0, f*float64(TILE_SIZE)*0.25*UP)
	}

	{ // Offset a bit up into the tile
		geom.Translate(0, float64(TILE_SIZE)*0.20*UP)
	}

	if !DRAWING_THE_PLAYER {
		// Draw an outline around every enemy sprite
		// TODO:  This allows us to animate the colors! Neat!

		var colorm ebi.ColorM
		colorm.Scale(0, 0, 0, 1) // Kills all color, black outline

		for _, xy := range REND_OUTLINE_XY {
			var geomOffset ebi.GeoM = geom
			geomOffset.Translate(xy[0], xy[1])
			screen.DrawImage(sprite, &ebi.DrawImageOptions{
				GeoM: geomOffset, ColorM: colorm,
			})
		}
	}

	{ // Draw the dude sprite
		var colorm ebi.ColorM
		if dude.GotHitThisTurn && f > 0 {
			f := f*3 + 1
			colorm.Scale(f, 1/f, 1/f, 1)
		}
		screen.DrawImage(sprite, &ebi.DrawImageOptions{
			GeoM:   geom,
			ColorM: colorm,
		})
	}
}

var REND_OUTLINE_XY = [4][2]float64{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}

func geomTranslateToTileLeftTop(geom ebi.GeoM, t Tile) ebi.GeoM {
	x, y := t.xy_ints()
	geom.Translate(float64(x*TILE_SIZE), float64(y*TILE_SIZE))
	return geom
}

/////

func (env *Environment) LoadFromString(s string) {
	byt := []byte(s)
	ch := TYPE_CHARS

	x, y := 0, 0
	for _, char := range byt {

		id := NewEntityID()
		tile := xyTile(x, y)
		addwall := func() {
			env.Walls[id] = Wall{entity: entity{id, TYPE_WALL}}
			env.Position.Set(id, tile)
		}
		addfloor := func() {
			env.Floors[id] = Floor{entity: entity{id, TYPE_FLOOR}}
			env.Position.Set(id, tile)
		}

		switch char {
		default:
			// Do nothing, no entity added (might be newline)
		case ch[TYPE_WALL]:
			addwall()

		case ch[TYPE_FLOOR]:
			addfloor()

		case ch[TYPE_PLAYER]:
			addfloor()

			p := NewPlayer()
			{
				p.MoveTo(x, y, env)
			}
			env.Player = p

		case ch[TYPE_ENEMY]:
			addfloor()

			en := NewEnemy()
			{
				en.MoveTo(x, y, env)
				en.PrevTile = env.Position.OfEntity[en.ID]
			}
			env.Enemies[en.ID] = Enemy{en.entity}
			env.Dudes[en.ID] = en
		}

		x++
		if char == '\n' {
			y++
			x = 0
		}
	}
}
