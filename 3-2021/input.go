package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"log"
	"strings"
)

// NOTE:  If we use the `inpututil`, it's gonna track **ALL** input
//   there is and can be. Because we want to track specific stuff,
//   let's not use that unless we have to (gamepads maybe).

type Input struct {
	Keymap map[ebi.Key]*inputTracked

	Player struct {
		Move struct {
			Up, Down, Left, Right inputTracked
		}
		Attack inputTracked
	}
	QuitNow inputTracked
}

//

func (i *Input) SetMapWithFunc(fn func(*Input) map[ebi.Key]*inputTracked) {
	i.Keymap = fn(i)
}
func (i Input) ListCurrentBindings() {
	log.Printf("CURRENT BINDINGS: \n%s", i.currentBindingsFormatted())
}
func (i Input) currentBindingsFormatted() string {
	var sb strings.Builder
	// TODO:  Have a way (probably `reflect`) to find the variable names
	//   of these bindings without me actually naming them.
	//   Or we can do struct tags, but I'd rather not do that.
	for key := range i.Keymap {
		sb.WriteString(key.String())
		sb.WriteRune('\n')
	}
	return sb.String()
}

func input__milky(i *Input) map[ebi.Key]*inputTracked {
	return map[ebi.Key]*inputTracked{
		ebi.KeyQ: &i.QuitNow,

		ebi.KeyC:     &i.Player.Move.Up,
		ebi.KeyT:     &i.Player.Move.Down,
		ebi.KeyH:     &i.Player.Move.Left,
		ebi.KeyN:     &i.Player.Move.Right,
		ebi.KeySpace: &i.Player.Attack,

		ebi.KeyW:  &i.Player.Move.Up,
		ebi.KeyUp: &i.Player.Move.Up,

		ebi.KeyS:    &i.Player.Move.Down,
		ebi.KeyDown: &i.Player.Move.Down,

		ebi.KeyA:    &i.Player.Move.Left,
		ebi.KeyLeft: &i.Player.Move.Left,

		ebi.KeyD:     &i.Player.Move.Right,
		ebi.KeyRight: &i.Player.Move.Right,
	}
}
func input__normalPerson(i *Input) map[ebi.Key]*inputTracked {
	return map[ebi.Key]*inputTracked{
		ebi.KeyQ: &i.QuitNow,

		ebi.KeyW:  &i.Player.Move.Up,
		ebi.KeyUp: &i.Player.Move.Up,

		ebi.KeyS:    &i.Player.Move.Down,
		ebi.KeyDown: &i.Player.Move.Down,

		ebi.KeyA:    &i.Player.Move.Left,
		ebi.KeyLeft: &i.Player.Move.Left,

		ebi.KeyD:     &i.Player.Move.Right,
		ebi.KeyRight: &i.Player.Move.Right,

		ebi.KeySpace: &i.Player.Attack,
	}
}

//

type inputTracked struct {
	IsPressed      bool
	IsJustPressed  bool
	IsJustReleased bool
	IsPressedTimer SecsDT
}

func (i *Input) Update(dt SecsDT) {
	if len(i.Keymap) == 0 {
		panic("input probably not initialized")
	}

	avoidDupes := map[*inputTracked]struct{}{}
	for key, tracker := range i.Keymap {
		if _, handledAlready := avoidDupes[tracker]; handledAlready {
			continue
		}

		tracker.IsPressed = ebi.IsKeyPressed(key)

		if tracker.IsPressed {
			avoidDupes[tracker] = struct{}{}
			tracker.IsPressedTimer += dt
		} else {
			tracker.IsPressedTimer = 0
		}

		tracker.IsJustPressed = inpututil.IsKeyJustPressed(key)
		tracker.IsJustReleased = inpututil.IsKeyJustReleased(key)
	}
}
