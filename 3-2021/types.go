package main

import (
	"math"
	"sync"

	"image/color"
	"math/rand"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

type Game struct {
	sync.RWMutex // Lock for easy goroutine access

	// The scene / mode the game currently plays
	CurrentScene, LastFrameScene enum_scene

	Input Input
	Time  Time

	Environment Environment
	Camera      Camera

	// Entity sets  TODO:  What are these doing here anyway
	Phys      map[entity_id]Phys
	Particles map[entity_id]Particle
}

type ( // Entities
	Phys struct {
		entity
		X, Y, Rot       float64
		Dx, Dy, Drot    float64
		Scale_1, Dscale float64 // (Scale - 1) for a harmless 0-default
	}
	Particle struct { // We probably won't have the time to do those
		entity
		Color                       color.Color
		TimeToLive, TimeToLiveStart SecsDT
	}
)

// ===================================================================================
//   Entity IDs, DOD
// ===================================================================================

type (
	entity_id        uint32
	enum_entity_type uint

	entity struct {
		ID   entity_id
		Type enum_entity_type
	}

	entity_set   map[entity_id]struct{}
	entity_slice []entity_id
)

const (
	TYPE_PLAYER enum_entity_type = iota + 1
	TYPE_ENEMY
	TYPE_PICKUP
	TYPE_FLOOR
	TYPE_WALL
	TYPE_DOOR

	TYPE_TIMER enum_entity_type = iota + 100 // Not visible
	TYPE_ANIMATION_PLAYER
)

var (
	TYPE_CHARS = map[enum_entity_type]byte{}
	TYPE_ENUMS = map[byte]enum_entity_type{}
)

func init() {
	TYPE_CHARS = map[enum_entity_type]byte{
		TYPE_PLAYER: '*',
		TYPE_ENEMY:  '!',
		TYPE_PICKUP: 'n',
		TYPE_FLOOR:  '_',
		TYPE_WALL:   '@',
		TYPE_DOOR:   'H',
	}
	for en, ch := range TYPE_CHARS {
		TYPE_ENUMS[ch] = en
	}
}
func (t enum_entity_type) String() string {
	return string(TYPE_CHARS[t]) // defaults to "" empty string
}

// Set of entity IDs that are already taken, used for lookup
// for uniqueness whenever we need a new ID.
var takenIDs map[entity_id]struct{}

func NewEntityID() entity_id {
	for {
		id := entity_id(rand.Uint32())
		if _, taken := takenIDs[id]; taken {
			continue
		}
		return id
	}
}

// ================================================================================
//   Tiles and its utilities
// ================================================================================

type Tile struct{ X, Y int }

func (t Tile) xy_floats() (float64, float64) { return float64(t.X), float64(t.Y) }
func (t Tile) xy_ints() (int, int)           { return t.X, t.Y }
func xyTile(x, y int) Tile                   { return Tile{X: x, Y: y} }
func xyTileFloat(x, y float64) Tile {
	return Tile{
		X: int(math.Floor(x)),
		Y: int(math.Floor(y)),
	}
}

// ================================================================================
//   ENUMS
// ================================================================================

type enum_scene int

const (
	SCENE_Start enum_scene = iota
)

// ================================================================================
//   TESTING HOOKS
// ================================================================================

type tester interface {
	Update() error
	Draw(*ebi.Image)
}
