package main

import (
	"testing"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"image/color"
)

const ( // Copied
	WALL    = '@'
	GROUND  = '_'
	NEWLINE = '\n'
)

func TestMakingLevel(t *testing.T) {
	defer GrabAndReformatPanics()

	templ := `
@@
_`
	t.Logf("Input: %s", templ)

	env := NewEnvironment()
	env.LoadFromString(templ)

	t.Logf("%s", AsJSON(env))
}

func TestDeleteNilMap(t *testing.T) {
	var m map[bool]struct{}
	delete(m, true)
}

//
//  TODO:  Benching drawing like this doesn't work, should probably
//    just run it once in the _tester instead.
//

type testGame struct {
	t         *testing.T
	s         int // size
	offscreen bool
	image     ebi.Image
	done      chan struct{}
}

func newTestGame(offscreen bool, t *testing.T) testGame {
	size := 30
	return testGame{
		t:         t,
		s:         size,
		offscreen: offscreen,
		image: *func() *ebi.Image {
			img := ebi.NewImage(size, size)
			img.Fill(color.White)
			return img
		}(),
		done: make(chan struct{}),
	}
}
func (tg testGame) Draw(s *ebi.Image) {
	geom := ebi.GeoM{}
	if tg.offscreen {
		geom.Translate(999, 999)
	}
	s.DrawImage(&tg.image, &ebi.DrawImageOptions{GeoM: geom})
	panic(nil) // Just a single draw here
}
func (tg testGame) Update() error              { return nil }
func (tg testGame) Layout(int, int) (int, int) { return tg.s, tg.s }

func TestDrawingOffscreen(t *testing.T) {
	tg := newTestGame(true, t)
	defer func() {
		r := recover()
		if r != nil {
			panic(r)
		}
		return
	}()
	ebi.RunGame(&tg)
}
func TestDrawingOnscreen(t *testing.T) {
	tg := newTestGame(false, t)
	defer func() {
		r := recover()
		if r != nil {
			panic(r)
		}
		return
	}()
	ebi.RunGame(&tg)
}
