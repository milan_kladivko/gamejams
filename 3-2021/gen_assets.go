
package main

// GENERATED ASSETS GO HERE

// Filenames listed as constants for autocompleting
const (
    FILE_build_web__glasspain_logo2_png = "build_web/glasspain_logo2.png"
    FILE_grefix___old__c_pl_idle_json = "grefix/.old/c-pl-idle.json"
    FILE_grefix___old__c_pl_idle_png = "grefix/.old/c-pl-idle.png"
    FILE_grefix___old__t_b_1_png = "grefix/.old/t-b-1.png"
    FILE_grefix___old__t_b_2_png = "grefix/.old/t-b-2.png"
    FILE_grefix___old__t_f_1_png = "grefix/.old/t-f-1.png"
    FILE_grefix__FxShadow_png = "grefix/FxShadow.png"
    FILE_grefix__assets__EnEnEgg_json = "grefix/assets/EnEnEgg.json"
    FILE_grefix__assets__EnEnEgg_png = "grefix/assets/EnEnEgg.png"
    FILE_grefix__assets__EnEnEggYolk_json = "grefix/assets/EnEnEggYolk.json"
    FILE_grefix__assets__EnEnEggYolk_png = "grefix/assets/EnEnEggYolk.png"
    FILE_grefix__assets__EnEnSpaghetti_json = "grefix/assets/EnEnSpaghetti.json"
    FILE_grefix__assets__EnEnSpaghetti_png = "grefix/assets/EnEnSpaghetti.png"
    FILE_grefix__assets__EnGlitches_json = "grefix/assets/EnGlitches.json"
    FILE_grefix__assets__EnGlitches_png = "grefix/assets/EnGlitches.png"
    FILE_grefix__assets__EvDoor_json = "grefix/assets/EvDoor.json"
    FILE_grefix__assets__EvDoor_png = "grefix/assets/EvDoor.png"
    FILE_grefix__assets__EvStaircase_json = "grefix/assets/EvStaircase.json"
    FILE_grefix__assets__EvStaircase_png = "grefix/assets/EvStaircase.png"
    FILE_grefix__assets__FxAtkChefslice_json = "grefix/assets/FxAtkChefslice.json"
    FILE_grefix__assets__FxAtkChefslice_png = "grefix/assets/FxAtkChefslice.png"
    FILE_grefix__assets__UiEnemyHealth_json = "grefix/assets/UiEnemyHealth.json"
    FILE_grefix__assets__UiEnemyHealth_png = "grefix/assets/UiEnemyHealth.png"
    FILE_grefix__assets__e_e_egg_yolky_json = "grefix/assets/e-e-egg-yolky.json"
    FILE_grefix__assets__e_e_egg_yolky_png = "grefix/assets/e-e-egg-yolky.png"
    FILE_grefix__assets__e_e_egg_json = "grefix/assets/e-e-egg.json"
    FILE_grefix__assets__e_e_egg_png = "grefix/assets/e-e-egg.png"
    FILE_grefix__assets__e_e_spaghetti_json = "grefix/assets/e-e-spaghetti.json"
    FILE_grefix__assets__e_e_spaghetti_png = "grefix/assets/e-e-spaghetti.png"
    FILE_grefix__assets__e_p_all_json = "grefix/assets/e-p-all.json"
    FILE_grefix__assets__e_p_all_png = "grefix/assets/e-p-all.png"
    FILE_grefix__assets__e_p_attack_json = "grefix/assets/e-p-attack.json"
    FILE_grefix__assets__e_p_attack_png = "grefix/assets/e-p-attack.png"
    FILE_grefix__assets__t_b_5_png = "grefix/assets/t-b-5.png"
    FILE_grefix__assets__t_f_5_png = "grefix/assets/t-f-5.png"
)

// Groups by globs, indexed by whatever inclusion globs have been
// used as the argument when loading.
var _assetGlobGroups = [][]string{ 
    { // from '**.json'
        FILE_grefix___old__c_pl_idle_json,
        FILE_grefix__assets__EnEnEgg_json,
        FILE_grefix__assets__EnEnEggYolk_json,
        FILE_grefix__assets__EnEnSpaghetti_json,
        FILE_grefix__assets__EnGlitches_json,
        FILE_grefix__assets__EvDoor_json,
        FILE_grefix__assets__EvStaircase_json,
        FILE_grefix__assets__FxAtkChefslice_json,
        FILE_grefix__assets__UiEnemyHealth_json,
        FILE_grefix__assets__e_e_egg_yolky_json,
        FILE_grefix__assets__e_e_egg_json,
        FILE_grefix__assets__e_e_spaghetti_json,
        FILE_grefix__assets__e_p_all_json,
        FILE_grefix__assets__e_p_attack_json,  
    }, 
    { // from '**.png'
        FILE_build_web__glasspain_logo2_png,
        FILE_grefix___old__c_pl_idle_png,
        FILE_grefix___old__t_b_1_png,
        FILE_grefix___old__t_b_2_png,
        FILE_grefix___old__t_f_1_png,
        FILE_grefix__FxShadow_png,
        FILE_grefix__assets__EnEnEgg_png,
        FILE_grefix__assets__EnEnEggYolk_png,
        FILE_grefix__assets__EnEnSpaghetti_png,
        FILE_grefix__assets__EnGlitches_png,
        FILE_grefix__assets__EvDoor_png,
        FILE_grefix__assets__EvStaircase_png,
        FILE_grefix__assets__FxAtkChefslice_png,
        FILE_grefix__assets__UiEnemyHealth_png,
        FILE_grefix__assets__e_e_egg_yolky_png,
        FILE_grefix__assets__e_e_egg_png,
        FILE_grefix__assets__e_e_spaghetti_png,
        FILE_grefix__assets__e_p_all_png,
        FILE_grefix__assets__e_p_attack_png,
        FILE_grefix__assets__t_b_5_png,
        FILE_grefix__assets__t_f_5_png,  
    }, 
    { // from '**.wav'  
    }, 
    { // from '**.mp3'  
    },
}

// Raw binary data of the assets.
// OMMITED. Apply the "-include-data" argument.
var _assetData = map[string][]byte{}

