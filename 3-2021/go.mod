module jam

go 1.16

require (
	github.com/fatih/color v1.10.0
	github.com/hajimehoshi/ebiten/v2 v2.0.6
)
