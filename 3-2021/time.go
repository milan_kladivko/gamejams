package main

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	"log"
	"time"
)

// In the entire project, we'll be using the same DT unit.
// Working with math is weird enough, no point in throwing in nanos
// that `time.Duration` uses. Not good for games.
// This is syntax for being able to still use `float64` for times
// interchangably, but mentioning `SecsDT` where appropriate is preferrable.
type SecsDT = float64

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function.
func DurationToDt(d time.Duration) SecsDT {
	// Note: Yes, this doesn't truncate the seconds. It returns the fractional
	// conversion of nanoseconds to seconds.
	return d.Seconds()
}

//

type Time struct {
	// Timestamp of current frame's `Update()` call
	FrameStart time.Time

	DT          SecsDT // Difference in times between current and last frame's `Update()`
	TotalTime   SecsDT // Total number of elapsed time since the game's start
	TotalFrames int64  // Total number of drawn frames since the game's start

	GameTime GameTime

	Timers           map[entity_id]Timer
	DeletedTimers    map[entity_id]struct{} // Like a queue for deleted timers
	AnimationPlayers map[entity_id]*AnimationPlayer
}

//

type GameTime struct {
	TurnNumber int   // Use for gameplay
	Transition Timer // Use for pretty draws
}

func (g *Game) StepTurn() {
	g.Time.GameTime.StepTurn()

	g.Environment.Player.Dude.GotHitThisTurn = false
	for id, cp := range g.Environment.Dudes {
		cp.GotHitThisTurn = false
		g.Environment.Dudes[id] = cp
	}
}
func (gt *GameTime) StepTurn() {
	defer log.Printf("STEP %d", gt.TurnNumber)
	gt.TurnNumber++
	gt.Transition.Secs = 0
}
func (gt *GameTime) UpdateTransition(dt SecsDT) {
	tr := &gt.Transition
	if !tr.HasDeadline() {
		panic("need duration on the transition!")
	}
	tr.Secs += dt
	tr.Updates++
}

func (t *Time) UpdateAnimationPlayers(dt SecsDT) {
	for _, playerPtr := range t.AnimationPlayers {
		playerPtr.Next(t.DT)
	}
}
func (t *Time) UpdateDTAndTotals() SecsDT {
	now := time.Now()
	dt := now.Sub(t.FrameStart)
	t.FrameStart = now

	// Clamp to some small-ish value when severely lagging.
	dt_max := time.Millisecond * 200
	if dt > dt_max {
		dt = dt_max
	}

	if false {
		log.Printf("dt:  %.2fms", dt.Seconds()*1000)
		log.Printf("FPS: %.2f", ebi.CurrentFPS()) // Gets counted in the lib itself
	}

	t.DT = dt.Seconds() // Save into the global for use everywhere, don't pass it
	t.TotalTime += t.DT
	t.TotalFrames += 1 // Assuming one update per frame here

	return t.DT // For utility
}

//

type Timer struct {
	entity
	Secs     SecsDT // Elapsed seconds
	Deadline SecsDT
	Updates  uint64
}

func (tr Timer) Elapsed() SecsDT { return tr.Secs }

func (tr Timer) HasDeadline() bool { return tr.Deadline > 0 }
func (tr Timer) Remaining() SecsDT {
	if !tr.HasDeadline() {
		return -1
	}
	return tr.Deadline - tr.Secs
}
func (tr *Timer) Reverse() {
	tr.Secs = tr.Remaining()
	// .ElapsedTTL stays the same
	// .Updates are not touched
}
func (tr Timer) Completed_01() float64 {
	if !tr.HasDeadline() {
		return 0 // Never elapses its deadline
	}
	return Clamp(tr.Secs/tr.Deadline, 0, 1)
}
func (tr Timer) Remaining_01() float64 {
	if !tr.HasDeadline() {
		return 1 // Always has the whole life ahead of it
	}
	return Clamp(1-(tr.Secs/tr.Deadline), 0, 1)
}

type TimerOps struct {
	// Reversing could be a thing we might need
	KillAfter SecsDT
}

func (t *Time) NewTimer(ops TimerOps) entity_id {
	id := NewEntityID()
	t.Timers[id] = Timer{
		entity:   entity{id, TYPE_TIMER},
		Deadline: ops.KillAfter,
	}
	return id
}
func (t *Time) UpdateAndDeleteTimers(dt SecsDT) {

	toDelete := map[entity_id]struct{}{}

	for id := range t.Timers {
		tim := t.Timers[id]
		if tim.Secs >= tim.Deadline && tim.Deadline > 0 {
			toDelete[id] = struct{}{}
			continue
		}
		tim.Secs += dt
		tim.Updates++
		t.Timers[id] = tim
	}

	for id := range toDelete {
		delete(t.Timers, id)
	}

	// Overwrite the old list, might be an empty map
	t.DeletedTimers = toDelete
}
func (t *Time) HasTimerJustEnded(id entity_id) bool {
	// NOTE:  Retrieving from a nil-map would still work (returns false)
	_, ok := t.DeletedTimers[id]
	return ok
}
