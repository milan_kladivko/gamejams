package main

import (
	"fmt"
	ebi "github.com/hajimehoshi/ebiten/v2"
	"math"
)

type Camera struct {
	X, Y             float64 // Used offsets
	TargetX, TargetY float64 // Where we want to be

	Follow struct {
		TargetID    entity_id
		PositionPtr *TilePosition
	}
}

func (c *Camera) FocusOnceOn(id entity_id, pos TilePosition) {
	t, ok := pos.OfEntity[id]
	if !ok {
		panic(fmt.Errorf("%d: this entity doesn't have a tile position", id))
	}
	x, y := t.xy_floats()
	tsize := float64(TILE_SIZE)

	x *= tsize
	y *= tsize

	x += tsize / 2
	y += tsize / 2

	c.FocusOnceAt(x, y)
}
func (c *Camera) FocusOnceAt(x, y float64) {
	c.StopFollowing()
	c.TargetX, c.TargetY = x, y
}

func (c *Camera) FocusFollowEntity(id entity_id, pos *TilePosition) {
	c.Follow.TargetID = id
	c.Follow.PositionPtr = pos
}
func (c *Camera) IsFollowing() bool {
	return c.Follow.TargetID > 0 && c.Follow.PositionPtr != nil
}
func (c *Camera) StopFollowing() {
	c.Follow.TargetID = 0
	c.Follow.PositionPtr = nil
}

func (c *Camera) Update(dt SecsDT) {
	if c.IsFollowing() {
		// Pretty slow, but might be necessary
		c.FocusOnceOn(c.Follow.TargetID, *c.Follow.PositionPtr)
	}

	dx := c.TargetX - c.X
	dy := c.TargetY - c.Y
	{
		easeIn := 3 * dt // Stay on target, just so I see it right away
		dx *= easeIn
		dy *= easeIn
	}
	c.X += dx
	c.Y += dy
}

func (c *Camera) GeoM() (g ebi.GeoM) {
	// TODO:  Any rounding here perhaps?
	tx, ty := -c.X, -c.Y
	tx, ty = tx+RENDER_W/2, ty+RENDER_H/2
	tx, ty = math.Round(tx), math.Round(ty)
	g.Translate(tx, ty)
	return g
}
