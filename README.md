# gamejams

Repo for all gamejams for easy tracking of our progress.


# Notes and todos

> Please keep sorted by importance

- [x] Review the 1-2021 gamejam
  - [x] Decide what parts could be reused for the next one
  - [x] Improve the quality of code before reuse

- [x] Get ready for the 2-2021 jam
  - [x] 1-bit pixel art research (also it's 84x48, really smol)
  - [x] Basic window
  - [x] Code reuse
  - [x] Set up the window with the right resolution, grab the right colors etc.
  
- [ ] Review the 2-2021 jam
  - [ ] Improve the animation code a bit
  - [ ] Hook up a watch system for config files. Could be either jsons or they \
        could be toml files... Probably not anything custom although that could \
        also be an option. \
        `go get -u github.com/BurntSushi/toml` \
        Basically we need an asset file that gets reloaded and things read \
        from the object that it dumps the stuff into. Should work well tbh.
  - [x] Logging is kinda meh, we can make a better timestamp for sure
  
- [ ] Get ready for the 3-2021 jam
  - [x] Clean up stuff we won't need -- I've made a lot of bad decisions \
        with the last game.
  - [x] Make multiple files for assets. We really don't need all of it packed \
        into one file like this.
  - [ ] Test the generator on an empty file
  - [ ] Review the way we do file generation. I remember it being a bit too tricky.

- Itch.io and web builds
  - [ ] Take a look at that butler or whatever
  - [ ] Focus border can still fail -- we need to know if THE GAME is in focus, \
        not just the body. 

- [ ] Missing utilities
  - [x] Generating a file with constants of all assets uploaded to a folder.
  - [x] Imdraw for Ebiten (maybe we can find something online)
  - [ ] Printing debug stuff on screen
  - [x] Test asset reloads at runtime
  - [ ] Watched configs for fine-tuning at runtime
  - [ ] A basic way to input commands in-game (some console)
  - [ ] Pausing the game
  
- [ ] Animation & AnimationPlayer
  - [x] The frame overflow action shouldn't have to be called, \
        should be a variable instead
  - [ ] The timers on animation players are really strange and don't really \
        work right... Sometimes the animations play way faster than they should \
        and I'm not sure why -- looks like some kind of overflow or precision bug.
  - [ ] Revise the API, using it is clunky and uncomfortable
  - [ ] Add event subscription -- at start, at end, looped, percentage, frame index etc.
    - [ ] Could handle switching and chaining animations, no need to support \
          beyond utility shortcuts, I think.
    

  
- [ ] Unresolved bugs
  - [ ] Some key inputs don't work on Milky's machine (everything non-letter)



# Used tools
## Aseprite
## Emacs with LSP
## Go programming language (Golang)
## Ebiten (2D game framework)

Chosen for web builds, but [Pixel](https://github.com/faiface/pixel) would have been preferred for its utilities and overall more comfortable API.

### Install

[Ebiten install](https://ebiten.org/documents/install.html)

Installing dependencies (Ubuntu/Debian)
```sh
sudo apt install libc6-dev libglu1-mesa-dev libgl1-mesa-dev libxcursor-dev libxi-dev libxinerama-dev libxrandr-dev libxxf86vm-dev libasound2-dev pkg-config
```

